<?php

require_once('admin.php');

$per_tag = new Permission;
$per_tag->premission_tag = "modify_roles";
$permission_block = $per_tag->check_permission($db);
if (!$permission_block) {
    $INCLUDE_FILE = "permission_denied.php";
    require_once('template_main.php');
    exit();
}


$groupname = "";
$groupdesc = "";
$status = "";
$nameerror = FALSE;
$error = FALSE;
$descerror = FALSE;

if (isset($_POST['btnedit']) || isset($_POST['btnadd'])) {
    $groupname = clean_text($_POST['groupnaem']);
    $groupdesc = clean_text($_POST['groupdesc']);
    $status = clean_text($_POST['status']);
    if ($groupname == '') {
        $nameerror = TRUE;
        $error = TRUE;
    }

    if ($groupdesc == '') {
        $descerror = TRUE;
        $error = TRUE;
    }

    if (!$error) {
        $data = array();
        $data['group_name'] = $groupname;
        $data['description'] = $groupdesc;
        $data['status'] = $status;

        if (isset($_POST['btnadd'])) {
            $result = $db->query_insert("tblgroups", $data);
            if ($result) {
                //**************** generate log entry *******************
                $logString = "Add New User Role,Role id- " . $result . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************
                header("Location:permissionset.php?type=2&id=" . $result . "&msg=" . base64_encode(7) . "");
                exit;
            } else {
                header("Location:modify_role.php?err=" . base64_encode(5) . "");
                exit;
            }
        }

        if (isset($_POST['btnedit'])) {
            $id = $_POST['eid'];
            $result = $db->query_update("tblgroups", $data, "id=" . $id);
            if ($result) {
                //**************** generate log entry *******************
                $logString = "Updated User Role,Role id- " . $id . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************
                header("Location:permissionset.php?type=2&id=" . $id . "&msg=" . base64_encode(6) . "");
                exit;
            } else {
                header("Location:modify_role.php?eid=" . $id . "&err=" . base64_encode(5) . "");
                exit;
            }
        }
    }
}





//group_name 	description 	active
if (isset($_GET) && isset($_GET['eid']) && is_numeric($_GET['eid'])) {
    $id = clean_text($_GET['eid']);
    $data = getData::getUserGroup_data_byID($id);
    $groupname = $data['group_name'];
    $groupdesc = $data['description'];
    $status = $data['status'];
}



if (isset($_GET['eid']) && is_numeric($_GET['eid'])) {
    $temp_heading = "Update User Role";
} else {
    $temp_heading = "Add User Role";
}

$page_main_heading = '<i class="fa fa-wrench"></i>&nbsp;&nbsp;'.'Administrative';
$breaddrum = "<li class='active'>$temp_heading</li>";
$INCLUDE_FILE = "includes/modify_role.tpl.php";


require_once('template_main.php');
?>