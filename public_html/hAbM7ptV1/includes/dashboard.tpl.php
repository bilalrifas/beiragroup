<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-title"><i class="fa fa-wrench"></i>&nbsp;&nbsp;Administrative</div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-11">
                        <a href="adminmails.php" class="btn btn-app btn-block"><i class="fa fa-envelope"></i>Admin Emails</a>
                    </div>
                    <div class="col-md-5">
                        <a href="permissionlevels.php" class="btn btn-app btn-block"><i class="fa fa-copy"></i>Permission Levels</a>
                    </div>
                    <div class="col-md-6">
                        <a href="permission_category.php" class="btn btn-app btn-block"><i class="fa fa-tags"></i>Permission Category</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <a href="user_rolls.php" class="btn btn-app btn-block"><i class="fa fa-users"></i>User Roles</a>
                    </div>
                    <div class="col-md-5">
                        <a href="viewusers.php" class="btn btn-app btn-block"><i class="fa fa-user"></i>Users</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <a href="rsync_dry.php" class="btn btn-app btn-block"><i class="fa fa-bolt"></i>Synchronize Mirror Server (Dry Run)</a>
                    </div>
                    <div class="col-md-6">
                        <a href="rsync.php" class="btn btn-app btn-block"><i class="fa fa-bolt"></i>Synchronize Mirror Server</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-title"><i class="fa fa-list-alt"></i>&nbsp;&nbsp;Manage Site Content</div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-5">
                        <a href="maindata.php" class="btn btn-app btn-block"><i class="fa fa-tasks"></i>Main Site Data</a>
                    </div>
                    <div class="col-md-6">
                        <a href="mainmenu_edit.php" class="btn btn-app btn-block"><i class="fa fa-list-alt"></i>Modify Menu</a>
                    </div>
                    <div class="col-md-11">
                        <a href="mainmenu.php" class="btn btn-app btn-block"><i class="fa fa-th-list "></i>Main Menu</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-title"><i class="fa fa-list-alt"></i>&nbsp;&nbsp;Manage Products</div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-5">
                        <a href="add_productCategory.php" class="btn btn-app btn-block"><i class="fa fa-archive"></i>Manage Product Categories</a>
                    </div>
                    <div class="col-md-6">
                        <a href="add_subProductCategory.php" class="btn btn-app btn-block"><i class="fa fa-plus-square"></i>Add Sub Product Categories</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <a href="add_products.php" class="btn btn-app btn-block"><i class="fa fa-plus-square-o"></i>Add Products</a>
                    </div>
                    <div class="col-md-6">
                        <a href="view_products.php" class="btn btn-app btn-block"><i class="fa fa-search-plus"></i>View All Products</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

