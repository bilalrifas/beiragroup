<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-title"><i class="fa fa-users"></i>&nbsp;&nbsp;User Roles</div>
            </div>
            <div class="box-body">
                <form class="form-horizontal span8" name="frm_modify_role" enctype="multipart/form-data" action="" method="post" role="form">

                    <div class="row">
                        <div class="col-md-3"><label for="inputname">Role Name</label></div>
                        <div class="col-md-4">
                            <input type="text" name="groupnaem" id="groupname" class="form-control" value="<?php echo $groupname; ?>" /><?php if ($nameerror) { ?>
                            <span class="text-danger">Name Cannot Be Blank</span><?php } ?>
                        </div>
                    </div><br/>
                    
                    <div class="row">
                        <div class="col-md-3"><label for="inputname">Role Description</label></div>
                        <div class="col-md-4">
                            <textarea name="groupdesc" id="groupdesc" class="form-control"><?php echo $groupdesc; ?></textarea>
                            <?php if ($descerror) { ?><span id="descerror" class="text-danger">Name Description Cannot Be Blank</span><?php } ?>
                        </div>
                    </div><br/>
                    
                    <div class="row">
                        <div class="col-md-3"><label for="inputname">Status</label></div>
                        <div class="col-md-4">
                            <select name ="status" id="status" class="form-control">
                                <option value="1" <?php
                                if ($status == 1) {
                                    echo "selected";
                                }
                                ?> >Active</option>
                                <option value="0" <?php
                                if ($status == 0) {
                                    echo "selected";
                                }
                                ?> >Deactive</option>
                            </select>
                        </div>
                    </div><br/>
                    
                    <div class="row"> 
                        <div class="col-md-4 col-md-offset-3">

                            <?php if (isset($_GET) && isset($_GET['eid']) && is_numeric($_GET['eid'])) { ?>
                                <input type="hidden" name="eid" id="eid" value="<?php echo $id; ?>" />
                                <button type="submit" name="btnedit" id="btnedit" class="btn btn-primary btn-flat btn-block"><b class="fa fa-edit"></b>&nbsp;&nbsp;&nbsp;Update Permission Level</button>
                            <?php } else { ?>
                                <button type="submit" name="btnadd" id="btnadd" class="btn btn-primary btn-flat btn-block">Add permission Levels&nbsp;&nbsp;<i class="fa fa-arrow-right"></i></button>
                                <?php } ?> 
                        </div>
                    </div>
                </form>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-md-2 col-md-offset-10">
                        <button class="btn btn-default btn-flat btn-block btnback"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back</button>
                    </div>
                </div>   
            </div>
        </div>
    </div>
</div>

