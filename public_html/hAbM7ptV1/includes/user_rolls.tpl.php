<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-title"><i class="fa fa-users"></i>&nbsp;&nbsp;User Roles</div>
                <div class="box-tools pull-right">
                    <!-- Button trigger add New Role modal -->
                    <div style="padding-right: 110px; "><a href="modify_role.php" class="btn btn-default btn-flat" ><i class="fa fa-users"></i>&nbsp;Add New Role</a></div>
                </div>
            </div>
            <div class="box-body">
                <?php if (sizeof($all_data) < 1) { ?>
                    <div class="alert alert-danger">
                        <strong>Sorry !</strong>&nbsp;&nbsp;&nbsp;No Data Found.
                    </div>
                <?php } else { ?>
                    <table  class="table table-condensed">
                        <thead>
                            <tr>
                                <th width="25%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >Name</th>
                                <th width="50%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >Description</th>
                                <th width="25%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >&nbsp;</th>

                            </tr>
                        </thead>
                        <tbody>
                            <!--         	 	 	 	 	 	 	status-->
                            <?php foreach ($datas as $data) { ?>
                                <tr>
                                    <td><?php echo $data['group_name']; ?></td> 
                                    <td><?php echo $data['description'] ?></td> 
                                    <td>
                                        <a href="modify_role.php?eid=<?php echo $data['id']; ?>" class="btn btn-primary btn-flat" ><b class="fa fa-edit"></b>&nbsp;&nbsp;Edit</a>
                                        <a href="<?php echo $_SERVER['PHP_SELF'] . '?action=delete&did=' . $data['id'] . ''; ?>" onclick="return confirm('Are you sure to delete this news?');" class=" btn btn-danger flat" ><b class="fa fa-eraser"></b>&nbsp;&nbsp;Delete</a>
                                    </td> 
                                </tr>
                            <?php } ?>
                            <tr>
                                <td colspan="4" style="text-align: right;">
                                    <div class="pagination" style="float: right;">
                                        <?php
                                        $links = $pager->getLinks();
                                        echo $links['all'];
                                        ?>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                <?php } ?>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-md-2 col-md-offset-10">
                        <button class="btn btn-default btn-flat btn-block btnback"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back</button>
                    </div>
                </div>   
            </div>
        </div>
    </div>
</div>