<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-2">
                <label for="selectAll" >Select All:</label><input  type="checkbox" name="selectAll" id="selectAll" />
            </div>
            <div class="col-md-2">
                <label for="deselectAll" >Deselect All:</label><input type="checkbox" name="deselectAll" id="deselectAll" />
            </div>
        </div>
    </div>
</div><br/>

<div class="row">
    <?php if (isset($error) && $error != '') { ?>
        <div class="alert alert-danger" style="color: #fefefe; padding:5px;" ><?php echo $error; ?></div>
    <?php } ?>
    <form name="frm_post_comments" enctype="multipart/form-data" action="" method="post">
        <input type="hidden" name="type" value="<?php echo $type; ?>" />
        <input type="hidden" name="id" value="<?php echo $id; ?>" />
        <?php
        $all_groups = getData::getAllActivepermission_categories();
        $i = 1;
        foreach ($all_groups as $groups) {
            ?>
            <div class="col-md-4">
                <div class="box box-solid box-primary">
                    <div class="box-header">
                        <div class="box-title"><i class="fa fa-copy"></i>&nbsp;&nbsp;<?php echo ucwords($groups['catagory']); ?></div>
                    </div>

                    <div class="box-body">
                        <ul style="padding: 0px ; margin: 0px;">
                            <?php
                            $allpermissions = getData::getpermission_leveldata_byCategory($groups['id']);
                            //----
                            $permission_pages = $db->query_first("SELECT permissions from tblgroup_permissions WHERE group_id = $id");

                            $permission_pages = explode(',', $permission_pages['permissions']);

                            //----
                            foreach ($allpermissions as $permission) {
                                ?>
                                <li style="font-size:12px; list-style: none;"><input style="margin: 0px;padding: 0px; width: 18px;height: 18px;" class="allchecks" <?php if (in_array($permission['id'], $permission_pages)) { ?> checked="checked" <?php } ?> type="checkbox" name="permissiondata[]" value="<?php echo $permission['id']; ?>" >&nbsp;&nbsp;<?php echo $permission['description']; ?></li>

                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
            <?php
            if (($i % 3) == 0) {
                echo "<div style='clear:both;'></div>";
            }
            $i++;
        }
        ?>
        <div style="clear: both;"></div>
        <hr>
        <button class="btn btn-default btn-flat btnback pull-right" style="margin-left: 10px;"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back</button>
        <button type="submit" name="reset_permission" id="reset_permission" class="btn btn-primary btn-flat pull-right"><i class="fa fa-copy"></i>&nbsp;&nbsp;Reset Permission&nbsp;&nbsp;</button>
    </form>
</div>


