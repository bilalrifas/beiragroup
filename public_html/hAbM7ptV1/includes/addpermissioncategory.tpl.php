<div class="modal fade" id="addnewpermissioncategory" tabindex="-1" role="dialog" aria-labelledby="addnewpermissioncategoryLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="addnewpermissioncategoryLabel"><i class="fa fa-tag"></i>&nbsp;&nbsp;Add New Permission Category</h4>
            </div>
            <form  name="frm_add_per_cat" id="frm_add_per_cat" enctype="multipart/form-data" action="ajax_data.php" method="post">
                <div id="msg_success" hidden="hidden">
                    <div class="alert alert-success ">
                        <i class="fa fa-check"></i>
                        <div id="msg_txt"></div>
                    </div>
                </div>
                <div id="msg_error" hidden="hidden">
                    <div class="alert alert-danger">
                        <i class="fa fa-check"></i>
                        <div id="msg_txt"></div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3"><label class="control-label" for="inputname">Category Name</label></div>
                        <div class="col-md-4">
                            <input type="text" name="name" id="name" class="form-control" />
                        </div>
                    </div><br/>
                    <div class="row">
                        <div class="col-md-3"><label class="control-label" for="inputname">Status</label></div>
                        <div class="col-md-4">
                            <select name ="status" id="status" class="form-control">
                                <option value="1" selected >Active</option>
                                <option value="0" >Deactive</option>
                            </select>
                        </div>
                    </div>    
                </div>
                <div class="modal-footer">
                    <button type="submit" name="btnadd" id="btnadd" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Add Permission Category</button>
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                </div>
                <input type="hidden" name="actionrequest" value="addpermiscat"/>
                <input type="hidden" name="type" id="type" value="0"/>
                <input type="hidden" name="eid" id="eid" value=""/>
            </form>
        </div>
    </div>
</div>