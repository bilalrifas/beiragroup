 <div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-title"><i class="fa fa-th-list"></i>&nbsp;&nbsp;Add Menu Item</div>
            </div>
            <div class="box-body">
                <form action="" method="post" enctype="multipart/form-data">

                    <div class="row">
                        <div class="col-md-2"><label class="control-label" for="inputtitle">Title</label></div>
                        <div class="col-md-4">
                            <input type="text" name="title" id="inputtitle" class="form-control" value="<?php echo $title; ?>">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <label class="checkbox">

                            </label>
                            <button type="submit" name="btnadd" class="btn btn-primary btn-flat btn-block"><i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp;Add Menu Item</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-md-2 col-md-offset-10">
                        <button class="btn btn-default btn-flat btn-block btnback"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back</button>
                    </div>
                </div>   
            </div>
        </div>
    </div>
</div>
<!------------------------------------------------------------------------------------------------------------------------->


<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-title"><i class="fa fa-th-list"></i>&nbsp;&nbsp;Menu Items</div>
            </div>
            <div class="box-body">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" >

                    <tr>
                        <td colspan="3" align="center" >
                            <?php if (sizeof($content_link) < 1) { ?>
                                <div class="no_data"><?php echo "No menu Items"; ?></div>
                            <?php } else { ?>
                                <table width="100%" border="0" cellspacing="0" cellpadding="2" class="table table-hover">

                                    <tr class="table_heading">
                                    <thead>
                                        <tr class="table_heading">

                                            <th height="30" width="15%" align="left" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" ><strong>Name</strong></th>
                                            <th width="9%" align="left" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>"><strong>Status</strong></th>
                                            <th width="13%" align="left" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>"><strong>Added Date</strong></th>
                                            <th align="left" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>">&nbsp;</th>
                                        </tr>
                                    </thead>  
                                    <tbody>  
                                        <?php
                                        //var_dump($content_links);
                                        $i = 0;
                                        foreach ($content_link as $row) {
                                            ?>

                                            <tr <?php
                                            if (($i % 2) == 0) {
                                                echo "bgcolor='#FAFAFA'";
                                            } else {
                                                echo 'bgcolor="#FFFFFF"';
                                            }
                                            ?>>
                                                <td align="left"  class="small_f1"><span style="width:100px; margin-right: 10px; text-align: left; color:blueviolet;"> (<?php echo $row['maincontent_id']; ?>).</span><?php echo $row['heading']; ?></td>
                                                <td align="left"  class="small_f1">
                                                    <?php
                                                    if ($row['status'] == 1) {
                                                        echo "active";
                                                    } else {
                                                        echo "inactive";
                                                    }
                                                    ?>		
                                                </td>
                                                <td align="left"  class="small_f1"><?php echo $row['updated_date']; ?></td>


                                                <td width="23%" align="left" valign="top">
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-primary btn-flat">Action</button>
                                                            <button type="button" class="btn btn-primary btn-flat dropdown-toggle" data-toggle="dropdown">
                                                                <span class="caret"></span>
                                                                <span class="sr-only">Toggle Dropdown</span>
                                                            </button>
                                                            <ul class="dropdown-menu" role="menu">
                                                                <?php if ($row['status'] != 1) { ?>
                                                                    <li><a href=<?php echo $_SERVER['PHP_SELF'] . '?action=status_change&active=1&id=' . $row['maincontent_id'] . ''; ?> onclick="return confirm('Are you sure to active this Menu Item?');">Active</a></li>
                                                                <?php } else { ?>
                                                                     <li><a href=<?php echo $_SERVER['PHP_SELF'] . '?action=status_change&active=0&id=' . $row['maincontent_id'] . ''; ?> onclick="return confirm('Are you sure to inactive this Menu Item?');">Inactive</a></li>
                                                                <?php } ?>
                                                            </ul>
                                                        </div>
                                                    &nbsp;
                                                    <a class="btn btn-danger btn-flat" href=<?php echo $_SERVER['PHP_SELF'] . '?action=delete_menu&id=' . $row['maincontent_id'] . ''; ?> onClick="return confirm('Are you sure to Delete this Menu Item?');"><i class="fa fa-eraser"></i>&nbsp;&nbsp;&nbsp;Delete</a> 

                                                </td>
                                            </tr>

                                            <?php
                                            $i++;
                                        }
                                        ?>
                                        <tr>
                                            <td height="39" colspan="7" align="right" valign="middle"></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>	


                                <tr>
                                    <td bgcolor="#FFFFFF">&nbsp;</td>
                                    <td colspan="5" bgcolor="#FFFFFF">&nbsp;</td>

                                    <td width="2%" bgcolor="#FFFFFF">&nbsp;</td>
                                </tr>
                            </table></td>
                    </tr>
                </table>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-md-2 col-md-offset-10">
                        <button class="btn btn-default btn-flat btn-block btnback"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back</button>
                    </div>
                </div>   
            </div>
        </div>
    </div>
</div>