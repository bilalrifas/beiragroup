<div class="row">
    <div class="col-md-12">
    <div class="box box-primary">
    <div class="box-header">
        <div class="box-title"></div>
    </div>
    <div class="box-body">
    <div class="row">
    <div class="col-md-8 col-md-offset-2">
        <form action="" method="POST" role="form">
            <legend><i class="fa fa-plus-square"></i>&nbsp;&nbsp;<?php echo $page_headings ?></legend>



            <div class="form-group">
                <label for="heading">Name</label>
                <input type="text" class="form-control" name="heading" value="<?php echo $heading ?>" placeholder="Enter Name" required="required">
            </div>
            <div class="form-group">
                <label for="special_heading">Display Title</label>
                <input type="text" class="form-control" name="special_heading" value="<?php echo $special_heading ?>" placeholder="Enter Title" required="required">
            </div>
            <div class="form-group">
                <label for="special_wording">Display Title Wording</label>
                <input type="text" class="form-control" name="special_wording" value="<?php echo $special_wording ?>" placeholder="Enter Wording" >
            </div>
            <div class="form-group">
                <label for="category_id">Main Category Type</label>
                <select name="category_id" id="input" class="form-control" required="required">
                    <option value="">Select a Category</option>
                    <?php foreach ($category_array as $key => $value): ?>
                        <option value="<?php echo $value['id'] ?>" <?php if ($value['id']==$category_id): ?> selected <?php endif ?> > <?php echo $value['title'] ?></option>
                    <?php endforeach ?>
                </select>
            </div>

            <?php if (isset($_GET['id']) && is_numeric($_GET['id'])):?>
            <button type="submit" name="btnedit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp;Edit Sub Category</button>
            <input type="hidden" name="id" value="<?php echo $id ?>">
            <?php else: ?>
            <button type="submit" name="btnadd" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp;Add Sub Category</button>
            <?php endif ?>
        </form>
    </div>
    </div>
    </div>
    <div class="box-footer">
    <div class="row">
        <div class="col-md-2 col-md-offset-10">
            <button class="btn btn-default btn-flat btn-block btnback"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back</button>
        </div>
    </div>
    </div>
    </div>
    </div>
</div>
