<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-title"><i class="fa fa-th-list"></i>&nbsp;&nbsp;<?php echo $pagename; ?></div>
            </div>
            <div class="box-body">
                <form action="" method="post" enctype="multipart/form-data">

                    <div class="row">
                        <div class="col-md-2"><label class="control-label" for="inputtitle">Title</label></div>
                        <div class="col-md-4">
                            <input type="text" name="heading" id="inputtitle" class="form-control" value="<?php echo $heading; ?>">
                        </div>
                    </div><br/>

                    <div class="row">
                        <div class="col-md-2"><label class="control-label" for="inputtitle">Special Title</label></div>
                        <div class="col-md-4">
                            <input type="text" name="spe_heading" id="inputtitle" class="form-control" value="<?php echo $spe_heading; ?>">
                        </div>
                    </div><br/>

                    <div class="row">
                        <div class="col-md-2"><label class="control-label" for="inputtitle">Details</label></div>
                        <div class="col-md-10">
                            <textarea name="details" id="details" rows="15" class="form-control tinyEditor">
                                <?php echo $details; ?>
                            </textarea>
                        </div>
                    </div><br/>

                    <div class="row">
                        <div class="col-md-2"><label class="control-label" for="inputtitle">Special Details</label></div>
                        <div class="col-md-8">
                            <textarea name="spe_details" id="details" rows="15" class="form-control tinyEditor">
                                <?php echo $spe_details; ?>
                            </textarea>
                        </div>
                    </div><br/>
                    <br/>
                    <pre>Meta Data</pre>
                    <br/>
                    <div class="row">
                        <div class="col-md-2"><label class="control-label" for="inputtitle">Title</label></div>
                        <div class="col-md-7">
                            <textarea name="meta_title" rows="4"  class="form-control"><?php echo $meta_title; ?></textarea>
                        </div>
                    </div><br/>

                    <div class="row">
                        <div class="col-md-2"><label class="control-label" for="inputtitle">Meta Description</label></div>
                        <div class="col-md-7">
                            <textarea name="meta_desc" rows="4"  class="form-control"><?php echo $meta_desc; ?></textarea>
                        </div>
                    </div><br/>

                    <div class="row">
                        <div class="col-md-2"><label class="control-label" for="inputtitle">Meta Keywords</label></div>
                        <div class="col-md-7">
                            <textarea name="meta_key" rows="4"  class="form-control"><?php echo $meta_key; ?></textarea>
                        </div>
                    </div><br/>
                    <br/>
                    <pre>Page Images</pre>
                    <br/>
                    <div class="row">
                        <div class="col-md-2"><label class="control-label" for="inputtitle">Page Image 1</label></div>
                        <div class="col-md-4">
                            <?php if (!file_exists("../images/$image1") || $image1 == "") { ?>
                                <input type="file" name="image1" id="image1" class="inputbox" onchange="return checkImage('image1')">	
                            <?php } else { ?>
                                <a href="../images/<?php echo $image1; ?>" data-toggle="lightbox"> <!--data-title="A random title" data-footer="A custom footer text"-->
                                    <img src="../images/<?php echo $image1; ?>" class="img-responsive">
                                </a>
                                
                                <a class="btn btn-danger btn-flat btn-block" href="<?php echo $_SERVER['PHP_SELF'] . '?action=delete_subotherothercontent_image&maincontent_id=' . $maincontent_id . "&subcontent_id=" . $subcontent_id . "&subothercontent_id=" . $subothercontent_id ."&subother_other_content_id=".$subother_othercontent_id. '&image=' . $image1 . '&imageno=' . 'image1'; ?>" onclick="return confirm('Are you sure to delete this image?');"><i class="fa fa-eraser"></i>&nbsp;&nbsp;Delete</a>
                            <?php } ?>
                        </div>
                    </div><br/>

                    <div class="row">
                        <div class="col-md-2"><label class="control-label" for="inputtitle">Page Image 2</label></div>
                        <div class="col-md-4">
                            <?php if (!file_exists("../images/$image2") || $image2 == "") { ?>
                                <input type="file" name="image2" id="image2" class="inputbox" onchange="return checkImage('image2')">	
                            <?php } else { ?>
                                
                                <a href="../images/<?php echo $image2; ?>" data-toggle="lightbox"> <!--data-title="A random title" data-footer="A custom footer text"-->
                                    <img src="../images/<?php echo $image2; ?>" class="img-responsive">
                                </a>
                                <a class="btn btn-danger btn-flat btn-block" href="<?php echo $_SERVER['PHP_SELF'] . '?action=delete_subotherothercontent_image&maincontent_id=' . $maincontent_id . "&subcontent_id=" . $subcontent_id . "&subothercontent_id=" . $subothercontent_id ."&subother_other_content_id=".$subother_othercontent_id. '&image=' . $image2 . '&imageno=' . 'image2'; ?>" onclick="return confirm('Are you sure to delete this image?');"><i class="fa fa-eraser"></i>&nbsp;&nbsp;Delete</a>
                            <?php } ?>
                        </div>
                    </div><br/><br/>
                    
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                                <input type="hidden" name="maincontent_id" value="<?php echo $maincontent_id; ?>" />
                            </label>
                            <input type="hidden" name="subcontent_id" value="<?php echo $subcontent_id; ?>" />	
                            <input type="hidden" name="maincontent_id" value="<?php echo $maincontent_id; ?>" />
                            <input type="hidden" name="sub_othercontent_id" value="<?php echo $subothercontent_id; ?>" />

                            <?php if ((isset($_GET['subother_other_content_id'])) && (is_numeric($_GET['subother_other_content_id']))) { ?>
                                <input type="hidden" name="subotherothercontent_id" value="<?php echo $subother_othercontent_id; ?>" />	
                                <button type="submit" name="btnedit" class="btn btn-primary btn-flat btn-block"><i class="fa fa-edit"></i>&nbsp;&nbsp;&nbsp;Update Details</button>
                            <?php } else { ?>
                                <button type="submit" name="btnadd" class="btn btn-primary btn-flat btn-block"><i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp;Save Details</button>
                            <?php } ?>
                        </div>
                    </div>
                </form>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-md-2 col-md-offset-10">
                        <button class="btn btn-default btn-flat btn-block btnback"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back</button>
                    </div>
                </div>   
            </div>
        </div>
    </div>
</div>
