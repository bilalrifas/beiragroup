<!--
<div class="tree well">
    <ul>
        <li>
            <span><i class="fa fa-plus"></i> Parent</span> <a href="">Goes somewhere</a>
            <ul>
                <li>
                	<span><i class="fa fa-minus"></i> Child</span> <a href="">Goes somewhere</a>
                    <ul>
                        <li>
	                        <span><i class="fa fa-leaf"></i> Grand Child</span> <a href="">Goes somewhere</a>
                        </li>
                    </ul>
                </li>
                <li>
                	<span><i class="fa fa-minus"></i> Child</span> <a href="">Goes somewhere</a>
                    <ul>
                        <li>
	                        <span><i class="fa fa-leaf"></i> Grand Child</span> <a href="">Goes somewhere</a>
                        </li>
                        <li>
                        	<span><i class="fa fa-minus"></i> Grand Child</span> <a href="">Goes somewhere</a>
                            <ul>
                                <li>
	                                <span><i class="fa fa-minus"></i> Great Grand Child</span> <a href="">Goes somewhere</a>
		                            <ul>
		                                <li>
			                                <span><i class="fa fa-leaf"></i> Great great Grand Child</span> <a href="">Goes somewhere</a>
		                                </li>
		                                <li>
			                                <span><i class="fa fa-leaf"></i> Great great Grand Child</span> <a href="">Goes somewhere</a>
		                                </li>
		                             </ul>
                                </li>
                                <li>
	                                <span><i class="fa fa-leaf"></i> Great Grand Child</span> <a href="">Goes somewhere</a>
                                </li>
                                <li>
	                                <span><i class="fa fa-leaf"></i> Great Grand Child</span> <a href="">Goes somewhere</a>
                                </li>
                            </ul>
                        </li>
                        <li>
	                        <span><i class="fa fa-leaf"></i> Grand Child</span> <a href="">Goes somewhere</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li>
            <span><i class="fa fa-folder-open"></i> Parent2</span> <a href="">Goes somewhere</a>
            <ul>
                <li>
                	<span><i class="fa fa-leaf"></i> Child</span> <a href="">Goes somewhere</a>
		        </li>
		    </ul>
        </li>
    </ul>
</div>

<div class="tree">
    <ul>
        <li>
            <span><i class="icon-calendar"></i> 2013, Week 2</span>
            <ul>
                <li>
                	<span class="badge badge-success"><i class="icon-minus-sign"></i> Monday, January 7: 8.00 hours</span>
                    <ul>
                        <li>
	                        <a href=""><span><i class="icon-time"></i> 8.00</span> &ndash; Changed CSS to accomodate...</a>
                        </li>
                    </ul>
                </li>
                <li>
                	<span class="badge badge-success"><i class="icon-minus-sign"></i> Tuesday, January 8: 8.00 hours</span>
                    <ul>
                        <li>
	                        <span><i class="icon-time"></i> 6.00</span> &ndash; <a href="">Altered code...</a>
                        </li>
                        <li>
	                        <span><i class="icon-time"></i> 2.00</span> &ndash; <a href="">Simplified our approach to...</a>
                        </li>
                    </ul>
                </li>
                <li>
                	<span class="badge badge-warning"><i class="icon-minus-sign"></i> Wednesday, January 9: 6.00 hours</span>
                    <ul>
                        <li>
	                        <a href=""><span><i class="icon-time"></i> 3.00</span> &ndash; Fixed bug caused by...</a>
                        </li>
                        <li>
	                        <a href=""><span><i class="icon-time"></i> 3.00</span> &ndash; Comitting latest code to Git...</a>
                        </li>
                    </ul>
                </li>
                <li>
                	<span class="badge badge-important"><i class="icon-minus-sign"></i> Wednesday, January 9: 4.00 hours</span>
                    <ul>
                        <li>
	                        <a href=""><span><i class="icon-time"></i> 2.00</span> &ndash; Create component that...</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li>
            <span><i class="icon-calendar"></i> 2013, Week 3</span>
            <ul>
                <li>
                	<span class="badge badge-success"><i class="icon-minus-sign"></i> Monday, January 14: 8.00 hours</span>
                    <ul>
                        <li>
	                        <span><i class="icon-time"></i> 7.75</span> &ndash; <a href="">Writing documentation...</a>
                        </li>
                        <li>
	                        <span><i class="icon-time"></i> 0.25</span> &ndash; <a href="">Reverting code back to...</a>
                        </li>
                    </ul>
                </li>
		    </ul>
        </li>
    </ul>
</div>
-->
<?php
$mainmenu = content::get_all_main_content();
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-title"><i class="fa fa-list-alt"></i>&nbsp;&nbsp;Modify Menu</div>
            </div>
            <div class="box-body">
                <div class="tree">
                    <ul>
                        <li><span class="flat"><i class="fa fa-minus"></i>&nbsp;&nbsp;<?php echo $companyname; ?></span><a href="#"></a>
                            <ul> 
                                <?php
                                foreach ($mainmenu as $mmenu) {
                                    $submenu = content::get_subcontents_by_main_id($mmenu['maincontent_id']);
                                    ?>
                                    <li><span class="flat"><?php if (sizeof($submenu) > 0) { ?><i class="fa fa-minus"></i>&nbsp;&nbsp;<?php } else { ?><i class="fa fa-leaf"></i>&nbsp;&nbsp;<?php } echo $mmenu['heading'] ?></span>
                                    <action_list class="label label-primary flat" style="font-size: 1.2em;">
                                        <?php if ($mmenu['status'] == 0) { ?>
                                            <a class="btn btn-default btn-flat" title="Active Page" href="<?php echo $_SERVER['PHP_SELF']; ?>?action=mainstatus&status=1&mid=<?php echo $mmenu['maincontent_id']; ?>" ><i class="fa fa-circle" style="color: #F00;"></i></a>&nbsp;&nbsp;
                                        <?php } else { ?>
                                            <a class="btn btn-default btn-flat" title="Deactive Page" href="<?php echo $_SERVER['PHP_SELF']; ?>?action=mainstatus&status=0&mid=<?php echo $mmenu['maincontent_id']; ?>" ><i class="fa fa-circle" style="color: #006505;"></i></a>&nbsp;&nbsp;
                                        <?php } ?>
                                        <a class="btn btn-primary btn-flat" title="Add New Sub Page" href="sub_content.php?maincontent_id=<?php echo $mmenu['maincontent_id']; ?>" ><i class="fa fa-plus"></i></a>&nbsp;&nbsp;
                                        <a class="btn btn-info btn-flat" title="Edit Page Content" href="main_content.php?maincontent_id=<?php echo $mmenu['maincontent_id']; ?>" ><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
                                        <a class="btn btn-danger btn-flat" title="Delete Page" href="<?php echo $_SERVER['PHP_SELF']; ?>?action=maindelete&mid=<?php echo $mmenu['maincontent_id']; ?>" ><i class="fa fa-eraser"></i></a>  
                                    </action_list>
                                    <ul>  
                                        <?php
                                        foreach ($submenu as $smenu) {
                                            $subothermenu = content::get_subothercontents_by_sub_id($smenu['subcontent_id']);
                                            ?> 
                                            <li><span class="flat"><?php if (sizeof($subothermenu) > 0) { ?><i class="fa fa-minus"></i>&nbsp;&nbsp;<?php } else { ?><i class="fa fa-leaf"></i>&nbsp;&nbsp;<?php } echo $smenu['heading'] ?></span>

                                            <action_list class="label label-primary flat" style="font-size: 1.2em;">
                                                <?php if ($smenu['status'] == 0) { ?>
                                                    <a class="btn btn-default btn-flat" title="Active Page" href="<?php echo $_SERVER['PHP_SELF']; ?>?action=substatus&status=1&sid=<?php echo $smenu['subcontent_id']; ?>" ><i class="fa fa-circle" style="color: #F00;"></i></a>&nbsp;&nbsp;
                                                <?php } else { ?>
                                                    <a class="btn btn-default btn-flat" title="Deactive Page" href="<?php echo $_SERVER['PHP_SELF']; ?>?action=substatus&status=0&sid=<?php echo $smenu['subcontent_id']; ?>" ><i class="fa fa-circle" style="color: #006505;"></i></a>&nbsp;&nbsp;
                                                <?php } ?>

                                                <a class="btn btn-primary btn-flat" title="Add New Sub Page" href="subother_content.php?subcontent_id=<?php echo $smenu['subcontent_id']; ?>" ><i class="fa fa-plus"></i></a>&nbsp;&nbsp;
                                                <a class="btn btn-info btn-flat" title="Edit Page Content" href="sub_content.php?maincontent_id=<?php echo $smenu['maincontent_id']; ?>&subcontent_id=<?php echo $smenu['subcontent_id']; ?>" ><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
                                                <a class="btn btn-danger btn-flat" title="Delete Page" href="<?php echo $_SERVER['PHP_SELF']; ?>?action=subdelete&sid=<?php echo $smenu['subcontent_id']; ?>" ><i class="fa fa-eraser"></i></a>
                                            </action_list>    
                                            <ul>
                                                <?php
                                                foreach ($subothermenu as $sothermenu) {
                                                    $subotherothermenu = content::get_subother_otherContent_by_subotherID($sothermenu['subothercontent_id']);
                                                    ?>
                                                    <li><span class="flat"><?php if (sizeof($subotherothermenu) > 0) { ?><i class="fa fa-minus"></i>&nbsp;&nbsp;<?php } else { ?><i class="fa fa-leaf"></i>&nbsp;&nbsp;<?php } echo $sothermenu['heading']; ?></span>
                                                    <action_list class="label label-primary flat" style="font-size: 1.2em;">
                                                        <?php if ($sothermenu['status'] == 0) { ?>
                                                            <a class="btn btn-default btn-flat" title="Active Page" href="<?php echo $_SERVER['PHP_SELF']; ?>?action=subothsta&status=1&soid=<?php echo $sothermenu['subothercontent_id']; ?>" ><i class="fa fa-circle" style="color: #F00;"></i></a>&nbsp;&nbsp;
                                                        <?php } else { ?>
                                                            <a class="btn btn-default btn-flat" title="Deactive Page" href="<?php echo $_SERVER['PHP_SELF']; ?>?action=subothsta&status=0&soid=<?php echo $sothermenu['subothercontent_id']; ?>" ><i class="fa fa-circle" style="color: #006505;"></i></a>&nbsp;&nbsp;
                                                        <?php } ?>

                                                        <a class="btn btn-primary btn-flat" title="Add New Sub Page" href="subother_other_content.php?maincontent_id=<?php echo $sothermenu['maincontent_id']; ?>&subcontent_id=<?php echo $sothermenu['subcontent_id']; ?>&subothercontent_id=<?php echo $sothermenu['subothercontent_id']; ?>" ><i class="fa fa-plus"></i></a>&nbsp;&nbsp;
                                                        <a class="btn btn-info btn-flat" title="Edit Page Content" href="subother_content.php?subcontent_id=<?php echo $sothermenu['subcontent_id']; ?>&subothercontent_id=<?php echo $sothermenu['subothercontent_id']; ?>" ><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
                                                        <a class="btn btn-danger btn-flat" title="Delete Page" href="<?php echo $_SERVER['PHP_SELF']; ?>?action=subothdel&soid=<?php echo $sothermenu['subothercontent_id']; ?>" ><i class="fa fa-eraser"></i></a>
                                                    </action_list>
                                                    <ul>
                                                        <?php foreach ($subotherothermenu as $sotherother) { ?>
                                                            <li><span class="flat"><i class="fa fa-leaf"></i>&nbsp;&nbsp;<?php echo $sotherother['heading']; ?></span>
                                                            <action_list class="label label-primary flat" style="font-size: 1.2em;">   
                                                                <?php if ($sotherother['status'] == 0) { ?>
                                                                    <a class="btn btn-default btn-flat" title="Active Page" href="<?php echo $_SERVER['PHP_SELF']; ?>?action=subothothsta&status=1&sooid=<?php echo $sotherother['subother_other_content_id']; ?>" ><i class="fa fa-circle" style="color: #F00;"></i></a>&nbsp;&nbsp;
                                                                <?php } else { ?>
                                                                    <a class="btn btn-default btn-flat" title="Deactive Page" href="<?php echo $_SERVER['PHP_SELF']; ?>?action=subothothsta&status=0&sooid=<?php echo $sotherother['subother_other_content_id']; ?>" ><i class="fa fa-circle" style="color: #006505;"></i></a>&nbsp;&nbsp;
                                                                <?php } ?>  
                                                                <a class="btn btn-info btn-flat" title="Edit Page Content" href="subother_other_content.php?maincontent_id=<?php echo $sotherother['maincontent_id']; ?>&subcontent_id=<?php echo $sotherother['subcontent_id']; ?>&subothercontent_id=<?php echo $sotherother['subothercontent_id']; ?>&subother_other_content_id=<?php echo $sotherother['subother_other_content_id']; ?>" ><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
                                                                <a class="btn btn-danger btn-flat" title="Delete Page" href="<?php echo $_SERVER['PHP_SELF']; ?>?action=subothothdel&sooid=<?php echo $sotherother['subother_other_content_id']; ?>" ><i class="fa fa-eraser"></i></a>
                                                            </action_list>
                                                            </li>
                                                        <?php } ?>
                                                    </ul>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                            </li>
                                        <?php } ?>
                                    </ul>   
                            </li>
                        <?php } ?>
                        <li><a href="mainmenu.php"> <i class="fa fa-plus"></i> &nbsp;&nbsp;Add New</a></li>
                    </ul>
                    </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


 
 


<!--

<div class="row">
    <ul>
        <li><a href="#"></a>
            <ul> 
                <?php
                foreach ($mainmenu as $mmenu) {
                    $submenu = content::get_subcontents_by_main_id($mmenu['maincontent_id']);
                    ?>
                    <li>
                        <a href="main_content.php?maincontent_id=<?php echo $mmenu['maincontent_id']; ?>"><?php echo $mmenu['heading'] ?></a>
                        <ul>  
                            <?php
                            foreach ($submenu as $smenu) {
                                $subothermenu = content::get_subothercontents_by_sub_id($smenu['subcontent_id']);
                                ?> 
                                <li>
                                    <a  href="sub_content.php?maincontent_id=<?php echo $smenu['maincontent_id']; ?>&subcontent_id=<?php echo $smenu['subcontent_id']; ?>"><?php echo $smenu['heading']; ?></a>
                                    <ul>
                                        <?php
                                        foreach ($subothermenu as $sothermenu) {
                                            $subotherothermenu = content::get_subother_otherContent_by_subotherID($sothermenu['subothercontent_id']);
                                            ?>
                                            <li>
                                                <a href="subother_content.php?subcontent_id=<?php echo $sothermenu['subcontent_id']; ?>&subothercontent_id=<?php echo $sothermenu['subothercontent_id']; ?>"><?php echo $sothermenu['heading']; ?></a>
                                                <ul >
                                                    <?php foreach ($subotherothermenu as $sotherother) { ?>
                                                        <li><a href="subother_other_content.php?maincontent_id=<?php echo $sotherother['maincontent_id']; ?>&subcontent_id=<?php echo $sotherother['subcontent_id']; ?>&subothercontent_id=<?php echo $sotherother['subothercontent_id']; ?>&subother_other_content_id=<?php echo $sotherother['subother_other_content_id']; ?>"><?php echo $sotherother['heading']; ?></a></li>
                                                    <?php } ?>

                                                    <li><a href="subother_other_content.php?maincontent_id=<?php echo $sothermenu['maincontent_id']; ?>&subcontent_id=<?php echo $sothermenu['subcontent_id']; ?>&subothercontent_id=<?php echo $sothermenu['subothercontent_id']; ?>"><i class="fa fa-plus"></i> &nbsp;&nbsp;Add New</a></li> 
                                                </ul>
                                            </li>
                                        <?php } ?>
                                        <li><a href="subother_content.php?subcontent_id=<?php echo $smenu['subcontent_id']; ?>"><i class="fa fa-plus"></i> &nbsp;&nbsp;Add New</a></li>
                                    </ul>
                                </li>
                            <?php } ?>
                            <li><a href="sub_content.php?maincontent_id=<?php echo $mmenu['maincontent_id']; ?>"><i class="fa fa-plus"></i> &nbsp;&nbsp;Add New</a></li>
                        </ul>   
                    </li>
                <?php } ?>
                <li><a href="mainmenu.php"> <i class="fa fa-plus"></i> &nbsp;&nbsp;Add New</a></li>
            </ul>
        </li>
    </ul>
</div>


 