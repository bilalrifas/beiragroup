<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-title"><i class="fa fa-th-list"></i>&nbsp;&nbsp;Sub Other Other Pages</div>
            </div>
            <div class="box-body">
                <?php if (sizeof($sub_otherothercontents) < 1) { ?>
                    <div class="alert"><strong>Sorry !</strong>&nbsp;&nbsp;&nbsp;No Data Found.</div>
                <?php } else { ?>
                    <table class="table table-condensed" style="width:100%;">
                        <thead>

                            <tr>
                                <th width="40%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >Name</th>
                                <th width="10%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >Added date</th>
                                <th width="10%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >Status</th>
                                <th width="20%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >&nbsp;</th>
                                <th width="12%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >Priority</th>
                                <th width="8%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >&nbsp;</th>
                            </tr>
                        </thead>  
                        <tbody>
                            <?php foreach ($sub_otherothercontents as $row) { ?> 

                                <tr>
                                    <td ><?php echo $row['heading']; ?></td>
                                    <td ><?php echo $row['added_date']; ?></td>
                                    <td >
                                        <div class="btn-group">
                                            <button class="btn btn-primary btn-flat"><?php
                                                if ($row['status'] == 1) {
                                                    echo "Active";
                                                } else {
                                                    echo "Inactive";
                                                }
                                                ?></button>
                                            <button class="btn btn-primary btn-flat dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <!-- dropdown menu links -->
                                                <li>
                                                    <?php if ($row['status'] == 1) {
                                                        ?>
                                                        <a href="<?php echo $_SERVER['PHP_SELF'] . '?action=change_content_status&status=0&subothercontent_id=' . $row['subothercontent_id'] . '&subotherothercontent_id=' . $row['subother_other_content_id']; ?>" onclick="return confirm('Are you sure to inactive this content?');" >Change to Inactive</a>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <a href="<?php echo $_SERVER['PHP_SELF'] . '?action=change_content_status&status=1&subothercontent_id=' . $row['subothercontent_id'] . '&subotherothercontent_id=' . $row['subother_other_content_id']; ?>"  onclick="return confirm('Are you sure to active this content?');" >Change to Active</a>
                                                    <?php } ?> 
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                    <td>
                                        <a href="subother_other_content.php?maincontent_id=<?php echo $maincontent_id; ?>&subcontent_id=<?php echo $row['subcontent_id']; ?>&subothercontent_id=<?php echo $row['subothercontent_id']; ?>&subother_other_content_id=<?php echo $row['subother_other_content_id']; ?>" class="btn btn-default btn-flat" ><i class="fa fa-edit"></i>&nbsp;&nbsp;Edit</a>
                                        &nbsp;
                                        <a href="<?php echo $_SERVER['PHP_SELF'] . '?action=delete_subotherothercontent&subothercontent_id=' . $row['subothercontent_id'] . '&subotherothercontent_id=' . $row['subother_other_content_id']; ?>" onclick="return confirm('Are you sure to delete this content?');" class=" btn btn-danger btn-flat" ><i class="fa fa-eraser"></i>&nbsp;&nbsp;Delete</a>	
                                    </td>
                                    <td>
                                        <select name="display_order[<?php echo $row['subother_other_content_id']; ?>]" class="form-control"  onchange="set_other_sub_order(this.value,<?php echo $row['subother_other_content_id']; ?>)" >
                                            <?php for ($j = 1; $j <= sizeof($sub_otherothercontents); $j++) { ?>
                                                <option value="<?php echo $j; ?>" <?php
                                                if ($row['display_order'] == $j) {
                                                    echo "selected";
                                                }
                                                ?>><?php echo $j; ?>
                                                </option>
                                            <?php } ?>
                                        </select>		
                                        <span id="status_sub<?php echo $row['subother_other_content_id']; ?>" class="label label-success flat"></span>
                                    </td>
                                    <td >&nbsp;</td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>    
                    <?php
                }
                ?>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-md-2 col-md-offset-10">
                        <button class="btn btn-default btn-flat btn-block btnback"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back</button>
                    </div>
                </div>   
            </div>
        </div>
    </div>
</div>

<script>
    function set_other_sub_order(value, subid) {
        var msgbox = $("#status_sub" + subid);
        $("#status_sub" + subid).html('<img src="images/loader.gif">');
        $.ajax({
            type: "POST",
            url: "set_sub_order.php",
            data: "subid=" + subid + "&value=" + value + "&action=subother_other",
            success: function(msg) {
                msgbox.html(msg);
                msgbox.hide();
                msgbox.fadeIn(1000);
                msgbox.fadeOut(1500);
            }
        });
    }
</script>
