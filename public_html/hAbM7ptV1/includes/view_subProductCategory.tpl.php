 <div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-title"><i class="fa fa-plus-square"></i>&nbsp;&nbsp;Add Sub Product Category</div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <label class="checkbox">
                        </label>
                        <a href="add_subProductCategory.php"><button type="button" name="add_subproduct" class="btn btn-primary btn-flat btn-block"><i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp;Add Sub Product Category</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-title"><i class="fa fa-th-list"></i>&nbsp;&nbsp;Sub Product Categories</div>
            </div>
            <div class="box-body">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" >

                    <tr>
                        <td colspan="3" align="center" >
                            <?php if (sizeof($content_link) < 1) { ?>
                                <div class="no_data"><?php echo "No Sub Product Categories"; ?></div>
                            <?php } else { ?>
                                <table width="100%" border="0" cellspacing="0" cellpadding="2" class="table table-hover">

                                    <tr class="table_heading">
                                    <thead>
                                        <tr class="table_heading">

                                            <th height="30" width="26%" align="left" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" ><strong>Name</strong></th>
                                            <th width="9%" align="left" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>"><strong>Status</strong></th>
                                            <th width="16%" align="left" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>"><strong>Update Date</strong></th>
                                            <th align="left" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>">&nbsp;</th>
                                            <th width="15%" align="left" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>"><strong>Display Order</strong></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 0;
                                        foreach ($content_link as $row) {
                                            ?>

                                            <tr <?php
                                            if (($i % 2) == 0) {
                                                echo "bgcolor='#FAFAFA'";
                                            } else {
                                                echo 'bgcolor="#FFFFFF"';
                                            }
                                            ?>>
                                                <td align="left"  class="small_f1"><span style="width:100px; margin-right: 10px; text-align: left; color:blueviolet;"></span><?php echo $row['heading']; ?></td>
                                                <td align="left"  class="small_f1">
                                                    <?php
                                                    if ($row['status'] == 1) {
                                                        echo "active";
                                                    } else {
                                                        echo "inactive";
                                                    }
                                                    ?>
                                                </td>
                                                <td align="left"  class="small_f1"><?php echo $row['update_date']; ?></td>


                                                <td align="left" valign="top">
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-primary btn-flat">Action</button>
                                                            <button type="button" class="btn btn-primary btn-flat dropdown-toggle" data-toggle="dropdown">
                                                                <span class="caret"></span>
                                                                <span class="sr-only">Toggle Dropdown</span>
                                                            </button>
                                                            <ul class="dropdown-menu" role="menu">
                                                                <?php if ($row['status'] != 1) { ?>
                                                                    <li><a href=<?php echo $_SERVER['PHP_SELF'] . '?cat_id='.$cat_id.'&title='.$cat_title.'&action=status_change&active=1&id=' . $row['id'] . ''; ?> onclick="return confirm('Are you sure to active this Menu Item?');">Active</a></li>
                                                                <?php } else { ?>
                                                                     <li><a href=<?php echo $_SERVER['PHP_SELF'] . '?cat_id='.$cat_id.'&title='.$cat_title.'&action=status_change&active=0&id=' . $row['id'] . ''; ?> onclick="return confirm('Are you sure to inactive this Menu Item?');">Inactive</a></li>
                                                                <?php } ?>
                                                            </ul>
                                                        </div>
                                                    &nbsp;
                                                    <a class="btn btn-danger btn-flat" href=<?php echo $_SERVER['PHP_SELF'] . '?cat_id='.$cat_id.'&title='.$cat_title.'&action=delete_subcategory&id=' . $row['id'] . ''; ?> onClick="return confirm('Are you sure to Delete this Menu Item?');"><i class="fa fa-eraser"></i>&nbsp;&nbsp;Delete</a>&nbsp;
                                                    <a data-toggle="modal" href="add_subProductCategory.php?id=<?php echo $row["id"]; ?>" class="btn btn-success"><i class="fa fa-pencil-square-o"></i>&nbsp;&nbsp;&nbsp;Edit</a>
                                                </td>
                                                <td align="left" valign="top">
                                                    <select name="display_order[<?php echo $row['id']; ?>]"  class="form-control" onchange="setprocat_order(this.value,<?php echo $row['id']; ?>)"  style="width: 60px" >
                                                        <?php for ($j = 1; $j <= sizeof($content_link); $j++) { ?>
                                                            <option value="<?php echo $j; ?>" <?php
                                                                    if ($row['display_order'] == $j) {
                                                                        echo "selected";
                                                                    }
                                                                    ?>><?php echo $j; ?></option>
                                                                    <?php } ?>
                                                    </select>
                                                    <span id="status_procat_<?php echo $row['id']; ?>" class="label label-success flat"></span>

                                                </td>
                                            </tr>

                                            <?php
                                            $i++;
                                        }
                                        ?>
                                        <tr>
                                            <td height="39" colspan="7" align="right" valign="middle"></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>


                                <tr>
                                    <td bgcolor="#FFFFFF">&nbsp;</td>
                                    <td colspan="5" bgcolor="#FFFFFF">&nbsp;</td>

                                    <td width="2%" bgcolor="#FFFFFF">&nbsp;</td>
                                </tr>
                            </table></td>
                    </tr>
                </table>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-md-2 col-md-offset-10">
                        <button class="btn btn-default btn-flat btn-block btnback"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function setprocat_order(value, id) {
        var msgbox = $("#status_procat_" + id);
        $("#status_procat_" + id).html('<img src="img/ajax-loader.gif">');
        $.ajax({
            type: "POST",
            url: "set_sub_order.php",
            data: "id=" + id + "&value=" + value + "&action=prosubcat",
            success: function(msg) {
                msgbox.html(msg);
                msgbox.hide();
                msgbox.fadeIn(1000);
                msgbox.fadeOut(1500);
            }

        });

    }
</script>