<?php

require_once('admin.php');
//-----------permissions-------
/*
if ($_SESSION['admin']['user_level'] != 1) {
    $permission = check_for_permissions(3);
    if ($permission == 0) {
        header('location:admin_home.php?msg=' . base64_encode(4) . '');
        exit;
    }
}
*/

$per_tag = new Permission;
$per_tag->premission_tag = "mainmenu";
$permission_block = $per_tag->check_permission($db);
if (!$permission_block) {
    $INCLUDE_FILE = "permission_denied.php";
    require_once('template_main.php');
    exit();
}

$err = "";
$title = "";



if ($_POST == true) {
    $err = "";
    $title = trim($_POST['title']);

    if ($title == "") {
        $err = $err . "Please enter title";
    }

    if ($err == "") {
        $data_arr = array();
        $data_arr['heading'] = $title;


        if (isset($_POST['btnadd']) == true) {
            $data_arr['status'] = 1;
            $data_arr['updated_date'] = date('Y-m-d');

            $insert_id = $db->query_insert("tblmain_content", $data_arr);
            if ($insert_id) {
                //**************** generate log entry *******************
                $logString = "Add Main Menu Item - " . $title . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************
                header("Location:mainmenu.php?msg=" . base64_encode(7) . "");
                exit;
            } else {
                $err = $err . "Not inserted";
            }
        }
    }
}

if (isset($_GET['action'])) {

    switch ($_GET['action']) {

        case 'status_change':

            $active = $_GET['active'];
            $id = $_GET['id'];

            $data = array();
            $data['status'] = $active;
            $result = $db->query_update("tblmain_content", $data, "maincontent_id=" . $id);

            if ($result) {
                //**************** generate log entry *******************
                $logString = "Change Menu status - User ID = " . $user_id . " / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************
                header('location:' . $_SERVER['PHP_SELF'] . '?msg=' . base64_encode(6) . '');
                exit;
            } else {
                header('location:' . $_SERVER['PHP_SELF'] . '?msg=' . base64_encode(5) . '');
                exit;
            }

            break;

        case 'delete_menu':

            $id = $_GET['id'];

            $result = $db->query("DELETE FROM tblmain_content WHERE maincontent_id =" . $id . "");
            $result = $db->query("DELETE FROM tblsub_content WHERE maincontent_id =" . $id . "");
            $result = $db->query("DELETE FROM tblsubother_content WHERE maincontent_id =" . $id . "");
            $result = $db->query("DELETE FROM tblsubother_other_content WHERE maincontent_id =" . $id . "");

            //**************** generate log entry *******************
            $logString = "Deleted menu - User ID = " . $user_id . " / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
            $log = Message::log_details($_SESSION['admin']['username'], $logString);
            // **************************************************

            header('location:' . $_SERVER['PHP_SELF'] . '?msg=' . base64_encode(8) . '');
            exit;
            break;
    }
}

$content_link = $db->fetch_all_array("SELECT maincontent_id,heading,status,updated_date FROM tblmain_content ORDER BY maincontent_id  ASC");

$page_main_heading = '<i class="fa fa-list-alt"></i>&nbsp;&nbsp;'.'Manage Site Content';

$breaddrum = "<li class='active'>Add Main Menu Item</li>";
$INCLUDE_FILE = "includes/mainmenu.tpl.php";
require_once('template_main.php');
?>