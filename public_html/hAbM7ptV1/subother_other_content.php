<?php

require_once('admin.php');
$per_tag = new Permission;
$per_tag->premission_tag = "manage_subother_other_content";
$permission_block = $per_tag->check_permission($db);
if (!$permission_block) {
    $INCLUDE_FILE = "permission_denied.php";
    require_once('template_main.php');
    exit();
}
//------------------

$err = "";


$subcontent_id = "";
$details = "";
$spe_details = "";
$heading = "";
$spe_heading = "";
//meta data
$meta_title = "";
$meta_desc = "";
$meta_key = "";
$image1 = "";
$image2 = "";

if (isset($_GET) && ($_GET['subothercontent_id'] != "") && (is_numeric($_GET['subothercontent_id']))) {

    $sub_other_content = $db->query_first("SELECT * FROM tblsubother_content WHERE subothercontent_id ='" . $_GET['subothercontent_id'] . "'");
    if ($sub_other_content) {
        //

        $subothercontent_id = $sub_other_content['subothercontent_id'];
        $sub_other_header = $sub_other_content['heading'];

        //get_subcontent_by_id
        $subcontent_id = $_GET['subcontent_id'];
        $sub_content = $db->query_first("SELECT * FROM tblsub_content WHERE subcontent_id ='" . $subcontent_id . "'");

        if ($sub_content) {
            $subcontent_id = $sub_content['subcontent_id'];
            $subheading = $sub_content['heading'];

            //get_maincontent_by_id
            $main_content = $db->query_first("SELECT * FROM tblmain_content WHERE maincontent_id ='" . $sub_content['maincontent_id'] . "'");

            $maincontent_id = $main_content['maincontent_id'];
            $mainheading = $main_content['heading'];

            if ((isset($_GET['subother_other_content_id'])) && (is_numeric($_GET['subother_other_content_id']))) {

                //get_subothercontent_by_id
                $subother_other_content = $db->query_first("SELECT * FROM tblsubother_other_content WHERE subother_other_content_id ='" . $_GET['subother_other_content_id'] . "'");

                if ($subother_other_content) {
                    $subother_othercontent_id = $subother_other_content['subother_other_content_id'];
                    $details = $subother_other_content['details'];
                    $heading = $subother_other_content['heading'];
                    $spe_heading = $subother_other_content['spe_heading'];
                    $spe_details = $subother_other_content['spe_details'];
                    $meta_title = $subother_other_content['meta_title'];
                    $meta_desc = $subother_other_content['meta_desc'];
                    $meta_key = $subother_other_content['meta_key'];
                    $image1 = $subother_other_content['image1'];
                    $image2 = $subother_other_content['image2'];
                } else {
                    header('location:dashboard.php');
                    exit;
                }
            }
        } else {
            header('location:dashboard.php');
            exit;
        }
    } else {
        header('location:dashboard.php');
        exit;
    }
} else {
    header('location:dashboard.php');
    exit;
}

if ($_POST == true) {

    $err = "";

    $subcontent_id = $_POST['subcontent_id'];
    $maincontent_id = $_POST['maincontent_id'];
    $subother_content_id = $_POST['sub_othercontent_id'];

    $heading = trim($_POST['heading']);
    $spe_heading = trim($_POST['spe_heading']);
    $details = $_POST['details'];
    $spe_details= $_POST['spe_details'];
    $meta_title = trim($_POST['meta_title']);
    $meta_desc = trim($_POST['meta_desc']);
    $meta_key = trim($_POST['meta_key']);

    if ($heading == "") {
        $err = $err . "<li>Please enter title</li>";
    }

    if ($err == "") {

        $data_arr = array();
        $data_arr['maincontent_id'] = $maincontent_id;
        $data_arr['subcontent_id'] = $subcontent_id;
        $data_arr['subothercontent_id'] = $subother_content_id;
        $data_arr['heading'] = $heading;
        $data_arr['spe_heading'] = $spe_heading;
        $data_arr['details'] = $details;
        $data_arr['spe_details'] = $spe_details;
        $data_arr['meta_title'] = $meta_title;
        $data_arr['meta_desc'] = $meta_desc;
        $data_arr['meta_key'] = $meta_key;

        if (isset($_FILES) && $_FILES['image1']['name'] != "") {

            $upload_image1 = upload::upload_images(DOC_ROOT . 'images/', $_FILES['image1']);

            if ($upload_image1) {
                $data_arr['image1'] = $upload_image1;
            }
        }

        if (isset($_FILES) && $_FILES['image2']['name'] != "") {

            $upload_image2 = upload::upload_images(DOC_ROOT . 'images/', $_FILES['image2']);

            if ($upload_image2) {
                $data_arr['image2'] = $upload_image2;
            }
        }

        if (isset($_POST['btnedit'])) {

            $subotherothercontent_id = $_POST['subotherothercontent_id'];

            $data_arr['updated_date'] = date('Y-m-d');

            $update = $db->query_update("tblsubother_other_content", $data_arr, "subother_other_content_id=" . $subotherothercontent_id);
            //**************** generate log entry *******************
            $logString = "update sub other Other content, subother other content_id -  " . $subotherothercontent_id . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
            $log = Message::log_details($_SESSION['admin']['username'], $logString);
            // **************************************************
            if ($update) {
                header("Location:subother_other_content.php?maincontent_id=".$maincontent_id."&subcontent_id=" . $subcontent_id . "&subothercontent_id=" . $_GET['subothercontent_id'] . "&subother_other_content_id=" . $subotherothercontent_id ."&msg=" . base64_encode(6) . "");
            } else {
                $err = "<li>Not updated</li>";
            }
        } else if (isset($_POST['btnadd'])) {

            $data_arr['added_date'] = date('Y-m-d');
            $data_arr['updated_date'] = date('Y-m-d');

            $subotherothercontent_id = $db->query_insert("tblsubother_other_content", $data_arr);
            //**************** generate log entry *******************
            $logString = "Insert sub other other content, subotherothercontent_id -  " . $subotherothercontent_id . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
            $log = Message::log_details($_SESSION['admin']['username'], $logString);
            // **************************************************
            if ($subotherothercontent_id) {
                header("Location:subother_other_content.php?maincontent_id=" . $maincontent_id . "&subcontent_id=" . $subcontent_id . "&subothercontent_id=" . $_GET['subothercontent_id'] . "&msg=" . base64_encode(7) . "");
            } else {
                $err = "<li>Not inserted</li>";
            }
        } else {
            header('location:dashboard.php');
            exit;
        }
    }
}


if (isset($_GET) && isset($_GET['action'])) {

    switch ($_GET['action']) {

        case 'delete_subotherothercontent_image':

            if (isset($_GET['subother_other_content_id']) && isset($_GET['subothercontent_id']) && isset($_GET['image']) && isset($_GET['imageno'])) {

                $subotherothercontent_id = $_GET['subother_other_content_id'];
                $subothercontent_id = $_GET['subothercontent_id'];
                $image = $_GET['image'];

                $data = array();
                $data[$_GET['imageno']] = "";

                $result = $db->query_update("tblsubother_other_content", $data, "subother_other_content_id=" . $subotherothercontent_id);

                if (file_exists(DOC_ROOT . 'images/' . $image)) {
                    $unlink = @unlink(DOC_ROOT . 'images/' . $image);
                }

                //**************** generate log entry *******************
                $logString = "delete subother other content image, subothercontent_id - " . $subothercontent_id . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = new Logging();
                $log->lwrite($logString);
                // **************************************************

                if ($result) {
                    header("Location:" . $_SERVER['PHP_SELF'] . "?maincontent_id=" . $maincontent_id . "&subcontent_id=" . $subcontent_id . "&subothercontent_id=" . $subothercontent_id ."&subother_other_content_id=".$subotherothercontent_id . "&msg=" . base64_encode(8) . "");
                    exit;
                } else {
                    header("Location:" . $_SERVER['PHP_SELF'] . "?maincontent_id=" . $maincontent_id . "&subcontent_id=" . $subcontent_id . "&subothercontent_id=" . $subothercontent_id ."&subother_other_content_id=".$subotherothercontent_id . "&msg=" . base64_encode(5) . "");
                    exit;
                }
            }
            break;
    }
}
if ((isset($_GET['subother_other_content_id'])) && (is_numeric($_GET['subother_other_content_id']))) {
    $pagename = "Update Sub Other Other content Details";
} else {
    $pagename = "Add Sub Other Other content Details";
}
//$breaddrum = " <a href='admin_home.php' class='breaddrum'>Home </a> >>  Content Management  >> <a href='main_content.php?maincontent_id=$maincontent_id'  class='breaddrum'> $mainheading </a> >> <a href='main_content.php?maincontent_id=$maincontent_id'  class='breaddrum'> $subheading </a> >> $type";
$page_main_heading = '<i class="fa fa-list-alt"></i>&nbsp;&nbsp;' . 'Manage Site Content';
$breaddrum = "<li><a href='main_content.php?maincontent_id=$maincontent_id'> $mainheading </a></li><li><a href='sub_content.php?maincontent_id=$maincontent_id&subcontent_id=$subcontent_id'> $subheading </a></li><li><a href='subother_content.php?subcontent_id=$subcontent_id&subothercontent_id=$subothercontent_id'> $sub_other_header </a></li><li class='active'>$pagename</li>";

$INCLUDE_FILE = "includes/subother_other_content.tpl.php";

require_once('template_main.php');
?>
