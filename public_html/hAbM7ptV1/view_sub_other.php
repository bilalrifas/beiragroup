<?php
require_once('admin.php');
$per_tag                                = new Permission;
$per_tag->premission_tag                = "manage_sub_content";
$permission_block			= $per_tag->check_permission($db);
if(!$permission_block){	
	$INCLUDE_FILE = "permission_denied.php";
	require_once('template_main.php');
	exit();
}

if(isset($_GET['subcontent_id'])&&$_GET['subcontent_id']!=''){
      $subcontentid=$_GET['subcontent_id'];
      $subother_contents=$db->fetch_all_array("SELECT * FROM tblsubother_content WHERE subcontent_id=".$subcontentid." ORDER BY display_order ASC");
} else {
    header('location:dashboard.php');
    exit;
}

if(isset($_GET) && isset($_GET['action']) ) {

	switch($_GET['action']){
		
		case 'change_subothercontent_status':
                        
                    //var_dump($_GET);
                    //die();
                        $status                 = $_GET['status'];
                        $maincontent_id         = $_GET['maincontent_id'];
                        $subcontent_id          = $_GET['subcontent_id'];
                        $subothercontent_id	= $_GET['subothercontent_id'];
                        
                        //$sub_content=$db->query_first("SELECT subcontent_id FROM tblsubother_content WHERE subothercontent_id ='".$subothercontent_id."'");
                        
                        $data = array();
                        $data['status'] 	= $status;
                        $result = $db->query_update("tblsubother_content", $data, "subothercontent_id=".$subothercontent_id);

                        if($result){
                                 //**************** generate log entry *******************
                                                $logString = "Change subothercontent status - subothercontent ID= ".$subothercontent_id." / USER - ".$_SESSION['admin']['username']." ID - ".$_SESSION['admin']['id'];
                                                $log = Message::log_details($_SESSION['admin']['username'],$logString);
                                // **************************************************			
                                header('location:view_sub_other.php?subcontent_id='.$subcontent_id.'&msg='.base64_encode(6).'');
                                exit;
                        }else{
                                header('location:view_sub_other.php?subcontent_id='.$subcontent_id.'&msg='.base64_encode(5).'');
                                exit;
                        }	
                break;

                case 'delete_subothercontent':

                        $maincontent_id         = $_GET['maincontent_id'];
                        $subothercontent_id	= $_GET['subothercontent_id'];
                        $subcontent_id          = $_GET['subcontent_id'];
                        
                        
                        $result = $db->query("DELETE FROM tblsubother_content WHERE subothercontent_id =".$subothercontent_id."");

                        if($result){
                                 //**************** generate log entry *******************
                                                $logString = "Delete subothercontent - subothercontent ID= ".$subothercontent_id." / USER - ".$_SESSION['admin']['username']." ID - ".$_SESSION['admin']['id'];
                                                $log = Message::log_details($_SESSION['admin']['username'],$logString);
                                // **************************************************			
                                header('location:view_sub_other.php?maincontent_id='.$maincontent_id.'&subcontent_id='.$subcontent_id.'&msg='.base64_encode(8).'');
                                exit;
                        }else{
                                header('location:view_sub_other.php?maincontent_id='.$maincontent_id.'&subcontent_id='.$subcontent_id.'&msg='.base64_encode(5).'');
                                exit;
                        }	
                break;
                
                
	}

}

$page_main_heading = '<i class="fa fa-list-alt"></i>&nbsp;&nbsp;'.'Manage Site Content';
$breaddrum = "<li class='active'>view All Sub pages</li>";

$INCLUDE_FILE = "includes/view_sub_other.tpl.php";

require_once('template_main.php');
?>
