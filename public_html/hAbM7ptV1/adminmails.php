<?php

require_once('admin.php');
$per_tag = new Permission;
$per_tag->premission_tag = "manage_admin_emails";
$permission_block = $per_tag->check_permission($db);
if (!$permission_block) {
    $INCLUDE_FILE = "permission_denied.php";
    require_once('template_main.php');
    exit();
}

$name = "";
$email = "";
$status = "";
//$display_order="";

if (isset($_GET['id'])) {
    $director_data = $db->query_first("SELECT * FROM tbladminemails WHERE id=" . $_GET['id']);
    $id = $_GET['id'];
    $name = $director_data['name'];
    $email = $director_data['email'];
    // $display_order =   $director_data['display_order'];  
}

if ($_POST == true) {
    $err = "";
    $name = trim($_POST['name']);
    $email = trim($_POST['email']);
    
    if ($name == "") {
        $err = $err . "<li>Please enter Name</li>";
    }

    if ($err == "") {

        $data_arr = array();
        $data_arr['name'] = $name;
        $data_arr['email'] = $email;
        
        if (isset($_POST['btnsave'])) {
            $data_arr['activated'] = 1;
            $data_arr['time_added'] = date("Y-m-d h:m:s");
            $imagez = $db->query_insert("tbladminemails", $data_arr);
            //**************** generate log entry *******************
            $logString = "Add new Email sender, id -  " . $imagez . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
            $log = Message::log_details($_SESSION['admin']['username'], $logString);
            // **************************************************
            if ($imagez) {
                header("Location:adminmails.php?&msg=" . base64_encode(7) . "");
            } else {
                $err = "<li>Not updated</li>";
            }
        }

        if (isset($_POST['btnedit'])) {
            $id = $_POST['id'];
            $result = $db->query_update("tbladminemails", $data_arr, "id=" . $id);
            if ($result) {
                //**************** generate log entry *******************
                $logString = "Update mail content, id -  " . $id . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************
                header("Location:adminmails.php?&msg=" . base64_encode(7) . "");
            } else {
                $err = "<li>Not updated</li>";
            }
        }
    }
}

$all_mails = $db->fetch_all_array("SELECT * FROM tbladminemails");
$params = array(
    'mode' => 'Sliding',
    'perPage' => 10,
    'delta' => 1,
    'itemData' => $all_mails
);
$pager = & Pager::factory($params);
$mails = $pager->getPageData();

if (isset($_GET) && isset($_GET['action'])) {

    switch ($_GET['action']) {

        case 'status':

            $status = $_GET['val'];
            $id = $_GET['id'];

            $data = array();
            $data['activated'] = $status;
            $result = $db->query_update("tbladminemails", $data, "id=" . $id);

            if ($result) {
                //**************** generate log entry *******************
                $logString = "Change status - testimonials ID= " . $f_id . " / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************			
                header('location:' . $_SERVER['PHP_SELF'] . '?msg=' . base64_encode(6) . '');
                exit;
            } else {
                header('location:' . $_SERVER['PHP_SELF'] . '?err=' . base64_encode(5) . '');
                exit;
            }
            break;

        case 'delete':
            $id = $_GET['id'];

            $result = $db->query("DELETE FROM tbladminemails WHERE id =" . $id . "");
            //echo "DELETE FROM tblroom_types WHERE id =".$room_id."";
            if ($result) {
                //**************** generate log entry *******************
                $logString = "Delete data ID= " . $t_id . " / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************			
                header('location:' . $_SERVER['PHP_SELF'] . '?&msg=' . base64_encode(8) . '');
                exit;
            } else {
                header('location:' . $_SERVER['PHP_SELF'] . '?msg=' . base64_encode(5) . '');
                exit;
            }
            break;
    }
}

//$breaddrum = " <a href='admin_home.php' class='breaddrum'>Home </a> >>  Content Management  >> <a href='main_content.php?maincontent_id=$maincontent_id'  class='breaddrum'> $mainheading </a> >> <a href='main_content.php?maincontent_id=$maincontent_id'  class='breaddrum'> $subheading </a> >> $type";
$breaddrum = "</li><li class='active'>Amin Mails</li>";
$page_main_heading = '<i class="fa fa-wrench"></i>&nbsp;&nbsp;'.'Administrative';
$INCLUDE_FILE = 'includes/adminmails.tpl.php';

require_once('template_main.php');
?>


