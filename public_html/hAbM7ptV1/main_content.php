<?php

require_once('admin.php');
$per_tag = new Permission;
$per_tag->premission_tag = "modify_main_content";
$permission_block = $per_tag->check_permission($db);
if (!$permission_block) {
    $INCLUDE_FILE = "permission_denied.php";
    require_once('template_main.php');
    exit();
}


$err = "";
$maincontent_id = "";
$spe_details = "";
$details = "";
$spe_heading = "";
$heading = "";
//meta data
$meta_title = "";
$meta_desc = "";
$meta_key = "";
$image1 = "";
$image2 = "";

if (isset($_GET) && isset($_GET['action'])) {

    switch ($_GET['action']) {

        case 'change_content_status':

            $status = $_GET['status'];
            $maincontent_id = $_GET['maincontent_id'];
            $subcontent_id = $_GET['subcontent_id'];

            $data = array();
            $data['status'] = $status;
            $result = $db->query_update("tblsub_content", $data, "subcontent_id=" . $subcontent_id);

            if ($result) {
                //**************** generate log entry *******************
                $logString = "Change subcontent status - subcontent ID= " . $subcontent_id . " / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************
                header('location:' . $_SERVER['PHP_SELF'] . '?maincontent_id=' . $maincontent_id . '&msg=' . base64_encode(6) . '');
                exit;
            } else {
                header('location:' . $_SERVER['PHP_SELF'] . '?maincontent_id=' . $maincontent_id . '&msg=' . base64_encode(5) . '');
                exit;
            }
            break;

        case 'change_sliderimage_status':

            $status = $_GET['status'];
            $maincontent_id = $_GET['maincontent_id'];
            $image_id = $_GET['image_id'];

            $data = array();
            $data['status'] = $status;
            $result = $db->query_update("tblslider_images", $data, "image_id=" . $image_id);

            if ($result) {
                //**************** generate log entry *******************
                $logString = "Change Slider Image status - image_id= " . $image_id . " / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************
                header('location:' . $_SERVER['PHP_SELF'] . '?maincontent_id=' . $maincontent_id . '&msg=' . base64_encode(6) . '');
                exit;
            } else {
                header('location:' . $_SERVER['PHP_SELF'] . '?maincontent_id=' . $maincontent_id . '&msg=' . base64_encode(5) . '');
                exit;
            }
            break;

        case 'delete_subcontent':
            $maincontent_id = $_GET['maincontent_id'];
            $subcontent_id = $_GET['subcontent_id'];

            $result = $db->query("DELETE FROM tblsub_content WHERE subcontent_id =" . $subcontent_id . "");

            if ($result) {
                //**************** generate log entry *******************
                $logString = "Delete subcontent - subcontent ID= " . $subcontent_id . " / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************
                header('location:' . $_SERVER['PHP_SELF'] . '?maincontent_id=' . $maincontent_id . '&msg=' . base64_encode(8) . '');
                exit;
            } else {
                header('location:' . $_SERVER['PHP_SELF'] . '?maincontent_id=' . $maincontent_id . '&msg=' . base64_encode(5) . '');
                exit;
            }
            break;



        case 'delete_content_image':

            if (isset($_GET['maincontent_id']) && isset($_GET['image']) && isset($_GET['imageno'])) {

                $maincontent_id = $_GET['maincontent_id'];
                $image = $_GET['image'];

                $data[$_GET['imageno']] = "";

                $result = $db->query_update("tblmain_content", $data, "maincontent_id=" . $maincontent_id);

                if (file_exists(DOC_ROOT . 'images/content/banner/' . $image)) {
                    $unlink = @unlink(DOC_ROOT . 'images/content/banner/' . $image);
                }

                //**************** generate log entry *******************
                $logString = "delete content image, maincontent_id - " . $maincontent_id . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************

                if ($result) {
                    header('location:' . $_SERVER['PHP_SELF'] . '?maincontent_id=' . $maincontent_id . '&msg=' . base64_encode(8) . '');
                    exit;
                } else {
                    header('location:' . $_SERVER['PHP_SELF'] . '?maincontent_id=' . $maincontent_id . '&msg=' . base64_encode(5) . '');
                    exit;
                }
            }
            break;

        case 'delete_slider_image':

            if (isset($_GET['maincontent_id']) && isset($_GET['image']) && isset($_GET['image_id'])) {

                $maincontent_id = $_GET['maincontent_id'];
                $image = $_GET['image'];
                $image_id=$_GET['image_id'];

                $result = $db->query("DELETE FROM tblslider_images WHERE image_id =" . $image_id . "");

                if (file_exists(DOC_ROOT . 'images/content/slider/' . $image)) {
                    $unlink = @unlink(DOC_ROOT . 'images/content/slider/' . $image);
                }

                //**************** generate log entry *******************
                $logString = "delete slider image, image_id - " . $image_id . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************

                if ($result) {
                    header('location:' . $_SERVER['PHP_SELF'] . '?maincontent_id=' . $maincontent_id . '&msg=' . base64_encode(8) . '');
                    exit;
                } else {
                    header('location:' . $_SERVER['PHP_SELF'] . '?maincontent_id=' . $maincontent_id . '&msg=' . base64_encode(5) . '');
                    exit;
                }
            }
            break;
    }
}


if (isset($_GET) && ($_GET['maincontent_id'] != "") && (is_numeric($_GET['maincontent_id']))) {

    $maincontent_id = $_GET['maincontent_id'];

    $main_content = $db->query_first("SELECT * FROM tblmain_content WHERE maincontent_id ='" . $maincontent_id . "'");

    if ($main_content) {
        //heading 	 	details
        $maincontent_id = $main_content['maincontent_id'];
        $details = $main_content['details'];
        $heading = $main_content['heading'];
        $spe_details = $main_content['spe_details'];
        $spe_heading = $main_content['spe_heading'];
        $meta_title = $main_content['meta_title'];
        $meta_desc = $main_content['meta_desc'];
        $meta_key = $main_content['meta_key'];
        $image1 = $main_content['image1'];
        $image2 = $main_content['image2'];
        $image3 = $main_content['image3'];
        $sub_contents = $db->fetch_all_array("SELECT * FROM tblsub_content WHERE maincontent_id=$maincontent_id ORDER BY display_order ASC");
        $slider_image_array=$db->fetch_all_array("SELECT * FROM tblslider_images WHERE maincontent_id=$maincontent_id ORDER BY display_order ASC");
    } else {
        header('location:admin_home.php');
        exit;
    }
} else {
    header('location:admin_home.php');
    exit;
}

if ($_POST == true) {

    if (isset($_POST['btnsetsuborder'])) {

        $display = array();
        foreach ($_POST['display_order'] as $subid => $value) {
            $display['display_order'] = $value;
            $set = $db->query_update("tblsub_content", $display, "subcontent_id=" . $subid);
        }

        header("Location:main_content.php?maincontent_id=" . $maincontent_id . "&msg=" . base64_encode(6) . "");
        exit;
    }


    if (isset($_POST['btnsave'])) {

        $err = "";

        $maincontent_id = $_POST['maincontent_id'];
        $heading = trim($_POST['heading']);
        $details = $_POST['details'];
        $spe_heading = trim($_POST['spe_heading']);
        $spe_details = $_POST['spe_details'];
        $meta_title = trim($_POST['meta_title']);
        $meta_desc = trim($_POST['meta_desc']);
        $meta_key = trim($_POST['meta_key']);

        if ($heading == "") {
            $err = $err . "<li>Please enter title</li>";
        }

        if ($err == "") {

            $data_arr = array();
            $data_arr['maincontent_id'] = $maincontent_id;
            $data_arr['heading'] = $heading;
            $data_arr['details'] = $details;

            $data_arr['spe_heading'] = $spe_heading;
            $data_arr['spe_details'] = $spe_details;

            $data_arr['meta_title'] = $meta_title;
            $data_arr['meta_desc'] = $meta_desc;
            $data_arr['meta_key'] = $meta_key;
            $data_arr['updated_date'] = date('Y-m-d');

            if (isset($_FILES) && $_FILES['image1']['name'] != "") {

                $temp_upload_image = upload::upload_images(DOC_ROOT . 'images/content/banner/', $_FILES['image1']);

                $imagename = str_replace(" ", "", $temp_upload_image);

                $temp_image_url = DOC_ROOT . 'images/content/banner/' . $temp_upload_image;
                $new_image_url = DOC_ROOT . 'images/content/banner/' . $imagename;

                rename($temp_image_url, $new_image_url);

                $data_arr['image1'] = $imagename;
            }

            if (isset($_FILES) && $_FILES['image2']['name'] != "") {

                $upload_image2 = upload::upload_images(DOC_ROOT . 'images/content/banner/', $_FILES['image2']);

                $imagename2 = str_replace(" ", "", $upload_image2);

                $temp_image_url = DOC_ROOT . 'images/content/banner/' . $upload_image2;
                $new_image_url = DOC_ROOT . 'images/content/banner/' . $imagename2;

                rename($temp_image_url, $new_image_url);

                $data_arr['image2'] = $imagename2;
            }

            if (isset($_FILES) && $_FILES['image3']['name'] != "") {


                $upload_image3 = upload::upload_images(DOC_ROOT . 'images/content/banner/', $_FILES['image3']);

                $imagename3 = str_replace(" ", "", $upload_image3);

                $temp_image_url = DOC_ROOT . 'images/content/banner/' . $upload_image3;
                $new_image_url = DOC_ROOT . 'images/content/banner/' . $imagename3;

                rename($temp_image_url, $new_image_url);

                $data_arr['image3'] = $imagename3;
            }

            $update = $db->query_update("tblmain_content", $data_arr, "maincontent_id=" . $maincontent_id);

            //**************** generate log entry *******************
            $logString = "Update content, id -  " . $maincontent_id . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
            $log = Message::log_details($_SESSION['admin']['username'], $logString);
            // **************************************************

            if ($update) {
                header("Location:main_content.php?maincontent_id=" . $maincontent_id . "&msg=" . base64_encode(6) . "");
            } else {
                $err = "<li>Not updated</li>";
            }
        }
    }
}


$temp_heading = "Add Main Menu Item";
$page_main_heading = '<i class="fa fa-list-alt"></i>&nbsp;&nbsp;' . 'Manage Site Content';
$INCLUDE_FILE = "includes/main_content.tpl.php";

$breaddrum = '<li class="active">Add New Item</li>';

require_once('template_main.php');
?>

