<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>
            <?php
            if (isset($companyname) && !empty($companyname)): echo $companyname . " Administration Panel";
            else: echo 'Administration Panel';
            endif;
            ?>
        </title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <link href="css/bootstrap.icon-large.min.css" rel="stylesheet" type="text/css" />

        <link href="css/ekko-lightbox.min.css" rel="stylesheet" type="text/css" />

        <link href="css/style.css" rel="stylesheet" type="text/css" />

        <link href="css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <?php include_once 'header.php'; ?>

        <div class="wrapper row-offcanvas row-offcanvas-left">
            <?php include_once 'side_panel.php'; ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <?php if(isset($page_main_heading)): echo $page_main_heading; endif;?>
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="dashboard.php"><i class="fa fa-dashboard"></i>Home</a></li>
                        <?php if(isset($breaddrum)&& !empty($breaddrum)):echo $breaddrum;endif; ?>
                        <!--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Blank page</li>-->
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <?php
                    if ($err != '') {
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <?php echo $err; ?>
                        </div>
                        <?php
                    } else if ($msg != '') {
                        ?>
                        <div class="alert alert-success alert-dismissable">
                            <i class="fa fa-check"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <?php echo $msg; ?>
                        </div>
                        <?php
                    } else if ($info != '') {
                        ?>
                        <div class="alert alert-info alert-dismissable">
                            <i class="fa fa-info"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <?php echo $info; ?>
                        </div>
                        <?php
                    } else if ($war != '') {
                        ?>
                        <div class="alert alert-warning alert-dismissable">
                            <i class="fa fa-warning"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <?php echo $war; ?>
                        </div>
                        <?php
                    }
                    ?>

                    <?php
                    if ($INCLUDE_FILE != '') {
                        require_once $INCLUDE_FILE;
                    }
                    ?>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->


        <!-- jQuery 2.0.2 -->
        <script src="js/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>

        <script src="js/jquery.validate.min.js" type="text/javascript"></script>

        <script src="js/jquery.cookie.js" type="text/javascript"></script>

        <!--<script src="js/additional-methods.min.js" type="text/javascript"></script>-->
        <script type="text/javascript" src="js/tinymce/js/tinymce/tinymce.min.js"></script>

        <script type="text/javascript" src="js/ekko-lightbox.min.js"></script>

        <script type="text/javascript" src="js/jasny-bootstrap.min.js"></script>

        <script src="js/script.js" type="text/javascript"></script>
    </body>
</html>