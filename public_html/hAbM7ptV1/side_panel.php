<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="img/avatar5.png" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>Hello, <?php
                    if (isset($_SESSION['admin']['username'])) {
                        echo ucfirst($_SESSION['admin']['username']);
                    }
                    ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
                    <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="active">
                <a href="dashboard.php">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li>
                <a href="maindata.php">
                    <i class="fa fa-inbox"></i> <span>Main Data</span>
                </a>
            </li>

            <?php
            $mainmenu = content::get_all_main_content();
            ?>

            <li class="dropdown"><a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-list"></i>&nbsp;Content Management&nbsp;<b class="caret"></b></a>
                <ul class="dropdown-menu pull-right">
                    <!-- -Main menu start-->
                    <?php
                    foreach ($mainmenu as $mmenu) {
                        $submenu = content::get_subcontents_by_main_id($mmenu['maincontent_id']);
                        ?>
                        <li class='dropdown-submenu' >
                            <?php if (sizeof($submenu) == 0) { ?>
                                <style>
                                    #mmenu<?php echo $mmenu['maincontent_id']; ?>:after{
                                        border-style:none !important;
                                        border-color:none !important;
                                    }
                                </style>
                            <?php } ?>
                            <a id="mmenu<?php echo $mmenu['maincontent_id']; ?>" href="main_content.php?maincontent_id=<?php echo $mmenu['maincontent_id']; ?>"><?php echo $mmenu['heading'] ?></a>
                            <ul class="dropdown-menu">
                                <!--     sub menu start -->
                                <?php
                                foreach ($submenu as $smenu) {
                                    $subothermenu = content::get_subothercontents_by_sub_id($smenu['subcontent_id']);
                                    ?>
                                    <li class='dropdown-submenu' >
                                        <?php if (sizeof($subothermenu) == 0) { ?>
                                            <style>
                                                #smenu<?php echo $smenu['subcontent_id']; ?>:after{
                                                    border-style:none !important;
                                                    border-color:none !important;
                                                }
                                            </style>
                                        <?php } ?>
                                        <a id="smenu<?php echo $smenu['subcontent_id']; ?>" href="sub_content.php?maincontent_id=<?php echo $smenu['maincontent_id']; ?>&subcontent_id=<?php echo $smenu['subcontent_id']; ?>"><?php echo $smenu['heading']; ?></a>
                                        <ul class="dropdown-menu">
                                            <!--sub other menu start-->
                                            <?php
                                            foreach ($subothermenu as $sothermenu) {
                                                $subotherothermenu = content::get_subother_otherContent_by_subotherID($sothermenu['subothercontent_id']);
                                                ?>
                                                <li class='dropdown-submenu' >
                                                    <?php if (sizeof($subotherothermenu) == 0) { ?>
                                                        <style>
                                                            #somenu<?php echo $sothermenu['subothercontent_id']; ?>:after{
                                                                border-style:none !important;
                                                                border-color:none !important;
                                                            }
                                                        </style>
                                                    <?php } ?>
                                                    <a id="somenu<?php echo $sothermenu['subothercontent_id']; ?>" href="subother_content.php?subcontent_id=<?php echo $sothermenu['subcontent_id']; ?>&subothercontent_id=<?php echo $sothermenu['subothercontent_id']; ?>"><?php echo $sothermenu['heading']; ?></a>
                                                    <ul class="dropdown-menu">
                                                        <!-- sub other other menu start-->
                                                        <?php foreach ($subotherothermenu as $sotherother) { ?>
                                                            <li><a href="subother_other_content.php?maincontent_id=<?php echo $sotherother['maincontent_id']; ?>&subcontent_id=<?php echo $sotherother['subcontent_id']; ?>&subothercontent_id=<?php echo $sotherother['subothercontent_id']; ?>&subother_other_content_id=<?php echo $sotherother['subother_other_content_id']; ?>"><?php echo $sotherother['heading']; ?></a></li>
                                                        <?php } ?>
                                                        <?php if (sizeof($subotherothermenu) != 0) { ?>
                                                            <li class="divider"></li>
                                                        <?php } ?>
                                                        <li><a href="subother_other_content.php?maincontent_id=<?php echo $sothermenu['maincontent_id']; ?>&subcontent_id=<?php echo $sothermenu['subcontent_id']; ?>&subothercontent_id=<?php echo $sothermenu['subothercontent_id']; ?>"><i class="fa fa-plus"></i> &nbsp;&nbsp;Add New</a></li>
                                                        <!-- sub other other menu End-->
                                                    </ul>

                                                </li>
                                            <?php } ?>
                                            <?php if (sizeof($subothermenu) != 0) { ?>
                                                <li class="divider"></li>
                                            <?php } ?>
                                            <li><a href="subother_content.php?subcontent_id=<?php echo $smenu['subcontent_id']; ?>"><i class="fa fa-plus"></i> &nbsp;&nbsp;Add New</a></li>
                                            <!--end of sub other menu-->
                                        </ul>
                                    </li>
                                <?php } ?>
                                <?php if (sizeof($submenu) != 0) { ?>
                                    <li class="divider"></li>
                                <?php } ?>
                                <li><a href="sub_content.php?maincontent_id=<?php echo $mmenu['maincontent_id']; ?>"><i class="fa fa-plus"></i> &nbsp;&nbsp;Add New</a></li>
                                <!--     sub menu End -->
                            </ul>
                        </li>

                    <?php } ?>
                    <?php if (sizeof($mainmenu) != 0) { ?>
                        <li class="divider"></li>
                    <?php } ?>
                    <li><a href="mainmenu.php"> <i class="fa fa-plus"></i> &nbsp;&nbsp;Add New</a></li>
                    <!-- -Main menu End-->
                </ul>
            </li>

            <li class="dropdown"><a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-cogs"></i>&nbsp;Product Management&nbsp;<b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li><a href="add_productCategory.php"><i class="fa fa-archive"></i> &nbsp;&nbsp;Manage Product Categories</a></li>
                <li><a href="add_subProductCategory.php" ><i class="fa fa-plus-square"></i> &nbsp;&nbsp;Add Sub Product Categories</a></li>
                <li><a href="add_products.php" ><i class="fa fa-plus-square-o"></i> &nbsp;&nbsp;Add a Product</a></li>
                <li><a href="view_products.php" ><i class="fa fa-search-plus"></i> &nbsp;&nbsp;View All Products</a></li>
            </ul>

            <li>
                <a href="mainmenu_edit.php">
                    <i class="fa fa-list-alt"></i> <span>Modify Menu</span>
                </a>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-wrench"></i>
                    <span>Administrative</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="adminmails.php"><i class="fa fa-envelope"></i> Admin Emails</a></li>
                    <li class="treeview">
                        <a href="#"><i class="fa fa-angle-double-right"></i> Users and Permissions</a>
                        <ul class="treeview-menu">
                            <li><a href="permissionlevels.php"><i class="fa fa-copy pull-left"></i>Permission Levels</a></li>
                            <li><a href="permission_category.php"><i class="fa fa-tags pull-left"></i>Permission Category</a></li>
                            <li><a href="user_rolls.php"><i class="fa fa-users pull-left"></i>User Roles</a></li>
                            <li><a href="viewusers.php"><i class="fa fa-user pull-left"></i>Users</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>


    </section>
    <!-- /.sidebar -->
</aside>