<?php

require_once('admin.php');

$per_tag = new Permission;
$per_tag->premission_tag = "permissionCategory";
$permission_block = $per_tag->check_permission($db);
if (!$permission_block) {
    $INCLUDE_FILE = "permission_denied.php";
    require_once('template_main.php');
    exit();
}

if (isset($_GET['action'], $_GET['did'])) {
    if ($_GET['action'] == 'delete' && is_numeric($_GET['did'])) {
        $catId = $_GET['did'];
        $row = $db->query_first("SELECT id FROM tblpermission_catagory WHERE id = $catId");
        $catId = $row['id'];

        if (empty($catId)) {
            $err.='Invalid Category Selected';
        } else {
            $db->query("DELETE from tblpermission_catagory WHERE id = $catId");
            header('location:' . $_SERVER['PHP_SELF'] . '?msg=' . base64_encode(8) . '');
        }
    } else {
        $err.='Invalid request';
    }
}

$all_data = $db->fetch_all_array("SELECT * FROM tblpermission_catagory ORDER BY catagory");
$params = array(
    'mode' => 'Sliding',
    'perPage' => 20,
    'delta' => 1,
    'itemData' => $all_data
);
$pager = & Pager::factory($params);
$datas = $pager->getPageData();

$page_main_heading = '<i class="fa fa-wrench"></i>&nbsp;&nbsp;'.'Administrative';
$breaddrum = "<li class='active'>Permission Category</li>";
$INCLUDE_FILE = "includes/permission_category.tpl.php";

require_once('template_main.php');
?>