<?php

require_once('admin.php');
if (isset($_POST['action']) && $_POST['action'] != "") {
    if ($_POST['action'] == "subother") {
        $data = array();
        $data['display_order'] = $_POST['value'];
        $subothercontent_id = $_POST['subid'];
        $res = $db->query_update("tblsubother_content", $data, "subothercontent_id=" . $subothercontent_id);
        if ($res) {
            echo "updated";
        } else {
            echo "not updated";
        }
    } elseif ($_POST['action'] == "sub") {
        $data = array();
        $data['display_order'] = $_POST['value'];
        $subcontent_id = $_POST['subid'];
        $res = $db->query_update("tblsub_content", $data, "subcontent_id=" . $subcontent_id);
        if ($res) {
            echo "updated";
        } else {
            echo "not updated";
        }
    } elseif ($_POST['action'] == "subother_other") {
        $data = array();
        $data['display_order'] = $_POST['value'];
        $subcontent_id = $_POST['subid'];
        $res = $db->query_update("tblsubother_other_content", $data, "subother_other_content_id=" . $subcontent_id);
        if ($res) {
            echo "updated";
        } else {
            echo "not updated";
        }
    } elseif ($_POST['action'] == "main_slides") {
        $data = array();
        $data['display_order'] = $_POST['value'];
        $id = $_POST['subid'];
        $res = $db->query_update("tblmain_slides", $data, "id=" . $id);
        if ($res) {
            echo "Success...! updated";
        } else {
            echo "not updated";
        }
    } elseif ($_POST['action'] == "section") {
        $data = array();
        $data['section'] = $_POST['value'];
        $id = $_POST['subid'];
        $res = $db->query_update("tbldirectors", $data, "id=" . $id);
        if ($res) {
            echo "updated";
        } else {
            echo "not updated";
        }
    } elseif ($_POST['action'] == "list") {

        $data = array();
        $data['display_order'] = $_POST['value'];
        $subcontent_id = $_POST['subid'];

        $res = $db->query_update("tbldirectors", $data, "id=" . $subcontent_id);

        if ($res) {
            echo "updated";
        } else {
            echo "not updated";
        }
    } else if ($_POST['action'] == "reportc") {
        $data = array();
        $data['category'] = $_POST['value'];
        $subcontent_id = $_POST['subid'];
        $res = $db->query_update("tblreports", $data, "report_id=" . $subcontent_id);

        if ($res) {
            echo "updated";
        } else {
            echo "not updated";
        }
    } else if ($_POST['action'] == "isreportc") {
        $data = array();
        $data['category'] = $_POST['value'];
        $subcontent_id = $_POST['subid'];
        $res = $db->query_update("tblreports", $data, "report_id=" . $subcontent_id);

        if ($res) {
            echo "updated";
        } else {
            echo "not updated";
        }
    } else if ($_POST['action'] == "mandcat") {
        $data = array();
        $data['category'] = $_POST['value'];
        $subcontent_id = $_POST['subid'];
        $res = $db->query_update("tbl_download_mandates", $data, "report_id=" . $subcontent_id);

        if ($res) {
            echo "updated";
        } else {
            echo "not updated";
        }
    } else if ($_POST['action'] == "subcateg") {
        $data = array();
        $data['sub_cat'] = $_POST['value'];
        $subcontent_id = $_POST['subid'];
        $res = $db->query_update("tbl_download_mandates", $data, "report_id=" . $subcontent_id);

        if ($res) {
            echo "updated";
        } else {
            echo "not updated";
        }
    } elseif ($_POST['action'] == "loccat") {
        $data = array();
        $data['category'] = $_POST['value'];
        $id = $_POST['subid'];
        $res = $db->query_update("tblBranchers", $data, "id=" . $id);
        if ($res) {
            echo "updated";
        } else {
            echo "not updated";
        }
    } else if ($_POST['action'] == "promotionc") {
        $data = array();
        $data['display_order'] = $_POST['value'];
        $id = $_POST['id'];
        $res = $db->query_update("tblpromotions", $data, "id=" . $id);
        if ($res) {
            echo "updated";
        } else {
            echo "not updated";
        }
    } elseif ($_POST['action'] == "slider") {
        $data = array();
        $data['display_order'] = $_POST['value'];
        $image_id = $_POST['image_id'];
        $res = $db->query_update("tblslider_images", $data, "image_id=" . $image_id);
        if ($res) {
            echo "updated";
        } else {
            echo "not updated";
        }
    } elseif ($_POST['action'] == "procat") {
        $data = array();
        $data['display_order'] = $_POST['value'];
        $id = $_POST['id'];
        $res = $db->query_update("tblproduct_category", $data, "id=" . $id);
        if ($res) {
            echo "updated";
        } else {
            echo "not updated";
        }
    } elseif ($_POST['action'] == "prosubcat") {
        $data = array();
        $data['display_order'] = $_POST['value'];
        $id = $_POST['id'];
        $res = $db->query_update("tblproduct_subcategory", $data, "id=" . $id);
        if ($res) {
            echo "updated";
        } else {
            echo "not updated";
        }
    } elseif ($_POST['action'] == "pro") {
        $data = array();
        $data['display_order'] = $_POST['value'];
        $id = $_POST['id'];
        $res = $db->query_update("tblproducts", $data, "id=" . $id);
        if ($res) {
            echo "updated";
        } else {
            echo "not updated";
        }
    }

}
?>