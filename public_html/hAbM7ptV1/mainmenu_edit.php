<?php

require_once('admin.php');
$per_tag = new Permission;
$per_tag->premission_tag = "modify_mainmenu";
$permission_block = $per_tag->check_permission($db);
if (!$permission_block) {
    $INCLUDE_FILE = "permission_denied.php";
    require_once('template_main.php');
    exit();
}


if (isset($_GET) && isset($_GET['action'])) {
    $action = clean_text($_GET['action']);
    if ($action == 'mainstatus') {

        $active = $_GET['status'];
        $id = $_GET['mid'];

        $data = array();
        $data['status'] = $active;
        $result = $db->query_update("tblmain_content", $data, "maincontent_id=" . $id);

        if ($result) {
            //**************** generate log entry *******************
            $logString = "Change Menu status - User ID = " . $user_id . " / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
            $log = Message::log_details($_SESSION['admin']['username'], $logString);
            // **************************************************
            header('location:' . $_SERVER['PHP_SELF'] . '?msg=' . base64_encode(6) . '');
            exit;
        }
    } else if ($action == 'maindelete') {
        $id = $_GET['mid'];

        $result = $db->query("DELETE FROM tblmain_content WHERE maincontent_id =" . $id . "");
        $result = $db->query("DELETE FROM tblsub_content WHERE maincontent_id =" . $id . "");
        $result = $db->query("DELETE FROM tblsubother_content WHERE maincontent_id =" . $id . "");
        $result = $db->query("DELETE FROM tblsubother_other_content WHERE maincontent_id =" . $id . "");

        //**************** generate log entry *******************
        $logString = "Deleted menu - User ID = " . $user_id . " / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
        $log = Message::log_details($_SESSION['admin']['username'], $logString);
        // **************************************************

        header('location:' . $_SERVER['PHP_SELF'] . '?msg=' . base64_encode(8) . '');
        exit;
    } else if ($action == 'substatus') {

        $active = $_GET['status'];
        $id = $_GET['sid'];

        $data = array();
        $data['status'] = $active;
        $result = $db->query_update("tblsub_content", $data, "subcontent_id=" . $id);

        if ($result) {
            //**************** generate log entry *******************
            $logString = "Change Menu status - User ID = " . $user_id . " / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
            $log = Message::log_details($_SESSION['admin']['username'], $logString);
            // **************************************************
            header('location:' . $_SERVER['PHP_SELF'] . '?msg=' . base64_encode(6) . '');
            exit;
        }
    } else if ($action == 'subdelete') {
        $id = $_GET['sid'];

        $result = $db->query("DELETE FROM tblsub_content WHERE subcontent_id =" . $id . "");
        $result = $db->query("DELETE FROM tblsubother_content WHERE subcontent_id =" . $id . "");
        $result = $db->query("DELETE FROM tblsubother_other_content WHERE subcontent_id =" . $id . "");

        //**************** generate log entry *******************
        $logString = "Deleted menu - User ID = " . $user_id . " / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
        $log = Message::log_details($_SESSION['admin']['username'], $logString);
        // **************************************************

        header('location:' . $_SERVER['PHP_SELF'] . '?msg=' . base64_encode(8) . '');
        exit;
    } else if ($action == 'subothsta') {
        $active = $_GET['status'];
        $id = $_GET['soid'];

        $data = array();
        $data['status'] = $active;
        $result = $db->query_update("tblsubother_content", $data, "subothercontent_id=" . $id);

        if ($result) {
            //**************** generate log entry *******************
            $logString = "Change Menu status - User ID = " . $user_id . " / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
            $log = Message::log_details($_SESSION['admin']['username'], $logString);
            // **************************************************
            header('location:' . $_SERVER['PHP_SELF'] . '?msg=' . base64_encode(6) . '');
            exit;
        }
        
    } else if ($action == 'subothdel') {
        $id = $_GET['soid'];

        $result = $db->query("DELETE FROM tblsubother_content WHERE subothercontent_id =" . $id . "");
        $result = $db->query("DELETE FROM tblsubother_other_content WHERE subothercontent_id =" . $id . "");

        //**************** generate log entry *******************
        $logString = "Deleted menu - User ID = " . $user_id . " / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
        $log = Message::log_details($_SESSION['admin']['username'], $logString);
        // **************************************************

        header('location:' . $_SERVER['PHP_SELF'] . '?msg=' . base64_encode(8) . '');
        exit;
        
    } else if ($action == 'subothothsta') {
        $active = $_GET['status'];
        $id = $_GET['sooid'];

        $data = array();
        $data['status'] = $active;
        $result = $db->query_update("tblsubother_other_content", $data, "subother_other_content_id=" . $id);

        if ($result) {
            //**************** generate log entry *******************
            $logString = "Change Menu status - User ID = " . $user_id . " / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
            $log = Message::log_details($_SESSION['admin']['username'], $logString);
            // **************************************************
            header('location:' . $_SERVER['PHP_SELF'] . '?msg=' . base64_encode(6) . '');
            exit;
        }
        
    } else if ($action == 'subothothdel') {
        $id = $_GET['sooid'];

        $result = $db->query("DELETE FROM tblsubother_other_content WHERE subother_other_content_id =" . $id . "");

        //**************** generate log entry *******************
        $logString = "Deleted menu - User ID = " . $user_id . " / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
        $log = Message::log_details($_SESSION['admin']['username'], $logString);
        // **************************************************

        header('location:' . $_SERVER['PHP_SELF'] . '?msg=' . base64_encode(8) . '');
        exit;
    }
}


$page_main_heading = '<i class="fa fa-list-alt"></i>&nbsp;&nbsp;'.'Manage Site Content';

$breaddrum = "<li class='active'>Modify Main Menu</li>";

$INCLUDE_FILE = 'includes/mainmenu_edit.tpl.php';

require_once('template_main.php');
?>