<!--<pre>
<?php //print_r($page_content); ?>
</pre>-->

<html>
<title>The Beira Group | Brush Manufacturer | Range Of Quality Brushes | Hygiene Product Range | Brush Exporter</title>
<meta name="viewport" content="width=device-width, initial-scale=1">





<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
 <link rel="shortcut icon" type="image/x-icon" href="images/favic.jpg">
  <link rel="apple-touch-icon" href="images/webclip.jpg">
 <script type="text/javascript" src="js/jquery1.11.1.js"></script>
  <link rel="stylesheet" type="text/css" href="css/magnific-popup.css">
 <script src="//use.typekit.net/mrg7dwk.js"></script>



<!-- bootstrap -->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"  >
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" >
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>

<!--  bootstrap -->







<style>
@media screen 
  and (min-device-width: 1200px) 
  and (max-device-width: 1600px) 
  and (-webkit-min-device-pixel-ratio: 1) { 
  .size_txt {
    
    font-size: 17px !important;
  }
.w3-sidenav {
    height: 24% !important;
   
}
}
.w3-container {
    padding: 0.01em 78px;
}
.w3-red, .w3-hover-red:hover {
    color: #000!important;
    background-color: rgb(255, 255, 255)!important;
}
.w3-sidenav {
    height: 16%;
    width: 200px;
    background-color: #fff !important;
    position: relative !important;
    z-index: 1;
    overflow: auto;
}
.city {display:none;}
.w3-card-2, .w3-example {
    box-shadow: 0 2px 4px 0 rgba(0,0,0,0.0),0 2px 10px 0 rgba(0,0,0,0.0)!important;
}
.foot {
    
    bottom: 0 !important;
   
}
.col-xs-6{padding-top: 160px;}
.container {
    padding-bottom: 5px;
    text-align: left !important;
    padding-top: 33px !important;
    }
    li,ul{text-align:left;}

    body { 
   min-height: 100%;  
	}
  h1{
    color: rgb(34,95,166);
    font-family: inherit;
    font-weight: 100;
    font-size: 28px;
    margin: 10px 30px;
}
.inner-h1 {
    margin: 0px 40px !important;
    padding-top: 90px !important;
}
.inner-h2 {
    color: rgb(34,95,166);
    font-family: inherit;
    font-weight: 100;
    font-size: 18px;
    margin: 0px 30px;
}
.inner-h1-dt{
  margin: 10px 40px !important;
    
    color: rgb(34,95,166);
    font-family: inherit;
    font-weight: 100;
    font-size: 28px;
    margin: 10px 30px;
    margin-top: -129px !important;
}
.mar{
  margin-top: -129px !important;
}
ul, ol{
      display: block;
    margin: inherit;
    color: #333;
    line-height: 1.5;
    font-weight: 100;
    font-family: inherit;
    text-align: justify !important;
    font-size: 17px;
    margin: 10px 30px 20px;
}
.pad_a{padding-left: 30px;}
body{overflow: hidden !important;}

</style>
<body onload="loadDefault()">

<div class="col-md-12" style="min-height: 463px;">

<nav class="w3-sidenav w3-light-grey w3-card-2" class="size_txt" style="width:273px; height: 284px !important;">
  <div class="w3-container">
   
  </div>
  <a href="#" class="tablink" onclick="openCity(event, 'Annual')"><h2 class="inner-h2">Annual Reports</h2></a>
  <a href="#" class="tablink" onclick="openCity(event, 'Interim')"><h2 class="inner-h2">Interim Results</h2></a>
  <a href="#" class="tablink" onclick="openCity(event, 'Pari')"><h2 class="inner-h2">Prospectus</h2></a>
  <a href="#" class="tablink" onclick="openCity(event, 'Tokyo')"><h2 class="inner-h2">Corporate Governance</h2></a>
  <a href="#" class="tablink" onclick="openCity(event, 'Par')"><h2 class="inner-h2">Corporate Calendar</h2></a>
  <a href="#" class="tablink" onclick="openCity(event, 'Londo')"><h2 class="inner-h2">Broker Research</h2></a>
  
</nav>




<div style="margin-left: 263px;padding-top: 0px;margin-top: -138px;
    position: absolute;">
  

  <div id="Annual" class="w3-container city">
    <h1 class="inner-h1-dt">Annual Reports</h1> 
    <p class="inner-small-despript">The Annual Report for year 2016/17 is available by clicking on the link below.</p>
    <a href="resources/pdf/BPPL_Holdings_PLC_AR_2016_17.pdf" class="pad_a">Annual Report 2016/17</a>
  </div>

  <div id="Interim" class="w3-container city">
    <h1 class="inner-h1-dt">Interim Results</h1>
    <p class="inner-small-despript">The Interim Financial Statements for the first quarter ended 30th June 2017 is available by clicking on the link below.</p>
    <a href="resources/pdf/BPPL_Holdings_PLC_IR_2017_June_30.pdf" class="pad_a">Interim Report 2017</a>
  </div>

  <div id="Pari" class="w3-container city">
    <h2 class="inner-h1-dt">Prospectus</h2>

<ul>
<li>The March 2017 IPO Prospectus is available by clicking on the link below.</li>
<a href="resources/pdf/Prospectus.pdf" class="pad_a">Prospectus</a>&nbsp;&nbsp;&nbsp;<a href="resources/pdf/Annexures_to_IPO_Prospectus.pdf" class="pad_a">Annexures to the Prospectus</a>
<!--<li>The accompanying Research Report is available by clicking on the link below.</li>
<a href="resources/pdf/Research_Report.pdf" class="pad_a">Link</a>-->
<li>The Audited Financial Statements for the financial year ending 31st March 2016 is available by clicking on the link below.</li>
<a href="resources/pdf/Audited_Financial_Statements_for_the_financial_year_ended_31st_March_2016.pdf" class="pad_a">Link</a>
<li>The Interim Financial Statements for the period ending 31st December 2016 is available by clicking on the link below.</li>
<a href="resources/pdf/Interim_Financials_for_the_period_ended_31st_December_2016.pdf" class="pad_a">Link</a>
<li>The Articles of Association of the company are available by clicking on the link below.</li>
<a href="resources/pdf/Articles_of_Association.pdf" class="pad_a">Link</a>
</ul>
  </div>

  <div id="Tokyo" class="w3-container city">
    <h2 class="inner-h1-dt">2017 Corporate Governance</h2>
    <ul>
    <li><b>Audit committee</b>
		<ul>
			<li>Chairman - Mr Manjula Hiranya De Silva - Independent Director</li>
			<li>Member - Mrs Sharmini Tamara Ratwatte - Independent Director</li>
			<li>Member - Mr Ranil Prasad Pathirana – Non executive Director</li>
			<li style="list-style: none;"><a download="AuditCommitteeCharter.pdf" href="./downloads/AuditCommitteeCharter.pdf">Audit Committee Charter Download</a></li>
		</ul>
	</li>
	
	<li><b>Related Party transaction committee</b>
		<ul>
			<li>Chairman - Mr Manjula Hiranya De Silva - Independent Director</li>
			<li>Member - Mrs Sharmini Tamara Ratwatte - Independent Director</li>
			<li>Member - Mr Ranil Prasad Pathirana – Non executive Director</li>
			<!--<li style="list-style: none;"><a download="CompensationCommittee.pdf" href="./downloads/CompensationCommittee.pdf">Compensation Committee Charter Download</a></li> -->
		</ul>
	</li>
	
	<li><b>Remuneration committee</b>
		<ul>
			<li>Chairman – Mr Savantha De Saram- Indepandent</li>
			<li>Member -  Mrs Sharmini Tamara Ratwatte - Independent Director</li>
			<li>Member - Mr Ranil Prasad Pathirana – Non executive Director</li>
			<li style="list-style: none;"><a download="RemunerationCommitteeCharter.pdf" href="./downloads/RemunerationCommitteeCharter.pdf">Remuneration Committee Charter Download</a></li>
		</ul>
	</li>
    </ul>
    
  </div>

  <div id="Par" class="w3-container city">
    <h2 class="inner-h1-dt">Corporate Calendar</h2>
    <ul>
    <!-- <li>2017 February - 3rd Quarter Results.</li>
    <li>Announcement of results, for the Quarterly end 31 December 2016.</li>
    <li>2017 May Full Year ending 31st March ending 2017 & Announcement of Dividend.</li>
    <li>Annual General Meeting October 2017,</li>
    <li>2017 – Announcement of Results for half year ending 30th Sep 2017.</li> -->

<li>2017 May - Results for the completed year, ending 31st March 2017.</li>
<li>2017 August - Results for the first quarter ending 30th June 2017.</li>
<li>2017 August - Annual General Meeting.</li>
<li>2017 November – Results for the half year ending 30th September 2017.</li>
<li>2018 February - Results for the nine months period ending 31st December 2017.</li> 
<li>2018 May - Results for the financial year ending 31st March 2018.</li>
    </ul>
    
  </div>

  <div id="Londo" class="w3-container city">
   <h2 class="inner-h1-dt">Broker Research</h2>
    <p class="inner-small-despript">Broker research covering BPPL Holdings is produced by the following institutions.</p>
    <br>
    <ul>
       <li>CT CLSA Securities (Pvt) Ltd.</li>
       <ul>
          <li><a href="resources/pdf/broker_research/CT_CLSA_-_BPPL_Holdings_(BPPL)_1Q18_Results_Summary.pdf" title="Click to view the report" target="_blank">BPPL Holdings (BPPL) 1Q18 Results Summary</a></li>
       </ul>
    </ul>
    <!--<div class="col-md-3"><img src="images/pdf.jpg" style="width:35%;padding-bottom: 6px;"><br>Article<a href="images/pdf.pdf" target="_blank"><br><br><input type="button" value="Download"></a>
    </div>
    <div class="col-md-3"><img src="images/pdf.jpg" style="width:35%;padding-bottom: 6px;"><br>Prospectus<a href="images/pdf.pdf" target="_blank"><br><br><input type="button" value="Download"></a> 
    </div>-->
  </div>

</div>
</div>

<script>
function loadDefault(){
	document.getElementById('Pari').style.display = "block";
}
function openCity(evt, cityName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" w3-red", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-red";
}
</script>

</body>
</html>

