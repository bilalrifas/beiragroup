<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container">
    <div class="row" style="max-width: 100%;height: auto;">
		<div class="col-md-12">
			<div class="panel panel-primary" style="margin-top:25px;">
				<div class="panel-heading">
					<h3 class="panel-title">BPPL Holdings April 2016 to February 2017 Interim Financial Results</h3>
					<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-down"></i></span>
				</div>
                <div class="panel-collapse collapse out">
				    <div class="panel-body">

				    <h5><b>Net Profit of Rs393 million for the eleven months ended 28th February 2017</b></h5><br>

					<p>BPPL Holdings announced its interim unaudited financial results for the eleven month period April 2016 to February 2017. </p><br>
					<p>Consolidated revenue for the period was Rs2.2 billion, up 19% over the corresponding period in the previous year. Revenue continued to grow as the company pursued its dual objectives of penetrating the household market segment both through direct sales to retailers and own branded goods sales in Sri Lanka and Indonesia. Direct sales accounted for 11% of total sales for the period, up 30% year-on-year. Own branded goods also grew by 55%, again over the corresponding period in the previous year.</p><br>
					<p>Gross profit was up by a faster 30% year-on-year to Rs881 million due to margin expansion amid revenue growth. Gross profit margins, which improved from 37% to 40% during the eleven month period ended February 2017, continued to benefit from higher productivity, lower costs as a result of improved raw material sourcing and Sri Lankan Rupee depreciation against the US Dollar.  </p><br>
					<p>Improved productivity and stringent cost controls also led to a 47% increase in operating profit to Rs472 million compared to the same period in the previous year. Moreover, margins continued to expand to 21% at a Profit-Before-Tax level due to lower interest expenses as accumulated profits were used for debt retirement. Profit-Before-Tax was Rs453 million for the period whilst Profit-After-Tax attributable to the company’s shareholders was Rs393 million, an increase of 48% year-on-year. Non-annualized EPS for the period amounted to Rs1.28 (based on number of shares as at 28th February 2017). </p><br>
					<p>Meanwhile, BPPL Holdings moved ahead with its plans for extruding synthetic yarn by placing orders with machinery suppliers following successful trials conducted with its own hot washed recycled PET flakes and discussions with leading textile producers. The construction of a new factory in the Horana BOI Industrial Zone also commenced in January 2017. This yarn production facility, which involves an investment of Rs675 million, is set to come on-stream in the January to March quarter of 2018 and contribute to revenue from April 2018.</p><br>
					<p>The company is also on track to commence power generation from its own 347KW solar and 200KW biomass based power plants later this month. </p><br>
					<p>BPPL offered 30,685,000 ordinary shares priced at Rs12 per share to the public via an Initial Public Offering recently. The offer was fully subscribed on the first day. </p><br>
					</div>
<!-- 				<div>
				<a class="btn" href="downloads/Feb17 Interim Financials Release 22 March 2017.pdf" style="margin-left:15px;line-height:0.8;margin-bottom: 20px" download>Read More</a>
				</div>  -->
                </div>
			</div>
		</div>
	</div>
</div>



<style type="text/css">
	.row{
    margin-top:40px;
    padding: 0 10px;
}

.clickable{
    cursor: pointer;   
}

.panel-heading span {
	margin-top: -20px;
	font-size: 15px;
}
.panel-body{
	text-align: left;
}

.btn:hover{
	color: white;
}
</style>


<script type="text/javascript">
	
	$(document)
    .on('click', '.panel-heading span.clickable', function(e){
        $(this).parents('.panel').find('.panel-collapse').collapse('toggle');
    })
    .on('show.bs.collapse', '.panel-collapse', function () {
        var $span = $(this).parents('.panel').find('.panel-heading span.clickable');
        $span.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
    })
    .on('hide.bs.collapse', '.panel-collapse', function () {
        var $span = $(this).parents('.panel').find('.panel-heading span.clickable');
        $span.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
    })
</script>
</body>
</html>
