<?php
require "../user.php";
require '../classes/SlackNotifier.php';

//$securimage = new Securimage();
/*
	// removed this captcha check
	elseif ($securimage->check($_POST['captcha_code']) == false) {
		echo "2";
	  	exit();
	}
*/

// for autobot
$key = 'd2afc4cefa3fca46731918e598969cbe';

// for recaptcha
include_once DOC_ROOT . 'classes/recaptcha/autoload.php';
$siteKey = '6LfR9QgTAAAAAExBgbJlYGKsmuostKv5VEuiTAdM';
$secret = '6LfR9QgTAAAAAJwlL0MxA7IEdWduDNRQ-g3ayE0W';
$lang = 'en';	


if (isset($_POST)) {

    $captchares = true;

    $botkey = 'd2afc4cefa3fca46731918e598969cbe';
    if(isset($_POST['bot'])) { $incbkey = filter_var($_POST['bot'], FILTER_SANITIZE_STRING); }

    if(!isset($incbkey) || ($incbkey != $botkey)){    
    	
    	// CSRF check
    	if($_POST['csrf_token'] != $_SESSION['csrftoken'])
    	{
        	// CSRF attack detected
        	//$result = $e->getMessage() . ' Form ignored.';
        	session_regenerate_id();
        	header("HTTP/1.0 404 Not Found");
    	}
    	
    	// get google recaptcha response
        $recaptcha = new \ReCaptcha\ReCaptcha($secret);
    	$resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
    	$captchares = $resp->isSuccess();    	
    	
    } 


	$name=$purifier->purify($_POST['name']);
	$email=$purifier->purify($_POST['email']);
	$message=$purifier->purify($_POST['message']);

	if($name==""){
		echo "1";
		exit();
	} elseif ($email=="" || !$user->valid_email($email)) {
		echo "1";
		exit();
	} elseif ($message=="") {
		echo "1";
		exit();
	} elseif($captchares == false){

		echo "2";
		exit();		

	}else {

		$body='
		<table style="width: 100%;" border="0" cellspacing="5" cellpadding="5"><caption>
			<h1 style="text-align: center;"><img src="'.HTTP_PATH.'images/content/custom_uploads/logo.png" alt="" /><span style="font-family: verdana,geneva; font-size: 24pt;">&nbsp; <span style="color: #3366ff;">The Beira Group Inquiry<br /></span></span></h1>
			</caption>
			<tbody>
			<tr><th style="width: 50%; text-align: right;"><pre>Name :</pre></th>
			<td style="width: 50%; text-align: left;"><pre>'.$name.'</pre></td>
			</tr>
			<tr><th style="width: 50%; text-align: right;">
			<pre>Email Addess :</pre>
			</th>
			<td style="width: 50%; text-align: left;"><pre>'.$email.'</pre></td>
			</tr>
			<tr><th style="width: 50%; text-align: right;" valign="top">
			<pre>Message :</pre>
			</th>
			<td style="width: 50%; text-align: left;"><pre>'.$message.'</pre></td>
			</tr>
			<tr><th style="width: 50%; text-align: right;">
			<pre>Date and Time :</pre>
			</th>
			<td style="width: 50%; text-align: left;"><pre>'.date("Y-m-d H:i:s").'</pre></td>
			</tr>
			</tbody>
		</table>
		';
		$subject="Beira Group Inquiry";
		$to="info@beiragroup.com";
		$admin_email_array=content::get_active_admin_emails();

		$from=$email;

		$result="";
		foreach ($admin_email_array as $key => $row) {
			$result=mailing::html_mail($row['email'],$subject,$body,$from,$from);
		}


		if ($result) 
		{
            // send slack alert
            $slack = new SlackNotifier();

            $slack->notify(array('guest_name' => $name, 'guest_email' => $email, 'guest_msg' => $message));
		
			echo "0";
			exit();
		} 
		else 
		{
			echo "3";
			exit();
		}

	}

}



?>