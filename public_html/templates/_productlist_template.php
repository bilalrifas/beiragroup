<?php include_once("detect.php"); ?>
<!doctype html>
<html style="width:100%; height:100%;" class="tk-myriad-pro">
<head>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WTWZQ8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WTWZQ8');</script>
<!-- End Google Tag Manager -->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" media="all" href="css/allie.css" />
<![endif]-->
<!--
  <meta charset="utf-8">
  <meta name="description" content="<?php echo $page_content['meta_desc']; ?>">
  <meta name="keywords" content="<?php echo $page_content['meta_key']; ?>"> -->
  <link rel="shortcut icon" type="image/x-icon" href="images/favic.jpg">
  <link rel="apple-touch-icon" href="images/webclip.jpg">
  <!--<title><?php echo $page_content['meta_title']; ?></title> -->
  <script src="//use.typekit.net/mrg7dwk.js"></script>
  <script>try{Typekit.load();}catch(e){}</script>
  <?php include_once("ie-fix.php"); ?>

</head>

<body>
<div class="menu-wrap">
  <?php include_once("header.php"); ?>
</div>
  <?php include "includes/$requirefile";   ?>

   <?php include_once("footer.php"); ?>

<script type="text/javascript" src="js/jquery1.11.1.js"></script>

<?php //echo $seo_code; ?>

<script type="text/javascript" src="js/webflow.js"></script>

<link rel="stylesheet" type="text/css" href="css/beiragroup.webflow.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<link rel="stylesheet" type="text/css" href="css/normalize.css">
<link rel="stylesheet" type="text/css" href="css/webflow.css">
<link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css">
<link rel="stylesheet" type="text/css" href="webfont/stylesheet.css">
<script type="text/javascript" src="js/modernizr.js"></script>
<script src="js/webfont.js"></script>
  <script>
    WebFont.load({
      google: {
        families: ["Open Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic","Ubuntu:300,300italic,400,400italic,500,500italic,700,700italic","PT Sans:300,300italic,400,400italic,500,500italic,700,700italic","PT Serif:300,300italic,400,400italic,500,500italic,700,700italic"]
      }
    });
  </script>
  <link rel="shortcut icon" type="image/x-icon" href="images/favic.jpg">
  <link rel="apple-touch-icon" href="images/webclip.jpg">
  


  <script>
  	$('#form').mouseenter(function(){
		$('#input').animate({opacity:'1',width:'120px'},'linear');
		});
	$('#btn').click(function(){
		$('#input').animate({width:'0px',opacity:'0'},'linear');
		});


		</script>
  <script>
   
$(window).scroll(function(){
	$('.w-nav').css("background-color","rgb(93, 121, 172)");
	});
  
  </script>
  <!--[if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif]-->
  
</body>
</html>