 
  <?php //include("includes/bestweb.php"); ?>
 <?php
include("includes/tag_manager_body.php");

 $about_us_content= @content::get_maincontent_by_id(5);
 $contact_us_content= @content::get_maincontent_by_id(6);
  $factory_content= @content::get_maincontent_by_id(10);
 // $tiptop_content=content::get_subcontent_by_id(2);
 $product_actegories_content= @content::get_active_product_categories();

 ?>

  <div class="w-nav accordion" data-collapse="medium" data-animation="default" data-duration="400" data-contain="1">
    <div class="w-container width-fix">
      <a class="w-nav-brand" href="/">
        <img class="logo" src="images/logo.png" alt="beiragroup-logo">
      </a>
      <nav class="w-nav-menu accordion" role="navigation">
      <!--<a class="w-nav-link" href="<?php echo HTTP_PATH; ?>">Home</a>-->
      <?php foreach ($product_actegories_content as $key => $row_content):
      $subcategory=content::get_first_subcat_by_mainid($row_content['id']);
        if ($subcategory) {
          $url=str_replace(' ', '', $row_content['title'])."_".str_replace(' ', '', $subcategory['heading']).".html";
        } else {
          $url=HTTP_PATH;
        }?>
        <a class="w-nav-link" href="<?php echo $url;?>"><?php echo $row_content['title'] ?></a>
      <?php endforeach ?>
      <a class="w-nav-link" href="brief-<?php echo str_replace(' ', '-', str_replace('&', 'and', $factory_content['heading']));?>.html"><?php echo $factory_content['heading'];?></a>
      <a class="w-nav-link" href="beira-<?php echo str_replace(' ', '-', str_replace('&', 'and', $about_us_content['heading']));?>.html"><?php echo $about_us_content['heading'];?></a>
      <a class="w-nav-link" href="beira-<?php echo str_replace(' ', '-', str_replace('&', 'and', $contact_us_content['heading']));?>.html"><?php echo $contact_us_content['heading'];?></a>
      <a class="w-nav-link" href="beira-search.html"><img class="magnify" src="images/search-icon-big.png" alt="search"></a>
      </nav>
      <div class="w-nav-button acoicon">
        <div class="w-icon-nav-menu acoicon"></div>
      </div>
    </div>
  </div>
<?php
  if (isset($_GET['main']) && $_GET['main']==5) {
    ?>
    <h2 class="range ab"> <?php echo $main['spe_details']; ?></h2>
    <?php
  }
  ?>