<style>
    .box {
    
    width: 22% !important;

}
.Chead {
    
    font-size: 26px !important;
   
}
.addr{line-height: 1.1;}
.Shead {
    
    font-size: 21px !important;
}
.mar{    margin-top: -8px !important;}
.mar2{    margin-top: -19px !important;}

</style>
<?php //$csrftoken = NoCSRF::generate('csrf_token'); ?>
<div class="container">

	<div class="box">

    	<h2 class="Chead">Head Office</h2>

        <h4 class="Shead"><?php //echo $companyname; ?>BPPL Holdings Limited</h4>

        <p class="address"><?php echo $address1; ?><br><br>

							<?php echo $telephone1; ?> , <?php echo $telephone2; ?><br><br>

							Fax: <?php echo $fax1; ?><br><br>

							Email:  <a style="text-decoration:none; color:rgb(54, 131, 199);" href="mailto:<?php echo $email1; ?>"><?php echo $email1; ?></a><br><br>




    </div>



    <div class="box">

    	<h2 class="Chead">Factories</h2>

        <h4 class="Shead"><?php echo $factoryname1; ?></h4>

    	<p class="address"> <?php echo $address2; ?></p>

        <h4 class="Shead"><?php echo $factoryname2; ?></h4>

    	<p class="address"><?php echo $address3; ?></p>

        <h4 class="Shead"><?php echo $factoryname3; ?></h4>

        <p class="address"><?php echo $address4; ?></p>



    </div>
<div class="box">

    	<h2 class="Chead">North American  - Sales Office</h2>

        <h4 class="Shead">Beira Marketing Services(N.A)Inc.</h4>

    	<p class="address">1465,Caulder Drive, Oakville, Ontario L6J 5TI, Canada</p>
          
        <p class="address">Tel: 1 905 815 9330</p>
        <p class="address">Fax: 1 905 815 9330</p>
        <p class="address">Email : <a style="text-decoration:none; color:rgb(54, 131, 199);" href="mailto:beiramarketing@cogeco.ca">beiramarketing@cogeco.ca</a></p>
        <br />
        <h2 class="Chead mar">Indonesian Sales Office</h2>
        
        
<p class="address">Grand Slipi Tower 9th floor ,Unit G JI.Jend.S.Parman Kav 22-24 Slipi West Jakarta 11480</p>
          
        <p class="address">Tel: +62 21 80660911</p>
        <p class="address">Fax: +62  21 80660901</p>
        <p class="address">Email : <a style="text-decoration:none; color:rgb(54, 131, 199);" href="mailto:irpan@bppl.lk">irpan@bppl.lk</a></p>


    </div>



    <div class="box">
    <h2 class="Chead">Write to us...</h2>

    <form id="contactus_form"  action="includes/contact_submit.php" method="post">
    <input class="input" name="name" type="text" autocomplete="off" required placeholder="Name"  onclick="this.placeholder = ''" onblur="this.placeholder = 'Your Name is Required'">
    <input class="input" name="email" type="email" autocomplete="off" required placeholder="E - Mail"  onclick="this.placeholder = ''" onblur="this.placeholder = 'We need your Email Adress'">
    <textarea class="input" name="message" rows="3" cols="20" placeholder="Your Message"  onclick="this.placeholder = ''" onblur="this.placeholder = 'Feel free to write your message'"></textarea>
    <div style="display: block;"><div class="g-recaptcha" data-sitekey="6LfR9QgTAAAAAExBgbJlYGKsmuostKv5VEuiTAdM"></div></div>
    <input type="hidden" name="csrf_token" value="<?php echo $_SESSION['csrftoken'] = md5(time()); ?>" />
    <input class="input hv" type="submit" name="submit" class="submit" value="Send the Message">
                    <!-- Desktop Social Media Icons [ START ] -->
                <div class="social_media_icons" >
                   <a href="https://www.facebook.com/Beira.Group/" target="_blank"> <img src="images//fb1.png" alt="facebook_icon" class="img-responsive" style="margin-right:10px;"></a>
                    <a href="https://www.linkedin.com/company/beira-group?trk=nav_account_sub_nav_company_admin" target="_blank"><img src="images/li1.png" alt="linkdn_icon" class="img-responsive"></a>
                    </div>
    </form>



    </div>

</div>
<script>

$(document).ready(function() {

    $("form").on('submit', function(e)
    {
        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
        $.ajax({
            url : formURL,
            type: "POST",
            data : postData,
            success: function(data, textStatus, jqXHR)
            {
                if (data==0) {
                    alert("Submission Successful.");
                } else if(data==1){
                    alert("Submission Data Invalid");
                } else if(data==2){
                    alert("Capture Code Invalid. Please Try again!")
                } else if(data==3){
                    alert("mail send failed. Please Try again!")
                }
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                alert("something went wrong. Please Try again!");
            }
        });

        e.preventDefault(); //Prevent Default action.
        e.unbind();
    });

});
</script>

<!-- google recaptcha integration -->
<script type="text/javascript">
var RecaptchaOptions = {
   lang : 'en',
};
</script>
<script src='https://www.google.com/recaptcha/api.js?hl=en'></script>
