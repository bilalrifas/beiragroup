<?php
require "../user.php";

//$securimage = new Securimage();
/*
	// removed this captcha check
	elseif ($securimage->check($_POST['captcha_code']) == false) {
		echo "2";
	  	exit();
	}
*/
// for recaptcha
include_once DOC_ROOT . 'classes/recaptcha/autoload.php';
$siteKey = '6LfR9QgTAAAAAExBgbJlYGKsmuostKv5VEuiTAdM';
$secret = '6LfR9QgTAAAAAJwlL0MxA7IEdWduDNRQ-g3ayE0W';
$lang = 'en';	


if (isset($_POST)) {

    // CSRF check
    if($_POST['csrf_token'] != $_SESSION['csrftoken'])
    {
        // CSRF attack detected
        //$result = $e->getMessage() . ' Form ignored.';
        session_regenerate_id();
        header("HTTP/1.0 404 Not Found");
    } 

    $recaptcha = new \ReCaptcha\ReCaptcha($secret);
    $resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);


	$name=$purifier->purify($_POST['name']);
	$email=$purifier->purify($_POST['email']);
	$message=$purifier->purify($_POST['message']);

	if($name==""){
		echo "1";
		exit();
	} elseif ($email=="" || !$user->valid_email($email)) {
		echo "1";
		exit();
	} elseif ($message=="") {
		echo "1";
		exit();
	} elseif($resp->isSuccess() == false){

		echo "2";
		exit();		

	}else {

		$body='
		<table style="width: 100%;" border="0" cellspacing="5" cellpadding="5"><caption>
			<h1 style="text-align: center;"><img src="'.HTTP_PATH.'images/content/custom_uploads/logo.png" alt="" /><span style="font-family: verdana,geneva; font-size: 24pt;">&nbsp; <span style="color: #3366ff;">The Beira Group Inquiry<br /></span></span></h1>
			</caption>
			<tbody>
			<tr><th style="width: 50%; text-align: right;"><pre>Name :</pre></th>
			<td style="width: 50%; text-align: left;"><pre>'.$name.'</pre></td>
			</tr>
			<tr><th style="width: 50%; text-align: right;">
			<pre>Email Addess :</pre>
			</th>
			<td style="width: 50%; text-align: left;"><pre>'.$email.'</pre></td>
			</tr>
			<tr><th style="width: 50%; text-align: right;" valign="top">
			<pre>Message :</pre>
			</th>
			<td style="width: 50%; text-align: left;"><pre>'.$message.'</pre></td>
			</tr>
			<tr><th style="width: 50%; text-align: right;">
			<pre>Date and Time :</pre>
			</th>
			<td style="width: 50%; text-align: left;"><pre>'.date("Y-m-d H:i:s").'</pre></td>
			</tr>
			</tbody>
		</table>
		';
		$subject="Beira Group Inquiry";
		$to="info@beiragroup.com";
		$admin_email_array=content::get_active_admin_emails();

		$from=$email;

		$result="";
		foreach ($admin_email_array as $key => $row) {
			$result=mailing::html_mail($row['email'],$subject,$body,$from,$from);
		}




		if ($result) {
		
  // slack channel alert start
	$room = "client-website-forms";
	$data = "payload=" . json_encode(array(

			"channel" => "#{$room}",
			"text" => "Inquiry from http://beiragroup.lk/\n
---------------------------------------------------------------\n
*Name :* {$name}\n
*Email :* {$email}\n
*Message :* {$message}\n",

));

	$ch = curl_init("https://hooks.slack.com/services/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");
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	curl_close($ch);
  // slack channel alert end		
		
			echo "0";
			exit();
		} else {
			echo "3";
			exit();
		}

	}

}



?>