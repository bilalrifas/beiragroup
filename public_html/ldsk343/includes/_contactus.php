<?php //$csrftoken = NoCSRF::generate('csrf_token'); ?>
<div class="container">

	<div class="box">

    	<h2 class="Chead">Head Office</h2>

        <h4 class="Shead"><?php echo $companyname; ?></h4>

        <p class="address"><?php echo $address1; ?><br><br>

							<?php echo $telephone1; ?> , <?php echo $telephone2; ?><br><br>

							Fax: <?php echo $fax1; ?><br><br>

							Email:  <a style="text-decoration:none; color:rgb(54, 131, 199);" href="mailto:<?php echo $email1; ?>"><?php echo $email1; ?></a><br><br>




    </div>



    <div class="box">

    	<h2 class="Chead">Factories</h2>

        <h4 class="Shead"><?php echo $factoryname1; ?></h4>

    	<p class="address"> <?php echo $address2; ?></p>

        <h4 class="Shead"><?php echo $factoryname2; ?></h4>

    	<p class="address"><?php echo $address3; ?></p>

        <h4 class="Shead"><?php echo $factoryname3; ?></h4>

        <p class="address"><?php echo $address4; ?></p>



    </div>



    <div class="box">
    <h2 class="Chead">Write to us...</h2>

    <form id="contactus_form"  action="includes/contact_submit.php" method="post">
    <input class="input" name="name" type="text" autocomplete="off" required placeholder="Name"  onclick="this.placeholder = ''" onblur="this.placeholder = 'Your Name is Required'">
    <input class="input" name="email" type="email" autocomplete="off" required placeholder="E - Mail"  onclick="this.placeholder = ''" onblur="this.placeholder = 'We need your Email Adress'">
    <textarea class="input" name="message" rows="3" cols="20" placeholder="Your Message"  onclick="this.placeholder = ''" onblur="this.placeholder = 'Feel free to write your message'"></textarea>
    <div style="display: block; padding-left: 52px;"><div class="g-recaptcha" data-sitekey="6LfR9QgTAAAAAExBgbJlYGKsmuostKv5VEuiTAdM"></div></div>
    <input type="hidden" name="csrf_token" value="<?php echo $_SESSION['csrftoken'] = md5(time()); ?>" />
    <input class="input hv" type="submit" name="submit" class="submit" value="Send the Message">
    </form>



    </div>

</div>
<script>

$(document).ready(function() {

    $("form").on('submit', function(e)
    {
        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
        $.ajax({
            url : formURL,
            type: "POST",
            data : postData,
            success: function(data, textStatus, jqXHR)
            {
                if (data==0) {
                    alert("Submission Successful.");
                } else if(data==1){
                    alert("Submission Data Invalid");
                } else if(data==2){
                    alert("Capture Code Invalid. Please Try again!")
                } else if(data==3){
                    alert("mail send failed. Please Try again!")
                }
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                alert("something went wrong. Please Try again!");
            }
        });

        e.preventDefault(); //Prevent Default action.
        e.unbind();
    });

});
</script>

<!-- google recaptcha integration -->
<script type="text/javascript">
var RecaptchaOptions = {
   lang : 'en',
};
</script>
<script src='https://www.google.com/recaptcha/api.js?hl=en'></script>