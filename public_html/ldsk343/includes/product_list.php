<form class="anupama">
  <select onchange="product_category_change(this.value);">
  <?php foreach ($allsubcategories as $key => $row): ?>
    <option value="<?php echo $val.'_'.str_replace(' ', '', $row['heading']) ?>.html" <?php if ($row['id']==$subcategory_content['id']): ?> selected <?php endif ?> ><?php echo $row['heading'] ?></option>
  <?php endforeach ?>
  </select>
</form>
<div class="new-sub new-subie">
  <?php foreach ($allsubcategories as $key => $row): ?>
    <a href="<?php echo $val.'_'.str_replace(' ', '', $row['heading']) ?>.html" class="prod prodie <?php if ($row['id']==$subcategory_content['id']): ?>prod-active<?php endif ?>"><?php echo $row['heading'] ?></a>
  <?php endforeach ?>
  <div class="pdf">
    <a href="images/catalogue.pdf" class="shukriya btn" title="Product Catalog">Product Catalogue</a>
  </div>
</div>

<div class="container dumith" >
<h1 class="type"><?php echo $subcategory_content["special_heading"] ?></h1><h3 class="dick"><?php echo $subcategory_content["special_wording"] ?></h3>
<?php foreach ($all_products as $key => $row):?>
  <div class="row">
    <div class="Pslide <?php echo ($key%2==0) ? "even" : "odd" ; ?>"><img class="pro" src="images/content/products/<?php echo $row['thumbnail'] ?>"<?php echo ($row['product_id'] == 'AB 4005' ? "style='width: 60%'" : ""); ?> alt=""></div>
    <p style="margin-<?php echo ($key%2==0) ? "right" : "left" ; ?>:39px;" class="cap <?php echo ($key%2==0) ? "even l" : "odd r" ; ?>"><?php echo $row["product_id"] ?></p>
    <p class="shoot <?php echo ($key%2==0) ? "e" : "o" ; ?>"><?php echo $row['first_desc'] ?></p>
    <!-- <a class="shoot <?php /*echo ($key%2==0) ? "e" : "o" ;*/ ?>" href="<?php /*echo $val.'_'.$val2.'_'.str_replace(' ', '', $row['product_id'])*/ ?>.html">View Product</a> -->
  </div>
<?php endforeach ?>
<?php if (sizeof($all_products)<1): ?>
  <div class="row">
    <h3 class="dick">No Products in this category.</h3>
  </div>
<?php endif ?>
</div>
<script>
  function product_category_change(url) {
    window.location.href = url;
  }
</script>