<?php
$pageName = basename($_SERVER['PHP_SELF']);
$contentID = $_GET['main'];
$contentName = $_GET["val"]."_".$_GET["val2"];

/* echo '<span class="'.$pageName.$_GET['main'].' '.$contentName.'">&nbsp</span> '; */

if($pageName == "index.php"){ /* Home page */
	echo '<title>Brush Manufacturer Sri Lanka | Brush Exporter | Beira Holdings PLC, Sri Lanka</title> 
<meta name="description" content="Beira Group is the largest brush manufacturer, brush exporter and exporter of sanitary maintenance tools in Asia. We produce brushes, brooms, banisters, dusters and scrubs."/> 
<meta name="keywords" content="Brush Manufacturer, Brush Exporter, Brushes, Brooms, Scrubs, Sanitary Maintenance Tools, Cleaning Solutions Vendor, OEM Brush Makers, Sanitary Maintenance Products Asia, Brush Manufacturer Asia, Brush Exporter Asia, Brush Manufacturers"/>';
}

if($pageName == "content.php"){
	if($contentID== "12"){ /* 12 is brands page*/
		echo '<title>Brush Manufacturer Sri Lanka | Brush Exporter | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="Take a look at our super quality brands, Tip Top and Jab."/> 
	<meta name="keywords" content="Brush Manufacturer, Brush Exporter, Brushes, Brooms, Scrubs, Sanitary Maintenance Tools"/>';
	}

	if($contentID== "5"){ /* 5 is about us page*/
		echo '<title>Brush Exporter Asia | About Beira | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="Beira manufactures a range of high quality, durable cleaning products for both professional and household applications."/> 
	<meta name="keywords" content="Beira group"/>';
	}

	if($contentID== "6"){ /* 6 is Contact us page*/
		echo '<title>Contact Us | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="Get in touch with Beira Group for more information."/> 
	<meta name="keywords" content="Medium Sweeps, Sweeps, Cleaning Brushes, Brush Manufacturer"/>';
	}

	if($contentID== "11"){ /* 11 is Investors page*/
		echo '<title>Investor Relations | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="This section provides a comprehensive reservoir of historical and recent information for both current and potential investors in BPPL."/> 
	<meta name="keywords" content="Brushes, Brooms, Scrubs, Sanitary Maintenance Tools, Sanitary Maintenance Products Asia, Brush Manufacturer Asia, Brush Exporter Asia, Brush Manufacturers"/>';
	}

	if($contentID== "10"){ /* 10 is brief-Factories page*/
		echo '<title>Factories | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="Based in Sri Lanka, the Beira Group’s five state-of-the-art factories offer an extensive inventory and rapid production capacity."/> 
	<meta name="keywords" content="Brush Exporter Asia, Brush manufacturing Country Asia, Sri Lanka Factories"/>';
	}

}

if($pageName == "product.php"){
	if($contentName== "Blocks_WoodenBlocks"){ /* Blocks_WoodenBlocks.html */
		echo '<title>Wooden Blocks | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="This range of products are designed to kitchen related tasks."/> 
	<meta name="keywords" content="Wooden Blocks, Wooden Kitchenware"/>';
	}

	if($contentName== "Janitorial_FineSweeps"){ /* Janitorial_FineSweeps.html */
		echo '<title>Fine Sweeps | Sanitary Maintenance Products Asia | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="This range of sweeps are designed to pick up the finest debris on highly polished or waxed floors."/> 
	<meta name="keywords" content="Fine Sweeps, Sanitary Maintenance Products Asia, Brush Exporter Sri Lanka"/>';
	}

	if($contentName== "BuildersRange_RoofingBrushes"){ /* BuildersRange_RoofingBrushes.html */
		echo '<title>Roofing Brushes | Beira Holdings PLC, Sri Lanka | Cleaning Brushes</title> 
	<meta name="description" content="This product is for cleaning roof surfaces and applying chemical substances."/> 
	<meta name="keywords" content="Roofing Brushes, Brushes, Cleaning Brushes"/>';
	}

	if($contentName== "FoodServices_HygieneRange"){ /* FoodServices_HygieneRange.html */
		echo '<title>Hygiene Range | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="Manufactured specifically to clean hygienically sensitive areas."/> 
	<meta name="keywords" content="Cleaning Brushes"/>';
	}

	if($contentName== "Household_AngleBrooms"){ /* Household_AngleBrooms.html */
		echo '<title>Broom Manufacturer Sri Lanka | Hygiene Range | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="To sweep off dust in indoor spaces, angle broom is the perfect tool."/> 
	<meta name="keywords" content="Broom Manufacturer Sri Lanka, Brush Manufacturer Asia, Brooms Exporter Asia"/>';
	}

	if($contentName== "Janitorial_MediumSweeps"){ /* Janitorial_MediumSweeps.html */
		echo '<title>Medium Sweeps | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="Medium sweeps are designed to collects small or medium debris on indoor and outdoor surfaces."/> 
	<meta name="keywords" content="Medium Sweeps, Sweeps, Cleaning Brushes, Brush Manufacturer"/>';
	}

	if($contentName== "Janitorial_HeavySweeps"){ /* Janitorial_HeavySweeps.html */
		echo '<title>Heavy Sweeps | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="This product will sweeps off hard debris from pathways, roads and concrete working sites."/> 
	<meta name="keywords" content="Heavy Sweeps, Brush Exporter Asia"/>';
	}

	if($contentName== "Janitorial_Banisters"){ /* Janitorial_Banisters.html */
		echo '<title>Banisters | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="Wipes off small piles of dust and grime on medium smooth surfaces."/> 
	<meta name="keywords" content="Banisters, Banister Manufacturer Sri Lanka"/>';
	}

	if($contentName== "Janitorial_CounterDusters"){ /* Janitorial_CounterDusters.html */
		echo '<title>Counter Dusters | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="Counter dusters are perfect to wipe off small piles of dirt from smooth surfaces."/> 
	<meta name="keywords" content="Dusters, Sri Lanka Brush Manufacturers"/>';
	}

	if($contentName== "Janitorial_HandScrubs"){ /* Janitorial_HandScrubs.html */
		echo '<title>Hand Scrubs | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="Our hand scrubs products clean small dirt patches on tiles, walls and floors."/> 
	<meta name="keywords" content="Hand Scrubs, Scrubs Sri Lanka/>';
	}

	if($contentName== "Janitorial_DeckScrubs"){ /* Janitorial_DeckScrubs.html */
		echo '<title>Deck Scrubs | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="This product scrubs rough surfaces - drainage lines, toilets etc."/> 
	<meta name="keywords" content="Deck Scrubs, Scrubs Manufacturer Sri Lanka"/>';
	}

	if($contentName== "Janitorial_ToiletBowlBrushes"){ /* Janitorial_ToiletBowlBrushes.html */
		echo '<title>Toilet Bowl Brushes | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="This product removes mold and other growth in toilets."/> 
	<meta name="keywords" content="Sanitary Maintenance Tools Sri Lanka, Toilet Bowl Brushes"/>';
	}

	if($contentName== "Janitorial_UtilityBrushes"){ /* Janitorial_UtilityBrushes.html */
		echo '<title>Broom Manufacturer Sri Lanka | Hygiene Range | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="To sweep off dust in indoor spaces, angle broom is the perfect tool."/> 
	<meta name="keywords" content="Broom Manufacturer Sri Lanka, Brush Manufacturer Asia, Brooms Exporter Asia"/>';
	}

	if($contentName== "Janitorial_CobwebBrushes"){ /* Janitorial_CobwebBrushes.html */
		echo '<title>Cobweb Brushes | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="Cobweb Brushes are for cleaning purposes such as removing cobwebs."/> 
	<meta name="keywords" content="Brushes, Cleaning Brushes"/>';
	}

	if($contentName== "Janitorial_RoundWindowBrushes"){ /* Janitorial_RoundWindowBrushes.html */
		echo '<title>Round Window Brushes | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="This Brushes are designed to Wash and scrub stubborn gatherings of dirt on windows."/> 
	<meta name="keywords" content="Window Brushes, Cleaning Solutions"/>';
	}

	if($contentName== "Janitorial_UprightBrooms"){ /* Janitorial_UprightBrooms.html */
		echo '<title>Upright Brooms | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="This range of brooms sweep off dust in indoor and outdoor spaces."/> 
	<meta name="keywords" content="Brooms, Brooms Manufacturer Sri Lanka"/>';
	}

	if($contentName== "Janitorial_VehicleWashBrushes"){ /* Janitorial_VehicleWashBrushes.html */
		echo '<title>Vehicle Wash Brushes | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="To clean vehicle exteriors without any scratches or damages."/> 
	<meta name="keywords" content="Brushes , Cleaning Brushes "/>';
	}

	if($contentName== "Janitorial_CottonMops"){ /* Janitorial_CottonMops.html */
		echo '<title>Cotton Mops | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="For mopping large indoor areas, Beira cotton mops are the ideal."/> 
	<meta name="keywords" content="Cotton Mops, Mops Manufacturer"/>';
	}

	if($contentName== "Janitorial_SyntheticMops"){ /* Janitorial_SyntheticMops.html */
		echo '<title>Synthetic Mops | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="For mopping large indoor areas - not available in Australia and New Zealand."/> 
	<meta name="keywords" content="Mops, Synthetic, Exporter"/>';
	}

	if($contentName== "Janitorial_AngleBrooms"){ /* Janitorial_AngleBrooms.html */
		echo '<title>Angle Brooms | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="Angle brooms sweep off dust in indoor spaces."/> 
	<meta name="keywords" content="Brooms, Brooms Manufacturer Sri Lanka"/>';
	}

	if($contentName== "Household_UprightBrooms"){ /* Household_UprightBrooms.html */
		echo '<title>Upright Brooms | Household | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="Sweeps off dust in indoor and outdoor spaces."/> 
	<meta name="keywords" content="Upright Brooms, Broom Manufacturer Sri Lanka"/>';
	}

	if($contentName== "Household_MediumSweeps"){ /* Household_MediumSweeps.html */
		echo '<title>Medium Sweeps | Household | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="This product will Collect small or medium debris on indoor and outdoor surfaces"/> 
	<meta name="keywords" content="Broom Manufacturer Sri Lanka, Brush Manufacturer Asia, Brooms Exporter Asia"/>';
	}

	if($contentName== "Household_Banisters"){ /* Household_Banisters.html */
		echo '<title>Banisters | Household | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="This product will sweep off dust in indoor spaces, angle broom is the perfect tool."/> 
	<meta name="keywords" content="Banisters, Brush Manufacturer Asia, Brooms Exporter Asia"/>';
	}

	if($contentName== "Household_CounterDusters"){ /* Household_CounterDusters.html */
		echo '<title>Counter Dusters | Household | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="This helpes to wipe off small piles of dirt from smooth surfaces."/> 
	<meta name="keywords" content="Broom Manufacturer Sri Lanka, Brush Manufacturer Asia, Counter Dusters "/>';
	}

	if($contentName== "Household_HandScrubs"){ /* Household_HandScrubs.html */
		echo '<title>Hand Scrubs | Household | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="Hand scrubs cleans small dirt patches on tiles, walls and floors."/> 
	<meta name="keywords" content="Broom Manufacturer Sri Lanka, Brush Manufacturer Asia, Scrubs"/>';
	}

	if($contentName== "Household_DeckScrubs"){ /* Household_DeckScrubs.html */
		echo '<title>Deck Scrubs | Household | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="Helps to Scrub rough surfaces - drainage lines, toilets etc."/> 
	<meta name="keywords" content="Deck Scrubs, Brush Manufacturer Asia, Brooms Exporter Asia"/>';
	}

	if($contentName== "Household_ShoeBrushes"){ /* Household_ShoeBrushes.html */
		echo '<title>Shoe Brushes | Household | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="Shoe brushes will clean and polishes your shoes effectively."/> 
	<meta name="keywords" content="Shoe Brushes, Brush Manufacturer Asia, Brooms Exporter Asia"/>';
	}

	if($contentName== "Household_NailBrushes"){ /* Household_NailBrushes.html */
		echo '<title>Nail Brushes | Household | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="This range of product will Remove dirt from focus areas effectively."/> 
	<meta name="keywords" content="Broom Manufacturer Sri Lanka, Nail Brushes, Brooms Exporter Asia"/>';
	}

	if($contentName== "Household_ToiletBowlBrushes"){ /* Household_ToiletBowlBrushes.html */
		echo '<title>Toilet Bowl Brushes | Household | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="This product will remove mold and other growth in toilets effectively."/> 
	<meta name="keywords" content="Broom Manufacturer Sri Lanka, Brush Manufacturer Asia, Toilet Bowl Brushes"/>';
	}

	if($contentName== "Household_VehicleWashBrushes"){ /* Household_VehicleWashBrushes.html */
		echo '<title>Vehicle Wash Brushes | Household | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="To clean vehicle exteriors without any scratches or damages."/> 
	<meta name="keywords" content="Broom Manufacturer Sri Lanka, Vehicle Wash Brushes, Brooms Exporter Asia"/>';
	}

	if($contentName== "Household_CottonMops"){ /* Household_CottonMops.html */
		echo '<title>Cotton Mops | Household | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="This product is for mopping large indoor areas."/> 
	<meta name="keywords" content="Broom Manufacturer Sri Lanka, Cotton Mops, Brooms Exporter Asia"/>';
	}

	if($contentName== "Household_SyntheticMops"){ /* Household_SyntheticMops.html */
		echo '<title>Synthetic Mops | Household | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="This product is for mopping large indoor areas - not available in Australia and New Zealand."/> 
	<meta name="keywords" content="Synthetic Mops, Brush Manufacturer Asia, Brooms Exporter Asia"/>';
	}

	if($contentName== "BuildersRange_MasonryBrushes"){ /* BuildersRange_MasonryBrushes.html */
		echo '<title>Masonry Brushes | Builders Range | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="Masonary Brushes are to clean debris leftover from masonry work."/> 
	<meta name="keywords" content="Broom Manufacturer Sri Lanka, Masonry Brushes, Brooms Exporter Asia"/>';
	}

	if($contentName== "BuildersRange_Squeegees"){ /* BuildersRange_Squeegees.html */
		echo '<title>Squeegees | Builders Range | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="Removes water, wet debris, slurry and muck from all surfaces."/> 
	<meta name="keywords" content="Broom Manufacturer Sri Lanka, Masonry Brushes, Squeegees"/>';
	}

	if($contentName== "BuildersRange_WireScratchBrushes"){ /* BuildersRange_WireScratchBrushes.html */
		echo '<title>Wire Scratch Brushes | Builders Range | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="Wire Scratch Brushes scratch out mold and rust effectively."/> 
	<meta name="keywords" content="Wire Scratch Brushes, Brush Manufacturer Asia, Brooms Exporter Asia"/>';
	}

	if($contentName== "BuildersRange_YardandStreetBrooms"){ /* BuildersRange_YardandStreetBrooms.html */
		echo '<title>Yard and Street Brooms | Builders Range | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="This will sweep off dirt and debris from yards and streets."/> 
	<meta name="keywords" content="Broom Manufacturer Sri Lanka, Yard and Street Brooms, Brooms Exporter Asia"/>';
	}

	if($contentName== "FoodServices_TankandKettleBrushes"){ /* FoodServices_TankandKettleBrushes.html */
		echo '<title>Broom Manufacturer Sri Lanka | Hygiene Range | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="To sweep off dust in indoor spaces, angle broom is the perfect tool."/> 
	<meta name="keywords" content="Broom Manufacturer Sri Lanka, Brush Manufacturer Asia, Brooms Exporter Asia"/>';
	}

	if($contentName== "FoodServices_MediumSweeps"){ /* FoodServices_MediumSweeps.html */
		echo '<title>Hygiene Range | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="Manufactured specifically to clean hygienically sensitive areas."/> 
	<meta name="keywords" content="Broom Manufacturer Sri Lanka, Brush Manufacturer Asia, Hygiene Products Asia"/>';
	}

	if($contentName== "Filaments_VegetableFibres"){ /* Filaments_VegetableFibres.html */
		echo '<title>Vegetable Fibres | Filaments | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="Take a look at our exclusive range of vegitable fibres."/> 
	<meta name="keywords" content="Vegetable Fibres, Brush Manufacturer Asia, Brooms Exporter Asia"/>';
	}

	if($contentName== "Filaments_AnimalFibres"){ /* Filaments_AnimalFibres.html */
		echo '<title>Animal Fibres | Hygiene Range | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="Take a look at our exclusive range of Animal Fibres."/> 
	<meta name="keywords" content="Broom Manufacturer Sri Lanka, Brush Manufacturer Asia, Animal Fibres"/>';
	}

	if($contentName== "Filaments_Wire"){ /* Filaments_Wire.html */
		echo '<title>Wire | Hygiene Range | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="Take a look at our exclusive range of effective Wires."/> 
	<meta name="keywords" content="Broom Manufacturer Sri Lanka, Animal Fibres, Brooms Exporter Asia"/>';
	}

	if($contentName== "Filaments_Synthetic"){ /* Filaments_Synthetic.html */
		echo '<title>Synthetic | Hygiene Range | Beira Holdings PLC, Sri Lanka</title> 
	<meta name="description" content="Take a look at our exclusive range of effective Synthetic Brushes."/> 
	<meta name="keywords" content="Synthetic Brushes, Brush Manufacturer Asia, Brooms Exporter Asia"/>';
	}

}

?>