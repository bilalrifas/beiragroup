<?php

class career {

	static function insert_jobcategory($data){
		global $db;
		$id=$db->query_insert("tbljob_categories", $data);
		return $id;
	}

	static function update_jobcategory($data,$id){
		global $db;
       	$update = $db->query_update('tbljob_categories',$data,"id=".$id."");
		return $update;
	}

	static function get_category_by_id($id){
		global $db;
    	$sql = "SELECT *
				  FROM tbljob_categories 
				  WHERE id=".$id."";
		$record = $db->query_first($sql);
		return $record;
	} 
	
	static function get_all_job_categories(){
		global $db;
    	$sql = "SELECT * FROM tbljob_categories ORDER BY category ASC";
		$rows = $db->fetch_all_array($sql);
		return $rows;
	}
	
	static function change_status( $active,$id){
		global $db;
		$data = array();
		$data['status'] 	= $active;
		$res = $db->query_update("tbljob_categories", $data, "id=".$id);
		return $res;
	}
	
	static function delete_category($id){
		global $db;
		$sql = "DELETE FROM tbljob_categories WHERE id =".$id;
		$res = $db->query($sql);
		return $res;
	}
	
	// front
	static function get_all_active_job_categories(){
		global $db;
    	$sql = "SELECT * FROM tbljob_categories WHERE status=1 ORDER BY category ASC";
		$rows = $db->fetch_all_array($sql);
		return $rows;
	}
	
	static function insert_candidate_data($data){
		global $db;
		$id=$db->query_insert("tblapplied_candidates", $data);
		return $id;
	}
	
	static function get_candidate_data_by_id($id){
		global $db;
    	$sql = "SELECT ac.*,jc.category as category_name
				  FROM tblapplied_candidates ac 
				  LEFT JOIN tbljob_categories jc ON jc.id=ac.category_id 	 
				  WHERE ac.id=".$id."";
		$record = $db->query_first($sql);
		return $record;
	}
	
	static function get_all_applied_candidates(){
		global $db;
    	$sql = "SELECT ac.*,jc.category as category_name
				  FROM tblapplied_candidates ac 
				  LEFT JOIN tbljob_categories jc ON jc.id=ac.category_id
				  ORDER BY ac.id DESC";
		$rows = $db->fetch_all_array($sql);
		return $rows;
	}
	
	static function get_active_jobcategory_by_id($id){
		global $db;
    	$sql = "SELECT *
				  FROM tbljob_categories 
				  WHERE id=".$id." AND status=1";
		$record = $db->query_first($sql);
		return $record;
	}
}

?>
