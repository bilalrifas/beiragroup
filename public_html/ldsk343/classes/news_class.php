<?php

class news{

	static function get_all_archives($year=NULL,$month=NULL){
		global $db;
		if($year!="" && $month!=""){
    	$sql = "SELECT *	  
				  FROM tblnews
				  WHERE DATE_FORMAT(news_date,'%m')='$month' 
				  AND DATE_FORMAT(news_date,'%Y')='$year' 
				  AND status=1
				  ORDER BY news_date DESC,id DESC";
		}else if($year!=""){
		$sql = "SELECT *	  
		  			FROM tblnews
		  			WHERE DATE_FORMAT(news_date,'%Y')='$year' 
					AND status=1
					ORDER BY news_date DESC,id DESC";
		}else{
		$sql = "SELECT *	  
				  FROM tblnews
				  WHERE status=1
				  ORDER BY news_date DESC,id DESC";
		}
				  
		$result = $db->fetch_all_array($sql);
		return $result;
	}
	
	static function get_latest_news(){
		global $db;
    	$sql = "SELECT *
						  FROM tblnews
						  WHERE status=1
						  ORDER BY id DESC";
		$result = $db->fetch_all_array($sql);
		return $result;
	}
	
	static function get_news_data_by_id($id){
		global $db;
    	$sql = "SELECT * FROM tblnews WHERE id ='".$id."'";
		$record = $db->query_first($sql);
		return $record;
	}
	
	static function get_other_latest_news($id){
		global $db;
    	$sql = "SELECT n.id,n.news_title,n.news_date
				  FROM tblnews n
				  WHERE n.status=1
				  AND n.id !=$id
				 ORDER BY n.news_date DESC,n.id DESC
				 LIMIT 10";
		$result = $db->fetch_all_array($sql);
		return $result;
	}
	
	static function get_all_news_by_category_id($category_id){
		global $db;
    	$sql = "SELECT  n.*,nc.category_name
				  FROM tblnews n
				  INNER JOIN tblnews_category nc ON n.category_id=nc.id
				  WHERE n.status=1 AND nc.status=1 AND nc.id=".$category_id."
				 ORDER BY n.news_date DESC,n.id DESC";
		$result = $db->fetch_all_array($sql);
		return $result;
	}
	
	static function get_news_category_by_id($category_id){
		global $db;
    	$sql = "SELECT * FROM tblnews_category WHERE id ='".$category_id."'";
		$record = $db->query_first($sql);
		return $record;
	}
        static function get_tv_latest_content(){
		global $db;
                $sql = "SELECT *
						  FROM tblhirutv
						  WHERE status=1
						  ORDER BY id DESC LIMIT 0,1";
		$result = $db->fetch_all_array($sql);
		return $result;
	}
}

?>
