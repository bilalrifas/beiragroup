<?php

require_once('PHPMailer_5.2.4/class.phpmailer.php');

class mailing{

    private static $username     = 'contactus@3cs.co';
    private static $password     = 'kR2Jy&p&Ng+7';

    private static $host         = '3cs.co';

    private static $enable_debug = false;
    private static $use_smpt     = false;

    private static $reply        = 'info@beiragroup.com';
    public static $from         = 'info@beiragroup.com';

    private static function create_object()
    {

       if( self::$enable_debug )
       {
            $mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
            $mail->SMTPDebug  = 2;                     // debugging: 1 = errors and messages, 2 = messages only
       }
       else
       {
            $mail = new PHPMailer(false); // the true param means it will throw exceptions on errors, which we need to catch
            $mail->SMTPDebug  = false;                    // debugging: 1 = errors and messages, 2 = messages only
       }

       if( self::$use_smpt )
       {
         $mail->IsSMTP(); // telling the class to use SMTP

         $mail->SMTPAuth   = true;                  // enable SMTP authentication
         $mail->SMTPSecure = "tls";                 // sets the prefix to the servier
         $mail->Host       = self::$host;      // sets OUTLOOK as the SMTP server
         $mail->Port       = 587;                   // set the SMTP port for the OUTLOOK server

         $mail->Username = self::$username; // OUTLOOK username
         $mail->Password = self::$password; // OUTLOOK password
       }
       else
       {
        $mail->IsMail();
       }



       return $mail;

    }


    static function html_mail($to,$subject,$body,$from=NULL,$reply=NULL,$attachments = array()){

        $to = trim($to);
        if( empty($to) || is_null($to) || count($to) < 0 || !filter_var($to, FILTER_VALIDATE_EMAIL) )
        {
            return false;
        }

        $message=" ";

        if( !is_null($from) && filter_var($from,FILTER_VALIDATE_EMAIL) ){
            $f = $from;
        }else{
            $f = self::$from;
        }



        if( !is_null($reply) && filter_var($reply,FILTER_VALIDATE_EMAIL) ){
            $r = $reply;
        }else{
            $r = self::$reply;
        }



        if( empty($subject) )
        {
            $subject = ' ';
        }

        if( empty($body) )
        {
            $body = ' ';
        }

        $message .= $body;

        $mail = self::create_object();

        $mail->AddReplyTo($r,$r);
        $mail->SetFrom($f,$f);

        $mail->Subject = $subject;
        $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically
        $mail->Body = $message;

        if( count($attachments) > 0 && isset($attachments[0]['path']) )
        {
            foreach ( $attachments as $attachment )
            {
                $attachment_name = ( isset($attachment['name']) ) ? $attachment['name'] : false;
                $mail->AddAttachment($attachment['path'], $attachment_name );
            }
        }

        $mail->AddAddress($to, $to);

        $send = $mail->Send();

        $mail->ClearAllRecipients();

        return $send;
    }


}

?>