<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-title"><i class="fa fa-user"></i>&nbsp;&nbsp;View Users</div>
                <div class="box-tools pull-right">
                    <!-- Button trigger add New User modal -->
                    <div style="margin-right: 115px;"><button class="btn btn-primary btn-flat" data-toggle="modal" data-target="#addNewUser"><i class="fa fa-user"></i>&nbsp;&nbsp;Add New User</button></div>
                </div>
            </div>
            <div class="box-body">
                <?php if (sizeof($all_datas) < 1) { ?>
                    <div class="alert alert-danger">
                        <strong>Sorry !</strong>&nbsp;&nbsp;&nbsp;No Data Found.
                    </div>
                <?php } else { ?>
                    <table  class="table table-condensed">
                        <thead>
                            <tr>
                                <th width="20%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >Name</th>
                                <th width="20%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >User Name</th>
                                <th width="25%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >Description</th>
                                <th width="35%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >&nbsp;</th>

                            </tr>
                        </thead>
                        <tbody>
                            <!--         	 	 	 	 	 	 	status-->
                            <?php foreach ($datas as $data) { ?>
                                <tr>
                                    <td><?php echo $data['name']; ?></td> 
                                    <td><?php echo $data['username']; ?></td> 
                                    <td><?php echo $data['description'] ?></td> 
                                    <td align="center">
                                        <button class="btn btn-default btn-flat btneditUser" data-toggle="modal" data-target="#editUser" eid="<?php echo $data['id']; ?>"><i class="fa fa-edit"></i>&nbsp;Edit</button>
                                        <a href="<?php echo $_SERVER['PHP_SELF'] . '?action=delete&did=' . $data['id'] . ''; ?>" onclick="return confirm('Are you sure to delete this news?');" class="btn btn-danger btn-flat" ><i class="fa fa-eraser"></i>&nbsp;Delete</a>
                                    </td> 
                                </tr>
                            <?php } ?>
                            <tr>
                                <td colspan="4" style="text-align: right;">
                                    <div class="pagination pagination-sm no-margin pull-right">
                                        <?php
                                        $links = $pager->getLinks();
                                        echo $links['all'];
                                        ?>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                <?php } ?>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-md-2 col-md-offset-10">
                        <button class="btn btn-default btn-flat btn-block btnback"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back</button>
                    </div>
                </div>   
            </div>
        </div>
    </div>
</div>


<!-- add new user Modal -->
<div class="modal fade" id="addNewUser" tabindex="-1" role="dialog" aria-labelledby="addNewUserLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="addNewUserLabel"><i class="fa fa-user"></i>&nbsp;&nbsp;Add User</h4>
            </div>
            <form name="frm_add_new_user" id="frm_add_new_user" enctype="multipart/form-data" action="ajax_data.php" method="post" role="form">
                <div id="msg_success" hidden="hidden">
                    <div class="alert alert-success ">
                        <i class="fa fa-check"></i>
                        <div id="msg_txt"></div>
                    </div>
                </div>
                <div id="msg_error" hidden="hidden">
                    <div class="alert alert-danger">
                        <i class="fa fa-check"></i>
                        <div id="msg_txt"></div>
                    </div>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-3"><label>Name</label></div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="name" id="name"  />
                        </div>
                    </div><br/>
                    <div class="row">
                        <div class="col-md-3"><label>Username</label></div>
                        <div class="col-md-4">
                            <input type="text" name="uname" id="uname" class="form-control" />
                        </div>
                    </div><br/>
                    <div class="row">
                        <div class="col-md-3"><label>Description</label></div>
                        <div class="col-md-8">
                            <textarea name="desc" id="desc" class="form-control"></textarea>
                        </div>
                    </div><br/>
                    <div class="row">
                        <div class="col-md-3"><label> User Role</label></div>
                        <div class="col-md-4">
                            <select name ="cat" id="status" class="form-control">
                                <option value="">Select Role</option>
                                <?php $allroles = getData::getUserGroup_data();
                                foreach ($allroles as $role) {
                                    ?>
                                    <option value="<?php echo $role['id']; ?>"  ><?php echo $role['group_name']; ?></option>
<?php } ?> 
                            </select>
                        </div>
                    </div>

                    <br/>
                    <div class="row">
                        <div class="col-md-3"><label>Password</label></div>
                        <div class="col-md-4">
                            <input type="password" name="pw" id="pw"  class="form-control" />
                        </div>
                    </div><br/>
                    <div class="row">
                        <div class="col-md-3"><label>Retype Password</label></div>
                        <div class="col-md-4">
                            <input type="password" name="repw" id="repw" class="form-control" />
                        </div>
                    </div><br/>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-flat">Save changes</button>
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                </div>
                <input type="hidden" name="actionrequest" value="adduser"/>
                <input type="hidden" name="type" value="0"/>
            </form>
        </div>
    </div>
</div>


<!-- edit user Modal -->
<div class="modal fade" id="editUser" tabindex="-1" role="dialog" rrecordis- aria-labelledby="editUserLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="editUserLabel"><i class="fa fa-user"></i>&nbsp;&nbsp;Edit User</h4>
            </div>
            <form name="frm_edit_new_user" id="frm_edit_new_user" enctype="multipart/form-data" action="ajax_data.php" method="post" role="form">
                <div id="msg_success" hidden="hidden">
                    <div class="alert alert-success ">
                        <i class="fa fa-check"></i>
                        <div id="msg_txt"></div>
                    </div>
                </div>
                <div id="msg_error" hidden="hidden">
                    <div class="alert alert-danger">
                        <i class="fa fa-check"></i>
                        <div id="msg_txt"></div>
                    </div>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-3"><label>Name</label></div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="name" id="name" />

                        </div>
                    </div><br/>
                    <div class="row">
                        <div class="col-md-3"><label>Username</label></div>
                        <div class="col-md-4">
                            <input disabled="disabled" type="text" name="uname" id="uname" class="form-control" />
                        </div>
                    </div><br/>
                    <div class="row">
                        <div class="col-md-3"><label>Description</label></div>
                        <div class="col-md-8">
                            <textarea name="desc" id="desc" class="form-control"></textarea>
                        </div>
                    </div><br/>
                    <div class="row">
                        <div class="col-md-3"><label> User Role</label></div>
                        <div class="col-md-4">
                            <select name ="cat" id="status" class="form-control">
                                <option value="">Select Role</option>
                                <?php $allroles = getData::getUserGroup_data();
                                foreach ($allroles as $role) {
                                    ?>
                                    <option value="<?php echo $role['id']; ?>"  ><?php echo $role['group_name']; ?></option>
                                <?php } ?>  
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-flat">Save changes</button>
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                </div>
                <input type="hidden" name="actionrequest" id="actionrequest" value="adduser"/>
                <input type="hidden" name="eid" id="eid"/>
                <input type="hidden" name="type" value="1"/>
            </form>
        </div>
    </div>
</div>