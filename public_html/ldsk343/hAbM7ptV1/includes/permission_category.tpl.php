<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-title"><i class="fa fa-tags"></i>&nbsp;&nbsp;Permission Category</div>
                <div class="box-tools pull-right">
                    <!-- Button trigger add new permission category modal -->
                    <div style="padding-right: 94px;"><button type="button" id="addnewcategory" class="btn btn-primary btn-flat " data-toggle="modal" data-target="#addnewpermissioncategory" ><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Add New Category</button></div>
                </div>
            </div>
            <div class="box-body">
                <?php if (sizeof($all_data) < 1) { ?>
                    <div class="alert alert-danger">
                        <strong>Sorry !</strong>&nbsp;&nbsp;&nbsp;No Data Found.
                    </div>
                <?php } else { ?>
                    <table  class="table table-condensed">
                        <thead>
                            <tr>
                                <th width="25%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >Category Name</th>
                                <th width="50%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >State</th>
                                <th width="25%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >&nbsp;</th>

                            </tr>
                        </thead>
                        <tbody>
                            <!--         	 	 	 	 	 	 	status-->
                            <?php foreach ($datas as $data) { ?>
                                <tr>
                                    <td><?php echo $data['catagory']; ?></td> 
                                    <td>
                                        <?php 
                                        
                                        if($data['status']==1){
                                            echo 'Active';
                                        }else{
                                            echo 'Inactive';
                                        }
                                    
                                        ?>
                                    </td> 
                                    <td>
                                        <button class="btn btn-default btn-flat btneditCategory" data-toggle="modal" data-target="#editpermissioncategory" eid="<?php echo $data['id']; ?>"><i class="fa fa-edit"></i>&nbsp;Edit</button>
                                        <a href="<?php echo $_SERVER['PHP_SELF'] . '?action=delete&did=' . $data['id'] . ''; ?>" onclick="return confirm('Are you sure to delete this category?');" class=" btn btn-danger flat" ><b class="fa fa-eraser"></b>&nbsp;&nbsp;Delete</a>
                                    </td> 
                                </tr>
                            <?php } ?>
                            <tr>
                                <td colspan="4" style="text-align: right;">
                                    <div class="pagination" style="float: right;">
                                        <?php
                                        $links = $pager->getLinks();
                                        echo $links['all'];
                                        ?>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                <?php } ?>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-md-2 col-md-offset-10">
                        <button class="btn btn-default btn-flat btn-block btnback"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back</button>
                    </div>
                </div>   
            </div>
        </div>
    </div>
</div>

<!-- add new permission category modal -->
<?php include_once 'addpermissioncategory.tpl.php'; ?>

<!-- edit permission category modal -->
<div class="modal fade" id="editpermissioncategory" tabindex="-1" role="dialog" aria-labelledby="editpermissioncategoryLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="editpermissioncategoryLabel"><i class="fa fa-tag"></i>&nbsp;&nbsp;Edit Permission Category</h4>
            </div>
            <form  name="frm_edit_per_cat" id="frm_edit_per_cat" enctype="multipart/form-data" action="ajax_data.php" method="post">
                <div id="msg_success" hidden="hidden">
                    <div class="alert alert-success ">
                        <i class="fa fa-check"></i>
                        <div id="msg_txt"></div>
                    </div>
                </div>
                <div id="msg_error" hidden="hidden">
                    <div class="alert alert-danger">
                        <i class="fa fa-check"></i>
                        <div id="msg_txt"></div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3"><label class="control-label" for="inputname">Category Name</label></div>
                        <div class="col-md-4">
                            <input type="text" name="name" id="name" class="form-control" />
                        </div>
                    </div><br/>
                    <div class="row">
                        <div class="col-md-3"><label class="control-label" for="inputname">Status</label></div>
                        <div class="col-md-4">
                            <select name ="status" id="status" class="form-control">
                                <option value="1" selected >Active</option>
                                <option value="0" >Deactive</option>
                            </select>
                        </div>
                    </div>    
                </div>
                <div class="modal-footer">
                    <button type="submit" name="btnadd" id="btnadd" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Add Permission Category</button>
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                </div>
                <input type="hidden" name="actionrequest" id="actionrequest" value="addpermiscat"/>
                <input type="hidden" name="type" id="type" value="1"/>
                <input type="hidden" name="eid" id="eid" value=""/>
            </form>
        </div>
    </div>
</div>