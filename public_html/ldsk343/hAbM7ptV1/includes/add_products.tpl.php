<div class="row">
    <div class="col-md-12">
    <div class="box box-primary">
    <div class="box-header">
        <div class="box-title"></div>
    </div>
    <div class="box-body">
    <div class="row">
    <div class="col-md-8 col-md-offset-2">
        <form action="" method="POST" role="form" enctype="multipart/form-data">
            <legend><i class="fa fa-plus-square"></i>&nbsp;&nbsp;<?php echo $page_headings ?></legend>

            <div class="form-group">
                <label for="category_id">Main Category Type</label>
                <select name="category_id" id="main_cat_sel" class="form-control" required="required" onchange="load_subcategories(this.value,'<?php echo $subcategory_id ?>')" >
                    <option value="">Select a Category</option>
                    <?php foreach ($category_array as $key => $value): ?>
                        <option value="<?php echo $value['id'] ?>" <?php if ($value['id']==$category_id): ?> selected <?php endif ?> > <?php echo $value['title'] ?></option>
                    <?php endforeach ?>
                </select>
            </div>

            <div class="form-group">
                <label for="subcategory_id">Sub Category Type</label>
                <select name="subcategory_id" id="sub_cat_sel" class="form-control" required="required">
                    <option value="">Select a Sub Category</option>
                </select>
            </div>

            <div class="form-group">
                <label for="product_id">Product ID</label>
                <input type="text" class="form-control" name="product_id" value="<?php echo $product_id ?>" placeholder="Enter Product ID" required="required">
            </div>
            <div class="form-group">
                <label for="name">Product Name</label>
                <input type="text" class="form-control" name="name" value="<?php echo $name ?>" placeholder="Enter Product Name">
            </div>
            <div class="form-group">
                <label for="thumbnail">Main Thumbnail</label>
                <?php if (!file_exists("../images/content/products/$thumbnail") || $thumbnail==""): ?>
                    <input type="file" name="thumbnail" id="thumbnail" class="" required="required" onchange="return checkImage('thumbnail')">
                <?php else: ?>
                    <div class="row">
                    <div class="col-md-4">
                        <a href="../images/content/products/<?php echo $thumbnail; ?>" data-toggle="lightbox">
                            <img src="../images/content/products/<?php echo $thumbnail; ?>" class="img-responsive">
                        </a>
                        <a class="btn btn-danger btn-flat btn-block" href="<?php echo $_SERVER['PHP_SELF'] . '?action=delete_content_image&id=' . $id . '&image=' . $thumbnail . '&imageno=thumbnail'; ?>" onclick="return confirm('Are you sure to delete this image?');"><i class="fa fa-eraser"></i>&nbsp;&nbsp;Delete</a>
                    </div>
                    </div>

                <?php endif ?>
            </div>
            <div class="form-group">
                <label for="first_desc">Product Description</label>
                <input type="text" class="form-control" name="first_desc" value="<?php echo $first_desc ?>" placeholder="Enter a Description">
            </div>
            <!-- <div class="form-group">
                <label for="second_desc">1<sup>st</sup> Panel Bold Description</label>
                <input type="text" class="form-control" name="second_desc" value="<?php /*echo $second_desc*/ ?>" placeholder="Enter a Description" >
            </div>
            <div class="form-group">
                <label for="first_image">1<sup>st</sup> Panel Image</label>
                <?php /*if (!file_exists("../images/content/products/$first_image") || $first_image==""):*/ ?>
                    <input type="file" name="first_image" id="first_image" class=""  onchange="return checkImage('first_image')">
                <?php /*else:*/ ?>
                    <div class="row">
                    <div class="col-md-4">
                    <a href="../images/content/products/<?php/* echo $first_image;*/ ?>" data-toggle="lightbox">
                        <img src="../images/content/products/<?php /*echo $first_image;*/ ?>" class="img-responsive">
                    </a>
                    <a class="btn btn-danger btn-flat btn-block" href="<?php /*echo $_SERVER['PHP_SELF'] . '?action=delete_content_image&id=' . $id . '&image=' . $first_image . '&imageno=first_image';*/ ?>" onclick="return confirm('Are you sure to delete this image?');"><i class="fa fa-eraser"></i>&nbsp;&nbsp;Delete</a>
                    </div>
                    </div>
                <?php /*endif*/ ?>
            </div>
            <div class="form-group">
                <label for="third_desc">2<sup>nd</sup> Panel Normal Description</label>
                <input type="text" class="form-control" name="third_desc" value="<?php /*echo $third_desc*/ ?>" placeholder="Enter a Description" >
            </div>
            <div class="form-group">
                <label for="forth_desc">2<sup>nd</sup> Panel Bold Description</label>
                <input type="text" class="form-control" name="forth_desc" value="<?php /*echo $forth_desc*/ ?>" placeholder="Enter a Description" >
            </div>
            <div class="form-group">
                <label for="second_image">2<sup>nd</sup> Panel Image</label>
                <?php /*if (!file_exists("../images/content/products/$second_image") || $second_image==""):*/ ?>
                    <input type="file" name="second_image" id="second_image" class=""  onchange="return checkImage('second_image')">
                <?php /*else:*/ ?>
                    <div class="row">
                    <div class="col-md-4">
                    <a href="../images/content/products/<?php /*echo $second_image;*/ ?>" data-toggle="lightbox">
                        <img src="../images/content/products/<?php /*echo $second_image;*/ ?>" class="img-responsive">
                    </a>
                    <a class="btn btn-danger btn-flat btn-block" href="<?php /*echo $_SERVER['PHP_SELF'] . '?action=delete_content_image&id=' . $id . '&image=' . $second_image . '&imageno=second_image';*/ ?>" onclick="return confirm('Are you sure to delete this image?');"><i class="fa fa-eraser"></i>&nbsp;&nbsp;Delete</a>
                    </div>
                    </div>
                <?php /*endif*/ ?>
            </div>
            <div class="form-group">
                <label for="fifth_desc">3<sup>rd</sup> Panel Bold Description</label>
                <input type="text" class="form-control" name="fifth_desc" value="<?php /*echo $fifth_desc*/ ?>" placeholder="Enter a Description" >
            </div>
            <div class="form-group">
                <label for="third_image">3<sup>rd</sup> Panel Image</label>
                <?php /*if (!file_exists("../images/content/products/$third_image") || $third_image==""):*/ ?>
                    <input type="file" name="third_image" id="third_image" class=""  onchange="return checkImage('third_image')">
                <?php /*else:*/ ?>
                    <div class="row">
                    <div class="col-md-4">
                    <a href="../images/content/products/<?php /*echo $third_image;*/ ?>" data-toggle="lightbox">
                        <img src="../images/content/products/<?php /*echo $third_image;*/ ?>" class="img-responsive">
                    </a>
                    <a class="btn btn-danger btn-flat btn-block" href="<?php /*echo $_SERVER['PHP_SELF'] . '?action=delete_content_image&id=' . $id . '&image=' . $third_image . '&imageno=third_image';*/ ?>" onclick="return confirm('Are you sure to delete this image?');"><i class="fa fa-eraser"></i>&nbsp;&nbsp;Delete</a>
                    </div>
                    </div>
                <?php /*endif*/ ?>
            </div> -->

            <?php if (isset($_GET['id']) && is_numeric($_GET['id'])):?>
            <button type="submit" name="btnedit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp;Edit Product</button>
            <input type="hidden" name="id" value="<?php echo $id ?>">
            <?php else: ?>
            <button type="submit" name="btnadd" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp;Add Product</button>
            <?php endif ?>
        </form>
    </div>
    </div>
    </div>
    <div class="box-footer">
    <div class="row">
        <div class="col-md-2 col-md-offset-10">
            <button class="btn btn-default btn-flat btn-block btnback"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back</button>
        </div>
    </div>
    </div>
    </div>
    </div>
</div>
