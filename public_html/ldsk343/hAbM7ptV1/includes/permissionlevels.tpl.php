<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-title"><i class="fa fa-copy"></i>&nbsp;&nbsp;Permission Levels</div>
                <div class="box-tools pull-right">
                    <!-- Button trigger modify permission level modal -->
                    <div style="margin-right: 75px;"><button class="btn btn-default btn-flat" data-toggle="modal" data-target="#modifyPermissionLevel"><i class="fa fa-copy"></i>&nbsp;&nbsp;Add New Permission Level</button></div>
                </div>
            </div>
            <div class="box-body">
                <?php if (sizeof($all_data) < 1) { ?>
                    <div class="alert alert-danger">
                        <strong>Sorry !</strong>&nbsp;&nbsp;&nbsp;No Data Found.
                    </div>
                <?php } else { ?>
                    <table  class="table table-condensed">
                        <thead>
                            <tr>
                                <th width="25%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >Page Name</th>
                                <th width="50%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >Description</th>
                                <th width="25%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >&nbsp;</th>

                            </tr>
                        </thead>
                        <tbody>
                            <!--         	 	 	 	 	 	 	status-->
                            <?php foreach ($datas as $data) { ?>
                                <tr>
                                    <td><?php echo $data['page_name']; ?></td> 
                                    <td><?php echo $data['description'] ?></td> 
                                    <td>
                                        <button type="button" class="btn btn-default btn-flat btnModifyPermiLevel" data-toggle="modal" data-target="#editmodifyPermissionLevel" eid="<?php echo $data['id']; ?>" ><i class="fa fa-edit"></i>&nbsp;Edit</button>
                                        <a href="<?php echo $_SERVER['PHP_SELF'] . '?action=delete&did=' . $data['id'] . ''; ?>" onclick="return confirm('Are you sure to delete this news?');" class=" btn btn-danger btn-flat" ><i class="fa fa-eraser"></i>&nbsp;Delete</a>
                                    </td> 
                                </tr>
                            <?php } ?>
                            <tr>
                                <td colspan="4" style="text-align: right;">
                                    <div class="pagination" style="float: right;">
                                        <?php
                                        $links = $pager->getLinks();
                                        echo $links['all'];
                                        ?>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                <?php } ?>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-md-2 col-md-offset-10">
                        <button class="btn btn-default btn-flat btn-block btnback"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back</button>
                    </div>
                </div>   
            </div>
        </div>
    </div>
</div>

<!-- modify permission level modal -->
<div class="modal fade" id="modifyPermissionLevel" tabindex="-1" role="dialog" aria-labelledby="modifyPermissionLevelLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="modifyPermissionLevelLabel"><i class="fa fa-file"></i>&nbsp;&nbsp;Add New Permission Level</h4>
            </div>
            <form name="frm_mod_per_level" id="frm_mod_per_level" enctype="multipart/form-data" action="ajax_data.php" method="post">
                <div id="msg_success" hidden="hidden">
                    <div class="alert alert-success ">
                        <i class="fa fa-check"></i>
                        <div id="msg_txt"></div>
                    </div>
                </div>
                <div id="msg_error" hidden="hidden">
                    <div class="alert alert-danger">
                        <i class="fa fa-check"></i>
                        <div id="msg_txt"></div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label" for="inputname">Page Name</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="name" id="name" value="" class="form-control" />
                        </div>
                    </div><br/>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label" for="inputname">Page Description</label>
                        </div>    
                        <div class="col-md-6">
                            <textarea name="desc" id="desc" class="form-control"></textarea>
                        </div>
                    </div><br/>
                    
                    <div class="row">
                        <div class="col-md-4"><label class="control-label" for="inputname">Permission Category</label></div>
                        <div class="col-md-6">
                            <select name="cat" id="cat" class="form-control" >
                                <option value="" >Select Category</option>  
                                <?php
                                $allcat = getData::getAllpermission_categories();
                                foreach ($allcat as $category) { ?>
                                    <option  value="<?php echo $category['id']; ?>"><?php echo $category['catagory']; ?></option>
                                    <?php } ?>
                            </select>
                            <!-- Button trigger add new permission category modal -->
                            <button type="button" id="addnewcategory" class="btn btn-primary btn-flat btn-block" data-toggle="modal" data-target="#addnewpermissioncategory" ><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Add New Category</button>
                        </div>
                    </div><br/>
                    
                    <div class="row">
                        <div class="col-md-4"><label class="control-label" for="inputname">Status</label></div>
                        <div class="col-md-6">
                            <select name ="status" id="status" class="form-control">
                                <option value="1">Active</option>
                                <option value="0">Deactivate</option>
                            </select>
                        </div>
                    </div>     
                </div>
                <div class="modal-footer">
                    <button type="submit" name="btnadd" id="btnadd" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Add Permission Level</button>
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                </div>
                <input type="hidden" name="actionrequest" id="actionrequest" value="addpermislevel"/>
                <input type="hidden" name="type" value="0"/>
                <input type="hidden" name="eid" value=""/>
            </form>
        </div>
    </div>
</div>


<!-- add new permission category modal -->
<?php include_once 'addpermissioncategory.tpl.php'; ?>

<!-- edit modify permission level modal -->

<div class="modal fade" id="editmodifyPermissionLevel" tabindex="-1" role="dialog" aria-labelledby="editmodifyPermissionLevelLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="editmodifyPermissionLevelLabel"><i class="fa fa-file"></i>&nbsp;&nbsp;Edit Permission Level</h4>
            </div>
            <form name="frm_edit_mod_per_level" id="frm_edit_mod_per_level" enctype="multipart/form-data" action="ajax_data.php" method="post">
                <div id="msg_success" hidden="hidden">
                    <div class="alert alert-success ">
                        <i class="fa fa-check"></i>
                        <div id="msg_txt"></div>
                    </div>
                </div>
                <div id="msg_error" hidden="hidden">
                    <div class="alert alert-danger">
                        <i class="fa fa-check"></i>
                        <div id="msg_txt"></div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label" for="inputname">Page Name</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="name" id="name" value="" class="form-control" />
                        </div>
                    </div><br/>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label" for="inputname">Page Description</label>
                        </div>    
                        <div class="col-md-6">
                            <textarea name="desc" id="desc" class="form-control"></textarea>
                        </div>
                    </div><br/>

                    <div class="row">
                        <div class="col-md-4"><label class="control-label" for="inputname">Permission Category</label></div>
                        <div class="col-md-6">
                            <div id="permission_cat">
                                <select name="cat" id="cat" class="form-control" >
                                    <option value="" >Select Category</option>  
                                    <?php
                                    $allcat = getData::getAllpermission_categories();
                                    foreach ($allcat as $category) {
                                        ?>
                                        <option  value="<?php echo $category['id']; ?>"><?php echo $category['catagory']; ?></option>
                                    <?php } ?>
                                </select>
                                <!-- Button trigger add new permission category modal -->
                                <button type="button" id="addnewcategory" class="btn btn-primary btn-flat btn-block" data-toggle="modal" data-target="#addnewpermissioncategory" ><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Add New Category</button>
                            </div>
                        </div>
                    </div><br/>

                    <div class="row">
                        <div class="col-md-4"><label class="control-label" for="inputname">Status</label></div>
                        <div class="col-md-6">
                            <div id="permission_state">
                                <select name ="status" id="status" class="form-control">
                                    <option value="1">Active</option>
                                    <option value="0">Deactivate</option>
                                </select>
                            </div>
                        </div>
                    </div>     
                </div>
                <div class="modal-footer">
                    <button type="submit" name="btnadd" id="btnadd" class="btn btn-primary btn-flat"><i class="fa fa-edit"></i>&nbsp;&nbsp;&nbsp;Update Permission Level</button>
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                </div>
                <input type="hidden" name="actionrequest" id="actionrequest" value="addpermislevel"/>
                <input type="hidden" name="type" id="type" value="1"/>
                <input type="hidden" name="eid" id="eid" value=""/>
            </form>
        </div>
    </div>
</div>