<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-title"><i class="fa fa-envelope"></i>&nbsp;&nbsp;<?php if(isset($_GET['id'])): echo 'Edit Admin Email';else: echo 'Add Admin Email';endif;  ?></div>
            </div>
            <div class="box-body">
                <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">

                    <div class="row">
                        <div class="col-md-3"><label class="control-label" for="inputtitle">Name</label></div>
                        <div class="col-md-4">
                            <input type="text" name="name" id="name" class="form-control" value="<?php echo $name; ?>">
                        </div>
                    </div><br/>

                    <div class="row">
                        <div class="col-md-3"><label class="control-label" for="inputtitle">Email</label></div>
                        <div class="col-md-4">
                            <input type="text" name="email" id="email" class="form-control" value="<?php echo $email; ?>">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-md-offset-3">
                            <label class="checkbox">
                                <?php if (isset($_GET['id'])) { ?>
                                    <input type="hidden" name="id" value="<?php echo $id; ?>" />
                                <?php } ?>
                            </label>
                            <?php if (isset($_GET['id'])) { ?>  
                            <button type="submit" name="btnedit" class="btn btn-primary btn-flat btn-block"><i class="fa fa-edit"></i>&nbsp;&nbsp;&nbsp;Update Details</button>
                            <?php } else { ?>    
                            <button type="submit" name="btnsave" class="btn btn-primary btn-flat btn-block"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Save Details</button>
                            <?php } ?>
                        </div>
                    </div>
                </form>  
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-md-2 col-md-offset-10">
                        <button class="btn btn-default btn-flat btn-block btnback"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back</button>
                    </div>
                </div>   
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-title"><i class="fa fa-envelope-o"></i>&nbsp;&nbsp;Admin Emails</div>
            </div>
            <div class="box-body">
                <?php if (sizeof($all_mails) < 1) { ?>
                    <div class="alert alert-danger">
                        <strong>Sorry !</strong>&nbsp;&nbsp;&nbsp;No Data Found.
                    </div>
                <?php } else { ?>
                    <table  class="table table-condensed">
                        <thead>
                            <tr>
                                <th width="35%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >Name</th>
                                <th width="15%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >Email</th>
                                <th width="15%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >Status</th>
                                <th width="20%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!--         	name 	descriptions 	 	status 	map 	image-->
                            <?php foreach ($mails as $data) { ?>
                                <tr>
                                    <td><?php echo $data['name'] ?></td> 
                                    <td><?php echo $data['email'] ?></td> 
                                    <td>
                                        <div class="btn-group">
                                            <button class="btn btn-primary btn-flat"><?php
                                                if ($data['activated'] == 1) {
                                                    echo "Active";
                                                } else {
                                                    echo "Inactive";
                                                }
                                                ?></button>
                                            <button class="btn btn-primary btn-flat dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <!-- dropdown menu links -->
                                                <li>
                                                    <?php if ($data['activated'] == 1) {
                                                        ?>
                                                        <a href="<?php echo $_SERVER['PHP_SELF'] . '?action=status&val=0&id=' . $data['id'] . ''; ?>" >Change to Inactive</a>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <a href="<?php echo $_SERVER['PHP_SELF'] . '?action=status&val=1&id=' . $data['id'] . ''; ?>" >Change to Active</a>
                                                    <?php }
                                                    ?>
                                                </li>
                                            </ul>
                                        </div>
                                    </td> 
                                    <td>
                                        <a href="adminmails.php?id=<?php echo $data['id']; ?>" class="btn btn-default btn-flat" ><i class="fa fa-edit"></i>&nbsp;&nbsp;Edit</a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="<?php echo $_SERVER['PHP_SELF'] . '?action=delete&id=' . $data['id'] . ''; ?>" onclick="return confirm('Are you sure to delete this E-mail?');" class=" btn btn-danger btn-flat" ><i class="fa fa-eraser"></i>&nbsp;&nbsp;Delete</a>
                                    </td> 
                                </tr>
                    <?php } ?>
                            <tr>
                                <td colspan="5" style="text-align: right;">
                                    <div class="pagination" style="float: right;">
                                        <?php
                                        $links = $pager->getLinks();
                                        echo $links['all'];
                                        ?>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                <?php } ?>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-md-2 col-md-offset-10">
                        <button class="btn btn-default btn-flat btn-block btnback"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back</button>
                    </div>
                </div>   
            </div>
        </div>
    </div>
</div>