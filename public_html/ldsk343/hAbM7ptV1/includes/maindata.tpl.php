<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-title"><i class="fa fa-tasks"></i>&nbsp;&nbsp;<?php echo $temp_heading; ?></div>
            </div>
            <div class="box-body">
                <form action="" method="post" enctype="multipart/form-data" accept-charset="utf-8">
                    <div class="row">
                        <div class="col-md-3"><label class="control-label" for="inputtitle">Company Name</label></div>
                        <div class="col-md-7">
                            <input type="text" name="companyname"  class="form-control" value="<?php echo $companyname; ?>">
                        </div>
                    </div><br/>

                    <div class="box-header">
                    <div class="box-title"><i class="fa fa-building-o"></i>&nbsp;&nbsp;HEAD OFFICE</div>
                    </div>

                    <div class="row">
                        <div class="col-md-3"><label class="control-label" for="inputtitle">Address</label></div>
                        <div class="col-md-7">
                            <textarea name="address1" rows="5" class="form-control tinyEditor"><?php echo $address1; ?></textarea>
                        </div>
                    </div><br/>

                    <div class="row">
                        <div class="col-md-3"><label class="control-label" for="inputtitle">Telephone Number 1</label></div>
                        <div class="col-md-4">
                            <input type="text"  class="form-control" name="telephone1" value="<?php echo $telephone1; ?>">
                        </div>
                    </div><br/>
                    <div class="row">
                        <div class="col-md-3"><label class="control-label" for="inputtitle">Telephone Number 2</label></div>
                        <div class="col-md-4">
                            <input type="text" name="telephone2"   class="form-control"  value="<?php echo $telephone2; ?>">
                        </div>
                    </div><br/>

                    <div class="row">
                        <div class="col-md-3"><label class="control-label" for="inputtitle">Fax Number </label></div>
                        <div class="col-md-4">
                            <input type="text" name="fax1"   class="form-control" value="<?php echo $fax1; ?>">
                        </div>
                    </div><br/>

                    <div class="row">
                        <div class="col-md-3"><label class="control-label" for="inputtitle">Email </label></div>
                        <div class="col-md-4">
                            <input type="text" name="email1"  class="form-control" value="<?php echo $email1; ?>">
                        </div>
                    </div><br/>

                    <div class="box-header">
                    <div class="box-title"><i class="fa fa-truck"></i>&nbsp;&nbsp;Factories</div>
                    </div>

                    <div class="row">
                        <div class="col-md-3"><label class="control-label" for="inputtitle">1<sup>st</sup> Factory Name </label></div>
                        <div class="col-md-7">
                            <textarea name="details" rows="5" class="form-control"><?php echo $details; ?></textarea>
                        </div>
                    </div><br/>

                    <div class="row">
                        <div class="col-md-3"><label class="control-label" for="inputtitle">1<sup>st</sup> Factory Address </label></div>
                        <div class="col-md-7">
                            <textarea name="address2" rows="5" class="form-control tinyEditor"><?php echo $address2; ?></textarea>
                        </div>
                    </div><br/>


                    <div class="row">
                        <div class="col-md-3"><label class="control-label" for="inputtitle">2<sup>nd</sup> Factory Name</label></div>
                        <div class="col-md-7">
                            <textarea name="details2" rows="5" class="form-control"><?php echo $details2; ?></textarea>
                        </div>
                    </div><br/>

                    <div class="row">
                        <div class="col-md-3"><label class="control-label" for="inputtitle">2<sup>nd</sup> Factory Address </label></div>
                        <div class="col-md-7">
                            <textarea name="address3" rows="5" class="form-control tinyEditor"><?php echo $address3; ?></textarea>
                        </div>
                    </div><br/>

                    <div class="row">
                        <div class="col-md-3"><label class="control-label" for="inputtitle">3<sup>rd</sup> Factory Name</label></div>
                        <div class="col-md-7">
                            <textarea name="details3" rows="5" class="form-control"><?php echo $details3; ?></textarea>
                        </div>
                    </div><br/>

                    <div class="row">
                        <div class="col-md-3"><label class="control-label" for="inputtitle">3<sup>rd</sup> Factory Address </label></div>
                        <div class="col-md-7">
                            <textarea name="address4" rows="5" class="form-control tinyEditor"><?php echo $address4; ?></textarea>
                        </div>
                    </div><br/>

                    <div class="box-header">
                    <div class="box-title"><i class="fa-trash-o"></i>&nbsp;&nbsp;Other</div>
                    </div>

                    <div class="row">
                        <div class="col-md-3"><label class="control-label" for="inputtitle">Telephone Number 3</label></div>
                        <div class="col-md-4">
                            <input type="text" name="telephone3"   class="form-control" value="<?php echo $telephone3; ?>">
                        </div>
                    </div><br/>



                    <div class="row">
                        <div class="col-md-3"><label class="control-label" for="inputtitle">Fax Number 2</label></div>
                        <div class="col-md-4">
                            <input type="text" name="fax2"   class="form-control" value="<?php echo $fax2; ?>">
                        </div>
                    </div><br/>

                    <div class="row">
                        <div class="col-md-3"><label class="control-label" for="inputtitle">Email 2</label></div>
                        <div class="col-md-4">
                            <input type="text" name="email2"  class="form-control" value="<?php echo $email2; ?>">
                        </div>
                    </div><br/>
                    <div class="row">
                        <div class="col-md-3"><label class="control-label" for="inputtitle">Facebook Link</label></div>
                        <div class="col-md-7">
                            <input type="text" name="fb_link" class="form-control" value="<?php echo $fb_link; ?>">
                        </div>
                    </div><br/>
                    <div class="row">
                        <div class="col-md-3"><label class="control-label" for="inputtitle">Google + Link</label></div>
                        <div class="col-md-7">
                            <input type="text" name="g_link" class="form-control" value="<?php echo $g_link; ?>">
                        </div>
                    </div><br/>
                    <div class="row">
                        <div class="col-md-3"><label class="control-label" for="inputtitle">Twitter Link</label></div>
                        <div class="col-md-7">
                            <input type="text" name="twiter_link" class="form-control" value="<?php echo $twiter_link; ?>">
                        </div>
                    </div><br/>
                    <div class="row">
                        <div class="col-md-3"><label class="control-label" for="inputtitle">Linkedin Link</label></div>
                        <div class="col-md-7">
                            <input type="text" name="linkedin_link" class="form-control" value="<?php echo $linkedin_link; ?>">
                        </div>
                    </div><br/>
                    <div class="row">
                        <div class="col-md-3"><label class="control-label" for="inputtitle">Pinterest Link</label></div>
                        <div class="col-md-7">
                            <input type="text" name="pinterest_link" class="form-control" value="<?php echo $pinterest_link; ?>">
                        </div>
                    </div><br/>
                    <div class="row">
                        <div class="col-md-3"><label class="control-label" for="inputtitle">Flickr Link</label></div>
                        <div class="col-md-7">
                            <input type="text" name="flickr_link" class="form-control" value="<?php echo $flickr_link; ?>">
                        </div>
                    </div><br/>
                    <div class="row">
                        <div class="col-md-3"><label class="control-label" for="inputtitle">SEO Code</label></div>
                        <div class="col-md-7">
                            <textarea name="seo_code" rows="5" class="form-control"><?php echo $seo_code; ?></textarea>
                        </div>
                    </div><br/>

                    <!--    //tel1 	tel2 	tel3 	fax1 	fax2 	email1 	email2 	fb_link 	g+_link 	twiter_link 	linkedin_link 	pinterest_link 	SEO_code 	online-->

                    <div class="row">
                        <div class="col-md-3"><label class="control-label" for="inputtitle">Site Availability</label></div>
                        <div class="col-md-3">
                            <select name="online" class="form-control">
                                <option <?php if ($online == 1) {
                                echo "selected";
                            } ?> value="1">online</option>
                                                            <option <?php if ($online == 0) {
                                echo "selected";
                            } ?> value="0">offline</option>
                            </select>
                        </div>
                    </div><br/>

                    <div class="row">
                        <div class="col-md-7 col-md-offset-3">
                            <button type="submit" name="btnsave" class="btn btn-primary btn-flat btn-block"><i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp;Save Details</button>
                        </div>
                    </div><br>
                </form>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-md-2 col-md-offset-10">
                        <button class="btn btn-default btn-flat btn-block btnback"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>