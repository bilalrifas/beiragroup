<?php
require_once('admin.php');
$per_tag = new Permission;
$per_tag->premission_tag = "sub_content";
$permission_block = $per_tag->check_permission($db);
if (!$permission_block) {
    $INCLUDE_FILE = "permission_denied.php";
    require_once('template_main.php');
    exit();
}
//------------------

$err = "";

$maincontent_id = "";
$details = "";
$spe_heading = "";
$spe_details = "";
$heading = "";

//meta data
$meta_title = "";
$meta_desc = "";
$meta_key = "";
$image1 = "";
$image2 = "";
$image3 = "";
if (isset($_GET) && isset($_GET['action'])) {

    switch ($_GET['action']) {

        case 'delete_content_image':

            if (isset($_GET['sub_id']) && isset($_GET['image']) && isset($_GET['imageno'])) {
                $sub_id = $_GET['sub_id'];
                $maincontent_id = $_GET['maincontent_id'];
                $image = $_GET['image'];

                $data[$_GET['imageno']] = "";

                $result = $db->query_update("tblsub_content", $data, "subcontent_id=" . $sub_id);

                if (file_exists(DOC_ROOT . 'images/content/banner/' . $image)) {
                    $unlink = @unlink(DOC_ROOT . 'images/content/banner/' . $image);
                }

                //**************** generate log entry *******************
                $logString = "delete content image, sub_id - " . $sub_id . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************

                if ($result) {
                    header('location:sub_content.php?maincontent_id=' . $maincontent_id  . '&subcontent_id=' . $sub_id. '&msg=' . base64_encode(8));
                    exit;
                } else {
                    header('location:sub_content.php?maincontent_id=' . $maincontent_id  . '&subcontent_id=' . $sub_id. '&msg=' . base64_encode(5));
                    exit;
                }
            }
            break;
    }
}

if (isset($_GET) && ($_GET['maincontent_id'] != "") && (is_numeric($_GET['maincontent_id']))) {

    $maincontent_id = $_GET['maincontent_id'];

    //get main content by id
    $main_content = $db->query_first("SELECT * FROM tblmain_content WHERE maincontent_id ='" . $maincontent_id . "'");

    if ($main_content) {
        $maincontent_id = $main_content['maincontent_id'];
        $mainheading = $main_content['heading'];

        if ((isset($_GET['subcontent_id'])) && (is_numeric($_GET['subcontent_id']))) {

            //get sub content by id
            $sub_content = $db->query_first("SELECT * FROM tblsub_content WHERE subcontent_id ='" . $_GET['subcontent_id'] . "'");

            if ($sub_content) {
                $subcontent_id = $sub_content['subcontent_id'];
                $maincontent_id = $sub_content['maincontent_id'];
                $details = $sub_content['details'];
                $heading = $sub_content['heading'];
                $spe_details = $sub_content['spe_details'];
                $spe_heading = $sub_content['spe_heading'];
                $meta_title = $sub_content['meta_title'];
                $meta_desc = $sub_content['meta_desc'];
                $meta_key = $sub_content['meta_key'];
                $image1 = $sub_content['image1'];
                $image2 = $sub_content['image2'];
                $image3 = $sub_content['image3'];
                $type = $heading;
            } else {
                header('location:main_content.php?maincontent_id=$maincontent_id');
                exit;
            }
        }
    } else {
        header('location:dashboard.php');
        exit;
    }
} else {
    header('location:dashboard.php');
    exit;
}

if ($_POST == true) {

    $err = "";

    $maincontent_id = $_POST['maincontent_id'];
    $heading = trim($_POST['heading']);
    $details = $_POST['details'];

    $spe_heading = trim(str_replace('<p>', '', $_POST['spe_heading']));
    $spe_heading = trim(str_replace('</p>', '', $spe_heading));

    $spe_details = trim(str_replace('<p>', '', $_POST['spe_details']));
    $spe_details = trim(str_replace('</p>', '', $spe_details));
    $meta_title = trim($_POST['meta_title']);
    $meta_desc = trim($_POST['meta_desc']);
    $meta_key = trim($_POST['meta_key']);

    if ($heading == "") {
        $err = $err . "<li>Please enter title</li>";
    }

    if ($err == "") {

        $data_arr = array();
        $data_arr['maincontent_id'] = $maincontent_id;
        $data_arr['heading'] = $heading;
        $data_arr['details'] = $details;
        $data_arr['spe_heading'] = $spe_heading;
        $data_arr['spe_details'] = $spe_details;
        $data_arr['meta_title'] = $meta_title;
        $data_arr['meta_desc'] = $meta_desc;
        $data_arr['meta_key'] = $meta_key;

        if (isset($_FILES) && $_FILES['image1']['name'] != "") {

            $upload_image1 = upload::upload_images(DOC_ROOT . 'images/content/banner/', $_FILES['image1']);

            $imagename1 = str_replace(" ", "", $upload_image1);

            $temp_image_url = DOC_ROOT . 'images/content/banner/' . $upload_image1;
            $new_image_url = DOC_ROOT . 'images/content/banner/' . $imagename1;

            rename($temp_image_url, $new_image_url);

            $data_arr['image1'] = $imagename1;
        }

        if (isset($_FILES) && $_FILES['image2']['name'] != "") {

            $upload_image2 = upload::upload_images(DOC_ROOT . 'images/content/banner/', $_FILES['image2']);

            $imagename2 = str_replace(" ", "", $upload_image2);

            $temp_image_url = DOC_ROOT . 'images/content/banner/' . $upload_image2;
            $new_image_url = DOC_ROOT . 'images/content/banner/' . $imagename2;

            rename($temp_image_url, $new_image_url);

            $data_arr['image2'] = $imagename2;
        }
        if (isset($_FILES) && $_FILES['image3']['name'] != "") {

            $upload_image3 = upload::upload_images(DOC_ROOT . 'images/content/banner/content/banner/', $_FILES['image3']);
            if ($upload_image3) {
                $data_arr['image3'] = $upload_image3;
            }
        }

        if (isset($_POST['btnedit'])) {

            $subcontent_id = $_POST['subcontent_id'];

            $data_arr['updated_date'] = date('Y-m-d');

            //update_sub_content
            $update = $db->query_update("tblsub_content", $data_arr, "subcontent_id=" . $subcontent_id);

            //**************** generate log entry *******************
            $logString = "update sub content, subcontent_id -  " . $subcontent_id . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
            $log = Message::log_details($_SESSION['admin']['username'], $logString);
            // **************************************************
            if ($update) {
                header("Location:sub_content.php?maincontent_id=" . $maincontent_id . "&subcontent_id=" . $subcontent_id . "&msg=" . base64_encode(6) . "");
                exit;
            } else {
                $err = "<li>Not updated</li>";
            }
        }

        if (isset($_POST['btnadd'])) {

            $data_arr['added_date'] = date('Y-m-d');
            $data_arr['updated_date'] = date('Y-m-d');

            $subcontent_id = $db->query_insert("tblsub_content", $data_arr);
            //**************** generate log entry *******************
            $logString = "Insert sub content, subcontent_id -  " . $subcontent_id . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
            $log = Message::log_details($_SESSION['admin']['username'], $logString);
            // **************************************************
            if ($subcontent_id) {
                header("Location:sub_content.php?maincontent_id=" . $maincontent_id . "&msg=" . base64_encode(7) . "");
                exit;
            } else {
                $err = "<li>Not inserted</li>";
            }
        }
    }
}



if ((isset($_GET['subcontent_id'])) && (is_numeric($_GET['subcontent_id']))) {
    $pagename = "Update Sub content Details";
} else {
    $pagename = "Add Sub content Details";
}
$page_main_heading = '<i class="fa fa-list-alt"></i>&nbsp;&nbsp;' . 'Manage Site Content';
$breaddrum = "<li><a href='main_content.php?maincontent_id=$maincontent_id'> $mainheading </a></li><li class='active'>$pagename</li>";

$INCLUDE_FILE = "includes/sub_content.tpl.php";

require_once('template_main.php');
?>

