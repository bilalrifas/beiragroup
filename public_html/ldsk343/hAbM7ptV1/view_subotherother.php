<?php

require_once('admin.php');
$per_tag = new Permission;
$per_tag->premission_tag = "Manage_subother_content";
$permission_block = $per_tag->check_permission($db);
if (!$permission_block) {
    $INCLUDE_FILE = "permission_denied.php";
    require_once('template_main.php');
    exit();
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if (isset($_GET) && isset($_GET['action'])) {

    switch ($_GET['action']) {

        case 'delete_subothercontent_image':

            if (isset($_GET['subothercontent_id']) && isset($_GET['subcontent_id']) && isset($_GET['image']) && isset($_GET['imageno'])) {

                $subothercontent_id = $_GET['subothercontent_id'];
                $subcontent_id = $_GET['subcontent_id'];
                $image = $_GET['image'];

                $data = array();
                $data[$_GET['imageno']] = "";

                $result = $db->query_update("tblsubother_content", $data, "subothercontent_id=" . $subothercontent_id);

                if (file_exists(DOC_ROOT . 'images/' . $image)) {
                    $unlink = @unlink(DOC_ROOT . 'images/' . $image);
                }

                //**************** generate log entry *******************
                $logString = "delete subother content image, subothercontent_id - " . $subothercontent_id . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************

                if ($result) {
                    header("Location:" . $_SERVER['PHP_SELF'] . "?subcontent_id=" . $subcontent_id . "&subothercontent_id=" . $subothercontent_id . "&msg=" . base64_encode(8) . "");
                    exit;
                } else {
                    header("Location:" . $_SERVER['PHP_SELF'] . "?subcontent_id=" . $subcontent_id . "&subothercontent_id=" . $subothercontent_id . "&msg=" . base64_encode(5) . "");
                    exit;
                }
            }
            break;

        case 'change_content_status':
            $status = $_GET['status'];
            $subotherothercontent_id = $_GET['subotherothercontent_id'];
            $subothercontent_id = $_GET['subothercontent_id'];


            $data = array();
            $data['status'] = $status;
            $result = $db->query_update("tblsubother_other_content", $data, "subother_other_content_id=" . $subotherothercontent_id);

            if ($result) {
                //**************** generate log entry *******************
                $logString = "Change subotherothercontent status - subotherothercontent ID= " . $subotherothercontent_id . " / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************			
                header('location:view_subotherother.php?subothercontent_id=' . $subothercontent_id . '&msg=' . base64_encode(6) . '');
                exit;
            } else {
                header('location:view_subotherother.php?subothercontent_id=' . $subothercontent_id . '&msg=' . base64_encode(5) . '');
                exit;
            }
            break;

        case 'delete_subotherothercontent':
            $subotherothercontent_id = $_GET['subotherothercontent_id'];
            $subothercontent_id = $_GET['subothercontent_id'];

            $sub_otherothercontentdata = $db->query_first("SELECT image1,image2 FROM tblsubother_other_content WHERE subother_other_content_id ='" . $subotherothercontent_id . "'");

            if (file_exists(DOC_ROOT . 'imgs/' . $sub_otherothercontentdata['image1'])) {
                $unlink = @unlink(DOC_ROOT . 'imgs/' . $sub_otherothercontentdata['image1']);
            }

            if (file_exists(DOC_ROOT . 'imgs/' . $sub_otherothercontentdata['image2'])) {
                $unlink = @unlink(DOC_ROOT . 'imgs/' . $sub_otherothercontentdata['image2']);
            }


            $result = $db->query("DELETE FROM tblsubother_other_content WHERE subother_other_content_id =" . $subotherothercontent_id . "");

            if ($result) {
                //**************** generate log entry *******************
                $logString = "Delete subothercontent - subother_other_content ID= " . $subotherothercontent_id . " / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************			
                header('location:view_subotherother.php?subothercontent_id=' . $subothercontent_id . '&msg=' . base64_encode(8) . '');
                exit;
            } else {
                header('location:view_subotherother.php?subothercontent_id=' . $subothercontent_id . '&msg=' . base64_encode(5) . '');
                exit;
            }
            break;
    }
}


if ((isset($_GET['subothercontent_id'])) && (is_numeric($_GET['subothercontent_id']))) {
    $subothercontent_id = $_GET['subothercontent_id'];
    $sub_otherothercontents = $db->fetch_all_array("SELECT * FROM tblsubother_other_content WHERE subothercontent_id=$subothercontent_id ORDER BY display_order ASC");
} else {
    header('location:dashboard.php');
    exit;
}

//$breaddrum = " <a href='admin_home.php' class='breaddrum'>Home </a> >>  Content Management  >> <a href='main_content.php?maincontent_id=$maincontent_id'  class='breaddrum'> $mainheading </a> >> <a href='main_content.php?maincontent_id=$maincontent_id'  class='breaddrum'> $subheading </a> >> $type";
$page_main_heading = '<i class="fa fa-list-alt"></i>&nbsp;&nbsp;'.'Manage Site Content';
$breaddrum = "<li class='active'>Sub other other pages</li>";

$INCLUDE_FILE = "includes/view_subotherother.tpl.php";

require_once('template_main.php');
?>
