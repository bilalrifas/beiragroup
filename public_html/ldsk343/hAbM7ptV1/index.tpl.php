<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title><?php
            if (isset($companyname)):echo $companyname . " Administration Panel";
            else: echo 'Administration Panel';
            endif;
            ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="bg-black">

        <div class="form-box" id="login-box">
            <div class="container-fluid">
                <div class="row">
                    <div class="margin text-left" style="margin-right: 20px;">
                        <div class="col-lg-12">
                            <?php
                            if (isset($err) && $err != '') {
                                ?>
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <i class="fa fa-ban"></i>
                                <?php echo $err; ?>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header">Sign In</div>
            <form action="" name="logintosystem" method="POST">

                <div class="body bg-gray">
                    <div class="form-group">
                        <input type="text" name="txtusername" class="form-control" placeholder="User Name"/>
                    </div>
                    <div class="form-group">
                        <input type="password" name="txtpassword" class="form-control" placeholder="Password"/>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" value="remember-me" name="reme"/> Remember me
                    </div>
                </div>
                <div class="footer">
                    <button type="submit" name="log_434in" class="btn bg-olive btn-block">Sign me in</button>
                    <div class="margin text-center"> Copyright <?php echo date("Y"); ?>  <?php echo $companyname; ?>. Site by <a href="http://www.3cs.lk" style="color:#06C;" target="_blank">3CS</a></div>
                </div>
            </form>
        </div>
        <!-- jQuery 2.0.2 -->
        <script src="js/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>

    </body>
</html>