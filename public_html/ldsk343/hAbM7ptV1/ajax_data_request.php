<?php

require_once('admin.php');

/*
  $row = array();
  $row['adverts'] = 'test';
  $advert = array(
  'ajax' => 'Hello world!',
  'eid'=>$_POST['eid'],
  'advert' => $row['adverts'],
  );
  echo json_encode($advert);
 */
if (isset($_POST['actionrequest']) && !empty($_POST['actionrequest'])) {
    if (isset($_POST) && isset($_POST['eid']) && is_numeric($_POST['eid']) && $_POST['actionrequest'] == 'adduser') {
        $id = clean_text($_POST['eid']);
        $data = getData::getUserdata_byID($id);
        echo json_encode($data);
        exit();
    }
    if ($_POST['actionrequest'] == 'permi_cat') {
        $allcat = getData::getAllpermission_categories();
        echo '<option value="" >Select Category</option>';
        foreach ($allcat as $category) {
            echo "<option value = " . $category['id'] . ">". $category['catagory']."</option>";
        }
        exit();
    }

    if ($_POST['actionrequest'] == 'addpermislevel') {
        $id = clean_text($_POST['eid']);
        $data = getData::getpermission_leveldata($id);
        echo json_encode($data);
        exit();
    }

    if($_POST['actionrequest'] == 'addpermiscat'){
        $id = clean_text($_POST['eid']);
        $data = $db->query_first("SELECT * FROM tblpermission_catagory WHERE id = $id");
        echo json_encode($data);
        exit();
    }

    if($_POST['actionrequest'] == 'get_sub_categories'){
        $allcat = getData::getSubCategoriesByMainCatId($_POST['cat_id']);
        echo '<option value="" >Select a Sub Category</option>';
        foreach ($allcat as $category) {
            $retVal = ($category['id']==$_POST['selected']) ? "selected" : "" ;
            echo "<option value = " . $category['id'] . "  $retVal >". $category['heading']."</option>";
        }
        exit();
    }

    if($_POST['actionrequest'] == 'get_product_list'){
      if (isset($_POST['subcat_id']) && $_POST['subcat_id']!="") {
        $allproducts = getData::getProductsBySubCatId($_POST['subcat_id']);
      } else {
        $allproducts = array();
      }
        ?>

        <tr>
          <td colspan="3" align="center" >
              <?php if (sizeof($allproducts) < 1) { ?>
                  <div class="no_data"><?php echo "<i class=\"fa fa-exclamation-triangle\"></i>
No Products To Show"; ?></div>
              <?php } else { ?>
                  <table width="100%" border="0" cellspacing="0" cellpadding="2" class="table table-hover">

                      <tr class="table_heading">
                      <thead>
                          <tr class="table_heading">

                              <th height="30" width="26%" align="left" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" ><strong>Product ID</strong></th>
                              <th width="9%" align="left" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>"><strong>Status</strong></th>
                              <th width="16%" align="left" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>"><strong>Update Date</strong></th>
                              <th align="left" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>">&nbsp;</th>
                              <th width="15%" align="left" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>"><strong>Display Order</strong></th>
                          </tr>
                      </thead>
                      <tbody>
                          <?php
                          $i = 0;
                          foreach ($allproducts as $row) {
                              ?>

                              <tr <?php
                              if (($i % 2) == 0) {
                                  echo "bgcolor='#FAFAFA'";
                              } else {
                                  echo 'bgcolor="#FFFFFF"';
                              }
                              ?>>
                                  <td align="left"  class="small_f1"><span style="width:100px; margin-right: 10px; text-align: left; color:blueviolet;"></span><?php echo $row['product_id']; ?></td>
                                  <td align="left"  class="small_f1">
                                      <?php
                                      if ($row['status'] == 1) {
                                          echo "active";
                                      } else {
                                          echo "inactive";
                                      }
                                      ?>
                                  </td>
                                  <td align="left"  class="small_f1"><?php echo $row['update_date']; ?></td>


                                  <td align="left" valign="top">
                                          <div class="btn-group">
                                              <button type="button" class="btn btn-primary btn-flat">Action</button>
                                              <button type="button" class="btn btn-primary btn-flat dropdown-toggle" data-toggle="dropdown">
                                                  <span class="caret"></span>
                                                  <span class="sr-only">Toggle Dropdown</span>
                                              </button>
                                              <ul class="dropdown-menu" role="menu">
                                                  <?php if ($row['status'] != 1) { ?>
                                                      <li><a href=<?php echo 'view_products.php?action=status_change&active=1&id=' . $row['id'] . '&cat_id='.$row['category_id'].'&subcat_id='.$row['subcategory_id']; ?> onclick="return confirm('Are you sure to active this Menu Item?');">Active</a></li>
                                                  <?php } else { ?>
                                                       <li><a href=<?php echo 'view_products.php?action=status_change&active=0&id=' . $row['id'] . '&cat_id='.$row['category_id'].'&subcat_id='.$row['subcategory_id']; ?> onclick="return confirm('Are you sure to inactive this Menu Item?');">Inactive</a></li>
                                                  <?php } ?>
                                              </ul>
                                          </div>
                                      &nbsp;
                                      <a class="btn btn-danger btn-flat" href=<?php echo 'view_products.php?action=delete_product&id=' . $row['id'] . '&cat_id='.$row['category_id'].'&subcat_id='.$row['subcategory_id']; ?> onClick="return confirm('Are you sure to Delete this Menu Item?');"><i class="fa fa-eraser"></i>&nbsp;&nbsp;Delete</a>&nbsp;
                                      <a data-toggle="modal" href="add_products.php?id=<?php echo $row["id"]; ?>" class="btn btn-success"><i class="fa fa-pencil-square-o"></i>&nbsp;&nbsp;&nbsp;Edit</a>
                                  </td>
                                  <td align="left" valign="top">
                                      <select name="display_order[<?php echo $row['id']; ?>]"  class="form-control" onchange="setpro_order(this.value,<?php echo $row['id']; ?>)"  style="width: 60px" >
                                          <?php for ($j = 1; $j <= sizeof($allproducts); $j++) { ?>
                                              <option value="<?php echo $j; ?>" <?php
                                                    if ($row['display_order'] == $j) {
                                                        echo "selected";
                                                    }
                                                    ?>><?php echo $j; ?></option>
                                                    <?php } ?>
                                      </select>
                                      <span id="status_pro_<?php echo $row['id']; ?>" class="label label-success flat"></span>

                                  </td>
                              </tr>

                              <?php
                              $i++;
                          }
                          ?>
                          <tr>
                              <td height="39" colspan="7" align="right" valign="middle"></td>
                          </tr>
                      <?php } ?>
                  </tbody>


                  <tr>
                      <td bgcolor="#FFFFFF">&nbsp;</td>
                      <td colspan="5" bgcolor="#FFFFFF">&nbsp;</td>

                      <td width="2%" bgcolor="#FFFFFF">&nbsp;</td>
                  </tr>
              </table></td>
      </tr>
        <?php
    }
}
?>