<?php

require_once('admin.php');
//-----------permissions-------

$per_tag = new Permission;
$per_tag->premission_tag = "Modify Product";
$permission_block = $per_tag->check_permission($db);
if (!$permission_block) {
    $INCLUDE_FILE = "permission_denied.php";
    require_once('template_main.php');
    exit();
}

$err = "";
$title = "";



if ($_POST == true) {
    $err = "";
    $title = trim($_POST['title']);

    if ($title == "") {
        $err = $err . "Please enter title";
    }

    if ($err == "") {
        $data_arr = array();
        $data_arr['title'] = $title;


        if (isset($_POST['btnadd']) == true) {

            $insert_id = $db->query_insert("tblproduct_category", $data_arr);
            if ($insert_id) {
                //**************** generate log entry *******************
                $logString = "Add Product Category - " . $title . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************
                header("Location:".$_SERVER['PHP_SELF']."?msg=" . base64_encode(7) . "");
                exit;
            } else {
                $err = $err . "Not inserted";
            }
        }
    }
}

if (isset($_GET['action'])) {

    switch ($_GET['action']) {

        case 'status_change':

            $active = $_GET['active'];
            $id = $_GET['id'];

            $data = array();
            $data['status'] = $active;
            $result = $db->query_update("tblproduct_category", $data, "id=" . $id);

            if ($result) {
                //**************** generate log entry *******************
                $logString = "Change Prduct Category status - Category ID = " . $id . " / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************
                header('location:' . $_SERVER['PHP_SELF'] . '?msg=' . base64_encode(6) . '');
                exit;
            } else {
                header('location:' . $_SERVER['PHP_SELF'] . '?msg=' . base64_encode(5) . '');
                exit;
            }

            break;

        case 'delete_category':

            $id = $_GET['id'];

            $result = $db->query("DELETE FROM tblproduct_category WHERE id =" . $id . "");

            //**************** generate log entry *******************
            $logString = "Deleted Category - Category ID = " . $id . " / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
            $log = Message::log_details($_SESSION['admin']['username'], $logString);
            // **************************************************

            header('location:' . $_SERVER['PHP_SELF'] . '?msg=' . base64_encode(8) . '');
            exit;
            break;
    }
}

$content_link = $db->fetch_all_array("SELECT * FROM tblproduct_category ORDER BY display_order  ASC");

$page_main_heading = '<i class="fa fa-archive"></i>&nbsp;&nbsp;'.'Manage Product Category';

$breaddrum = "<li class='active'>Add Product Category</li>";
$INCLUDE_FILE = "includes/add_productCategory.tpl.php";
require_once('template_main.php');
?>