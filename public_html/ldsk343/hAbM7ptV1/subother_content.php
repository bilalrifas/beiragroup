<?php

require_once('admin.php');
$per_tag = new Permission;
$per_tag->premission_tag = "manage_subother_content";
$permission_block = $per_tag->check_permission($db);
if (!$permission_block) {
    $INCLUDE_FILE = "permission_denied.php";
    require_once('template_main.php');
    exit();
}
//------------------

$err = "";


$subcontent_id = "";
$details = "";
$heading = "";
$spe_heading = "";
$spe_details = "";
//meta data
$meta_title = "";
$meta_desc = "";
$meta_key = "";
$image1 = "";
$image2 = "";
$image3 = "";
$specifications = "";
$benefits = "";

if (isset($_GET) && isset($_GET['action'])) {

    switch ($_GET['action']) {

        case 'delete_content_image':

            if (isset($_GET['subother_id']) && isset($_GET['image']) && isset($_GET['imageno'])) {
                $subother_id = $_GET['subother_id'];
                $sub_id = $_GET['sub_id'];
                $image = $_GET['image'];

                $data[$_GET['imageno']] = "";

                $result = $db->query_update("tblsubother_content", $data, "subothercontent_id=" . $subother_id);

                if (file_exists(DOC_ROOT . 'images/' . $image)) {
                    $unlink = @unlink(DOC_ROOT . 'images/' . $image);
                }

                //**************** generate log entry *******************
                $logString = "delete content image, sub_id - " . $sub_id . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************

                if ($result) {
                    header('location:subother_content.php?subcontent_id=' . $sub_id . '&subothercontent_id=' . $subother_id . '&msg=' . base64_encode(8));
                    exit;
                } else {
                    header('location:subother_content.php?subcontent_id=' . $sub_id . '&subothercontent_id=' . $subother_id . '&msg=' . base64_encode(5));
                    exit;
                }
            }
            break;
    }
}

if (isset($_GET) && ($_GET['subcontent_id'] != "") && (is_numeric($_GET['subcontent_id']))) {

    $subcontent_id = $_GET['subcontent_id'];
    //get_subcontent_by_id
    $sub_content = $db->query_first("SELECT * FROM tblsub_content WHERE subcontent_id ='" . $subcontent_id . "'");

    if ($sub_content) {
        $subcontent_id = $sub_content['subcontent_id'];
        $subheading = $sub_content['heading'];

        //get_maincontent_by_id
        $main_content = $db->query_first("SELECT * FROM tblmain_content WHERE maincontent_id ='" . $sub_content['maincontent_id'] . "'");

        $maincontent_id = $main_content['maincontent_id'];
        $mainheading = $main_content['heading'];



        if ((isset($_GET['subothercontent_id'])) && (is_numeric($_GET['subothercontent_id']))) {

            //get_subothercontent_by_id
            $subother_content = $db->query_first("SELECT * FROM tblsubother_content WHERE subothercontent_id ='" . $_GET['subothercontent_id'] . "'");

            if ($subother_content) {
                $subothercontent_id = $subother_content['subothercontent_id'];
                $details = $subother_content['details'];
                $heading = $subother_content['heading'];
                $meta_title = $subother_content['meta_title'];
                $meta_desc = $subother_content['meta_desc'];
                $spe_details = $subother_content['spe_details'];
                $spe_heading = $subother_content['spe_heading'];
                $meta_key = $subother_content['meta_key'];
                $image1 = $subother_content['image1'];
                $image2 = $subother_content['image2'];
                $image3 = $subother_content['image3'];
                $specifications = $subother_content['specifications'];
                $benefits = $subother_content['benefits'];
            } else {
                header('location:main_content.php?maincontent_id=$maincontent_id');
                exit;
            }
        }
    } else {
        header('location:dashboard.php');
        exit;
    }
} else {
    header('location:dashboard.php');
    exit;
}

if ($_POST == true) {

    $err = "";

    $subcontent_id = $_POST['subcontent_id'];
    $maincontent_id = $_POST['maincontent_id'];
    $heading = trim($_POST['heading']);
    $details = $_POST['details'];
    $spe_heading = trim($_POST['spe_heading']);
    $spe_details = $_POST['spe_details'];
    $meta_title = trim($_POST['meta_title']);
    $meta_desc = trim($_POST['meta_desc']);
    $meta_key = trim($_POST['meta_key']);
    $specifications = trim($_POST['specifications']);
    $benefits = trim($_POST['benefits']);

    if ($heading == "") {
        $err = $err . "<li>Please enter title</li>";
    }

    if ($err == "") {

        $data_arr = array();
        $data_arr['maincontent_id'] = $maincontent_id;
        $data_arr['subcontent_id'] = $subcontent_id;
        $data_arr['heading'] = $heading;
        $data_arr['details'] = $details;
        $data_arr['spe_heading'] = $spe_heading;
        $data_arr['spe_details'] = $spe_details;
        $data_arr['meta_title'] = $meta_title;
        $data_arr['meta_desc'] = $meta_desc;
        $data_arr['meta_key'] = $meta_key;
        $data_arr['specifications'] = $specifications;
        $data_arr['benefits'] = $benefits;


        $gallery = TRUE;

        if (isset($_FILES['filex']) && sizeof($_FILES['filex']['tmp_name']) == 1 && $_FILES['filex']['tmp_name'][0] == '') {
            $gallery = FALSE;
        }


        if (isset($_FILES['filex']) && $gallery) {
            $errors = array();
            $images_names = array();

            foreach ($_FILES['filex']['tmp_name'] as $key => $tmp_name) {
                $file_name = $key . $_FILES['filex']['name'][$key];
                $file_size = $_FILES['filex']['size'][$key];
                $file_tmp = $_FILES['filex']['tmp_name'][$key];
                $file_type = $_FILES['filex']['type'][$key];
                if ($file_size > 2097152) {
                    $errors[] = 'File size must be less than 2 MB';
                }
                // $query="INSERT into upload_data (`USER_ID`,`FILE_NAME`,`FILE_SIZE`,`FILE_TYPE`) VALUES('$user_id','$file_name','$file_size','$file_type'); ";
                $desired_dir = DOC_ROOT . 'images/gallery';

                if (empty($errors) == true) {
                    if (is_dir($desired_dir) == false) {
                        mkdir("$desired_dir", 0700);  // Create directory if it does not exist
                    }
                    if (is_dir("$desired_dir/" . $file_name) == false) {
                        move_uploaded_file($file_tmp, "$desired_dir/" . $file_name);

                        $imagename2 = str_replace(" ", "", $file_name);

                        $randnum = rand(1000, 9999);
                        $imagename2 = $randnum . "-" . $imagename2;

                        $temp_image_url = DOC_ROOT . 'images/gallery/' . $file_name;
                        $new_image_url = DOC_ROOT . 'images/gallery/' . $imagename2;

                        rename($temp_image_url, $new_image_url);
                        $images_names[] = $imagename2;
                    } else {         // rename the file if another one exist
                        $new_dir = "$desired_dir/" . $file_name . time();
                        rename($file_tmp, $new_dir);
                    }
                    // mysql_query($query);			
                } else {
                    print_r($errors);
                }
            }
            if (empty($error)) {
                echo "Success";
            }
        }



        if (isset($_FILES) && $_FILES['image1']['name'] != "") {

            $upload_image1 = upload::upload_images(DOC_ROOT . 'images/', $_FILES['image1']);

            $imagename1 = str_replace(" ", "", $upload_image1);

            $temp_image_url = DOC_ROOT . 'images/' . $upload_image1;
            $new_image_url = DOC_ROOT . 'images/' . $imagename1;

            rename($temp_image_url, $new_image_url);

            $data_arr['image1'] = $imagename1;
        }

        if (isset($_FILES) && $_FILES['image2']['name'] != "") {

            $upload_image2 = upload::upload_images(DOC_ROOT . 'images/', $_FILES['image2']);

            $imagename2 = str_replace(" ", "", $upload_image2);

            $temp_image_url = DOC_ROOT . 'images/' . $upload_image2;
            $new_image_url = DOC_ROOT . 'images/' . $imagename2;

            rename($temp_image_url, $new_image_url);

            $data_arr['image2'] = $imagename2;
        }

        if (isset($_FILES) && $_FILES['image3']['name'] != "") {

            $upload_image3 = upload::upload_images(DOC_ROOT . 'images/', $_FILES['image3']);

            $imagename3 = str_replace(" ", "", $upload_image3);

            $temp_image_url = DOC_ROOT . 'images/' . $upload_image3;
            $new_image_url = DOC_ROOT . 'images/' . $imagename3;

            rename($temp_image_url, $new_image_url);

            $data_arr['image3'] = $imagename3;
        }

        if (isset($_POST['btnedit'])) {

            $subothercontent_id = $_POST['subothercontent_id'];

            $data_arr['updated_date'] = date('Y-m-d');

            $update = $db->query_update("tblsubother_content", $data_arr, "subothercontent_id=" . $subothercontent_id);
            //**************** generate log entry *******************
            $logString = "update sub other content, subothercontent_id -  " . $subothercontent_id . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
            $log = Message::log_details($_SESSION['admin']['username'], $logString);
            // **************************************************
            if ($update) {
                if (isset($_FILES['filex']) && $gallery) {
                    if (isset($images_names) && sizeof($images_names) > 0) {
                        $img_data = array();
                        foreach ($images_names as $images) {
                            $img_data['name'] = $images;
                            $img_data['status'] = 1;
                            $img_data['display_order'] = 1;
                            $img_data['cat_id'] = $subothercontent_id;
                            $imagez = $db->query_insert("tblsolutiongallery", $img_data);
                        }
                    }
                }
                header("Location:subother_content.php?subcontent_id=" . $subcontent_id . "&subothercontent_id=" . $subothercontent_id . "&msg=" . base64_encode(6) . "");
            } else {
                $err = "<li>Not updated</li>";
            }
        } else if (isset($_POST['btnadd'])) {

            $data_arr['added_date'] = date('Y-m-d');
            $data_arr['updated_date'] = date('Y-m-d');

            $subothercontent_id = $db->query_insert("tblsubother_content", $data_arr);
            //**************** generate log entry *******************
            $logString = "Insert sub other content, subothercontent_id -  " . $subothercontent_id . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
            $log = Message::log_details($_SESSION['admin']['username'], $logString);
            // **************************************************
            if ($subothercontent_id) {
                if (isset($images_names) && sizeof($images_names) > 0) {
                    $img_data = array();
                    foreach ($images_names as $images) {
                        $img_data['name'] = $images;
                        $img_data['status'] = 1;
                        $img_data['display_order'] = 1;
                        $img_data['cat_id'] = $subothercontent_id;
                        $imagez = $db->query_insert("tblsolutiongallery", $img_data);
                    }
                }

                header("Location:subother_content.php?subcontent_id=" . $subcontent_id . "&msg=" . base64_encode(7) . "");
            } else {
                $err = "<li>Not inserted</li>";
            }
        }
    }
}




if ((isset($_GET['subothercontent_id'])) && (is_numeric($_GET['subothercontent_id']))) {
    $pagename = "Update Sub Other content Details";
} else {
    $pagename = "Add Sub Other content Details";
}

//$breaddrum = " <a href='admin_home.php' class='breaddrum'>Home </a> >>  Content Management  >> <a href='main_content.php?maincontent_id=$maincontent_id'  class='breaddrum'> $mainheading </a> >> <a href='main_content.php?maincontent_id=$maincontent_id'  class='breaddrum'> $subheading </a> >> $type";
$page_main_heading = '<i class="fa fa-list-alt"></i>&nbsp;&nbsp;' . 'Manage Site Content';
$breaddrum = "<li><a href='main_content.php?maincontent_id=$maincontent_id'> $mainheading </a></li><li><a href='sub_content.php?maincontent_id=$maincontent_id&subcontent_id=$subcontent_id'> $subheading </a></li><li class='active'>$pagename</li>";

$INCLUDE_FILE = "includes/subother_content.tpl.php";

require_once('template_main.php');
?>

