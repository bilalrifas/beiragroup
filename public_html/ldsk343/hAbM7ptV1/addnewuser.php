<style>
    .error{
        display: none;
    }

</style>
<?php
require_once('admin.php');
$name = "";
$uname = "";
$description = "";
$level = "";

//name 	description 	email 	username 	password 	added_date 	active 	level 
if (isset($_GET) && isset($_GET['eid']) && is_numeric($_GET['eid'])) {
    $temp_heading = "Edit User Details";
    $id = clean_text($_GET['eid']);
    $data = getData::getUserdata_byID($id);
    $name = $data['name'];
    $uname = $data['username'];
    $description = $data['description'];
    $level = $data['level'];
} else {
    $temp_heading = "Add User";
}
?>
<pre><b><?php echo $temp_heading; ?></b></pre>

<div id="statusshow" style="font-family:sans-serif; display:none; font-size:12px; padding:5px; text-align:center; font-weight:bold;"><img src="images/loader.gif" />&nbsp;Please wait</div>
<div id="sucess" style="padding:5px; display:none; width:300px;" class="label label-success msge">Successfully Inserted</div>
<div id="saverror" style="padding:5px; display:none; width:780px; margin-bottom: 20px; text-align: center;" class="label label-important msge"></div>


<div class="container" style="width: 750px; height: 350px;">
    <form name="frm_post_comments" enctype="multipart/form-data" action="" method="post" role="form">
    <div class="row">
        <div class="col-md-3"><label>sdfsdfds</label></div>
        <div class="col-md-4">
            
            <input type="text" class="form-control" name="name" id="name" value="<?php echo $name; ?>" />&nbsp;&nbsp;<span id="nameerror" class="label label-important error">Name Cannot Be Blank</span>
        </div>
    </div><br/>
    <div class="row">
        <div class="col-md-3"><label>sdfsdfds</label></div>
        <div class="col-md-4">
            <input type="text" class="form-control" placeholder=""> 
        </div>
    </div><br/>
    <div class="row">
        <div class="col-md-3"><label>sdfsdfds</label></div>
        <div class="col-md-8">
            <input type="text" class="form-control" placeholder=""> 
        </div>
    </div><br/>
    <div class="row">
        <div class="col-md-3"><label>sdfsdfds</label></div>
        <div class="col-md-4">
            <select class="form-control">
                <option>option 1</option>
                <option>option 2</option>
                <option>option 3</option>
                <option>option 4</option>
                <option>option 5</option>
            </select>
        </div>
    </div><br/>
    <div class="row">
        <div class="col-md-3"><label>sdfsdfds</label></div>
        <div class="col-md-4">
            <input type="text" class="form-control" placeholder=""> 
        </div>
    </div><br/>
    <div class="row">
        <div class="col-md-3"><label>sdfsdfds</label></div>
        <div class="col-md-4">
            <input type="text" class="form-control" placeholder=""> 
        </div>
    </div><br/>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-4">
            <button type="button" class="btn btn-primary btn-flat btn-block">Add User</button>
        </div>
    </div>
    </form>
</div>



<!--
<div class="container" style="width: 750px; height: 320px;">
    <div class="row">
        <div class="col-md-3"><label>sdfsdfds</label></div>
        <div class="col-md-4">
            <input type="text" class="form-control" placeholder=""> 
        </div>
    </div><br/>
    <div class="row">
        <div class="col-md-3"><label>sdfsdfds</label></div>
        <div class="col-md-4">
            <input type="text" class="form-control" placeholder=""> 
        </div>
    </div><br/>
    <div class="row">
        <div class="col-md-3"><label>sdfsdfds</label></div>
        <div class="col-md-8">
            <input type="text" class="form-control" placeholder=""> 
        </div>
    </div><br/>
    <div class="row">
        <div class="col-md-3"><label>sdfsdfds</label></div>
        <div class="col-md-4">
            <select class="form-control">
                <option>option 1</option>
                <option>option 2</option>
                <option>option 3</option>
                <option>option 4</option>
                <option>option 5</option>
            </select>
        </div>
    </div><br/>
    <div class="row">
        <div class="col-md-3"><label>sdfsdfds</label></div>
        <div class="col-md-4">
            <input type="text" class="form-control" placeholder=""> 
        </div>
    </div><br/>
    <div class="row">
        <div class="col-md-3"><label>sdfsdfds</label></div>
        <div class="col-md-4">
            <input type="text" class="form-control" placeholder=""> 
        </div>
    </div><br/>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-4">
            <button type="button" class="btn btn-primary btn-flat btn-block">Add User</button>
        </div>
    </div><br/>
</div>
-->


<script>
    $(document).ready(function() {

        $("#btnadd").click(function() {
            if (validate()) {
                datainsert(0);
            } else {
                $("#saverror").html("Please Fill the data");
                $("#saverror").show("slow");
            }
        });

        $("#btnedit").click(function() {
            if (validate()) {
                datainsert(1);
            } else {
                $("#saverror").html("Please Fill the data");
                $("#saverror").show("slow");
            }
        });


    });


    function validate() {
        $(".error").hide('slow');
        $(".msge").hide("slow");
        var name = $("#name").val();
        var uname = $("#uname").val();
        var status = $("#status").val();
        var pw = $("#pw").val();
        var repw = $("#repw").val();

        var submit = "TRUE";

        if (name == '') {
            $("#nameerror").show("slow");
            submit = "FALSE";
        }

        if (uname == "") {
            $("#unameerror").show("slow");
            submit = "FALSE";
        }

        if (status == 0) {
            $("#levelerror").show("slow");
            submit = "FALSE";
        }


        if (pw == '') {
            $("#pwerror").show("slow");
            submit = "FALSE";
        }


        if ((repw == '') || (pw != repw)) {
            $("#repwerror").show("slow");
            submit = "FALSE";
        }

        if (submit == 'TRUE') {
            return true;
        } else {
            return false;
        }

    }


    function datainsert(type) {
        $("#statusshow").show('slow');
        var eid = 0;
        var uname = "";
        var pw = "";

        var name = $("#name").val();

        var desc = $("#desc").val();
        var status = $("#status").val();

        if (type == 1) {
            eid = $("#eid").val();
        } else {
            uname = $("#uname").val();
            pw = $("#pw").val();
        }
        $.ajax({
            type: "POST",
            url: "ajax_data.php",
            data: "action=adduser&name=" + name + "&uname=" + uname + "&desc=" + desc + "&cat=" + status + "&type=" + type + "&eid=" + eid + "&pw=" + pw,
            success: function(val) {
                $("#statusshow").hide('slow');
                if (val == 1) {
                    $("#sucess").show('slow');
                } else if (val == 2) {
                    $("#saverror").html('Duplicate Entry..! Please Recheck Name');
                    $("#saverror").show('slow');
                } else {
                    $("#saverror").html('Error in Insert');
                    $("#saverror").show('slow');
                }

                setTimeout("jQuery('#facebox_overlay').click();", 1800);
            }
        });
    }

</script>