<?php

require_once('admin.php');
//-----------permissions-------

$per_tag = new Permission;
$per_tag->premission_tag = "Modify Product";
$permission_block = $per_tag->check_permission($db);
if (!$permission_block) {
    $INCLUDE_FILE = "permission_denied.php";
    require_once('template_main.php');
    exit();
}

$content_link=array();

if (isset($_GET['action'])) {

    switch ($_GET['action']) {

        case 'status_change':

            $active = $_GET['active'];
            $id = $_GET['id'];
            $category_id=$_GET['cat_id'];
            $subcategory_id=$_GET['subcat_id'];


            $data = array();
            $data['status'] = $active;
            $result = $db->query_update("tblproducts", $data, "id=" . $id);

            if ($result) {
                //**************** generate log entry *******************
                $logString = "Change product status - product ID = " . $id . " / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************
                header('location:' . $_SERVER['PHP_SELF'] . '?cat_id='.$category_id.'&subcat_id='.$subcategory_id.'&msg=' . base64_encode(6) . '');
                exit;
            } else {
                header('location:' . $_SERVER['PHP_SELF'] . '?cat_id='.$category_id.'&subcat_id='.$subcategory_id.'&msg=' . base64_encode(5) . '');
                exit;
            }

            break;

        case 'delete_product':

            $id = $_GET['id'];
            $category_id=$_GET['cat_id'];
            $subcategory_id=$_GET['subcat_id'];

            $result = $db->query("DELETE FROM tblproducts WHERE id =" . $id . "");

            if ($result) {
            //**************** generate log entry *******************
            $logString = "Deleted product -  ID = " . $id . " / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
            $log = Message::log_details($_SESSION['admin']['username'], $logString);
            // **************************************************

            header('location:' . $_SERVER['PHP_SELF'] . '?cat_id='.$category_id.'&subcat_id='.$subcategory_id.'&msg=' . base64_encode(8) . '');
            exit;
            } else{
                header('location:' . $_SERVER['PHP_SELF'] . '?cat_id='.$category_id.'&subcat_id='.$subcategory_id.'&msg=' . base64_encode(5) . '');
                exit;
            }

            break;
    }
}


/*$content_link = $db->fetch_all_array("SELECT * FROM tblproducts ORDER BY display_order  ASC");
$params = array(
    'mode' => 'Sliding',
    'perPage' => 20,
    'delta' => 1,
    'itemData' => $content_link
);
$pager = & Pager::factory($params);
$content_link = $pager->getPageData();*/

$category_array = $db->fetch_all_array("SELECT * FROM tblproduct_category ORDER BY display_order  ASC");
$subcategory_array = $db->fetch_all_array("SELECT * FROM tblproduct_subcategory ORDER BY display_order  ASC");
$category_id="";
$subcategory_id="";

if (isset($_GET['cat_id']) && is_numeric($_GET['cat_id'])) {
    $category_id=$_GET['cat_id'];
}

if (isset($_GET['subcat_id']) && is_numeric($_GET['subcat_id'])) {
    $subcategory_id=$_GET['subcat_id'];
}

$page_main_heading = '<i class="fa fa-search-plus"></i>  View All Products';

$breaddrum = "<li class='active'>View All Products</li>";
$INCLUDE_FILE = "includes/view_products.tpl.php";
require_once('template_main.php');
?>