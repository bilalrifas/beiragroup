<?php

require_once('admin.php');

$per_tag = new Permission;
$per_tag->premission_tag = "permissionlevels";
$permission_block = $per_tag->check_permission($db);
if (!$permission_block) {
    $INCLUDE_FILE = "permission_denied.php";
    require_once('template_main.php');
    exit();
}

$all_data = $db->fetch_all_array("SELECT * FROM tblgroups ORDER BY group_name");
$params = array(
    'mode' => 'Sliding',
    'perPage' => 10,
    'delta' => 1,
    'itemData' => $all_data
);
$pager = & Pager::factory($params);
$datas = $pager->getPageData();

if (isset($_GET) && isset($_GET['action'])) {

    switch ($_GET['action']) {

        case 'delete':
            $id = $_GET['did'];
            $sql = "DELETE FROM tblgroups WHERE id =" . $id;
            $result = $db->query($sql);
            $db->query("DELETE FROM tblgroup_permissions WHERE group_id =  $id");
            if ($result) {
                //**************** generate log entry *******************
                $logString = "Deleted Group,Group id- " . $id . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************
            }
            header('location:' . $_SERVER['PHP_SELF'] . '?msg=' . base64_encode(8) . '');
            exit;
            break;
    }
}

$page_main_heading = '<i class="fa fa-wrench"></i>&nbsp;&nbsp;'.'Administrative';
$breaddrum = "<li class='active'>User Roles</li>";
$INCLUDE_FILE = "includes/user_rolls.tpl.php";

require_once('template_main.php');
?>
