<?php

require_once('admin.php');

$per_tag = new Permission;
$per_tag->premission_tag = "modify_permissionlevels";
$permission_block = $per_tag->check_permission($db);
if (!$permission_block) {
    $INCLUDE_FILE = "permission_denied.php";
    require_once('template_main.php');
    exit();
}

$all_data = $db->fetch_all_array("SELECT * FROM tblpermission_level ORDER BY page_name");
$params = array(
    'mode' => 'Sliding',
    'perPage' => 20,
    'delta' => 1,
    'itemData' => $all_data
);
$pager = & Pager::factory($params);
$datas = $pager->getPageData();

if (isset($_GET) && isset($_GET['action'])) {

    switch ($_GET['action']) {

        case 'delete':
            $id = $_GET['did'];

              $sql = "DELETE FROM tblpermission_level WHERE id =" . $id;
              $result = $db->query($sql);

            //delete permisson from tblgroup_permissions as well

            $group_permissions = $db->fetch_all_array("SELECT * FROM tblgroup_permissions");

            foreach ($group_permissions as $group_permission => $permissions) {
                $permissions1 = explode(',', $permissions['permissions']);
                if (in_array($id, $permissions1)) {
                    $permissionid = $permissions['id'];
                    $key = '';
                    if ($key = array_search($id, $permissions1)) {

                        unset($permissions1[$key]);
                        $newpermissions = implode(',', $permissions1);
                        $data = array(
                            'permissions'=>$newpermissions,
                        );
                        $db->query_update('tblgroup_permissions',$data,"id = $permissionid");
                    } else {

                        if ($key == 0) {
                            unset($permissions1[$key]);
                            $newpermissions = implode(',', $permissions1);
                            $data = array(
                                'permissions'=>$newpermissions,
                            );
                            $db->query_update('tblgroup_permissions',$data,"id = $permissionid");
                        }
                    }
                }
            }
            if ($result) {
                //**************** generate log entry *******************
                $logString = "Deleted Permission Level,Level id- " . $id . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************
            }
            header('location:' . $_SERVER['PHP_SELF'] . '?msg=' . base64_encode(8) . '');
            exit;
            break;
    }
}


$breaddrum = "<li class='active'>Permission Levels</li>";
$page_main_heading = '<i class="fa fa-wrench"></i>&nbsp;&nbsp;'.'Administrative';
$INCLUDE_FILE = "includes/permissionlevels.tpl.php";

require_once('template_main.php');
?>