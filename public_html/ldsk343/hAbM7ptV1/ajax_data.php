<?php

require_once('admin.php');
if (isset($_POST) && isset($_POST['actionrequest']) && $_POST['actionrequest'] != '') {
    if ($_POST['actionrequest'] == 'editpromocat') {

        //name 	desc 	terms_conditions 	cancellation_policy 	note 	added_date 	status

        $datamod = array();
        $datamod['name'] = $_POST['name'];
        $datamod['desc'] = $_POST['desc'];
        $datamod['terms_conditions'] = $_POST['terms'];
        $datamod['cancellation_policy'] = $_POST['privacy'];
        $datamod['note'] = $_POST['note'];
        $datamod['status'] = $_POST['status'];
        $result = $db->query_update("tblpromotion_categories", $datamod, "id=" . $_POST['id']);
        if ($result) {
            //**************** generate log entry *******************
            $logString = "Promotion Category Changed ,Category id- " . $_POST['id'] . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
            $log = Message::log_details($_SESSION['admin']['username'], $logString);
            // **************************************************
            echo 1;
            exit;
        } else {
            echo 0;
            exit;
        }
    } else if ($_POST['actionrequest'] == 'addpermiscat') {
        $datamod = array();
        $datamod['catagory'] = $_POST['name'];
        $datamod['status'] = $_POST['status'];
        $type = $_POST['type'];
        $id = $_POST['eid'];
        if ($type == 0) {
            $dataexit = $db->query_first("SELECT COUNT(id) as sum FROM tblpermission_catagory WHERE catagory='" . $datamod['catagory'] . "'");
            if ($dataexit['sum'] > 0) {
                echo 2;
                exit;
            }

            $result = $db->query_insert("tblpermission_catagory", $datamod);
            if ($result) {
                //**************** generate log entry *******************
                $logString = "Add New Permission Category,Category id- " . $result . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************
                echo 1;
                exit;
            } else {
                echo 0;
                exit;
            }
        } else {
            $dataexits = $db->query_first("SELECT catagory FROM tblpermission_catagory WHERE id='" . $id . "'");

            if ($datamod['catagory'] == $dataexits['catagory']) {
                $result = $db->query_update("tblpermission_catagory", $datamod, "id=" . $id);
                if ($result) {
                    //**************** generate log entry *******************
                    $logString = "Updated Permission Category,Category id- " . $result . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                    $log = Message::log_details($_SESSION['admin']['username'], $logString);
                    // **************************************************
                    echo 1;
                    exit;
                } else {
                    echo 0;
                    exit;
                }
            } else {
                $dataexits = $db->query_first("SELECT COUNT(id) as sum FROM tblpermission_catagory WHERE catagory='" . $datamod['catagory'] . "'");
                if ($dataexits['sum'] > 0) {
                    echo 2;
                    exit();
                } else {
                    $result = $db->query_update("tblpermission_catagory", $datamod, "id=" . $id);
                    if ($result) {
                        //**************** generate log entry *******************
                        $logString = "Updated Permission Category,Category id- " . $result . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                        $log = Message::log_details($_SESSION['admin']['username'], $logString);
                        // **************************************************
                        echo 1;
                        exit;
                    } else {
                        echo 0;
                        exit;
                    }
                }
            }
        }
    } else if ($_POST['actionrequest'] == 'adduser') {
        $datamod = array();
        $datamod['name'] = $_POST['name'];
        $datamod['description'] = $_POST['desc'];
        $datamod['level'] = $_POST['cat'];
        $type = $_POST['type'];

        //name 	description 	email 	username 	password 	added_date 	active 	level
        if ($type == 0) {
            $datamod['username'] = $_POST['uname'];
            $datamod['password'] = MD5($_POST['pw']);

            $dataexit = $db->query_first("SELECT COUNT(id) as sum FROM tblusers WHERE username='" . $datamod['username'] . "'");
            if ($dataexit['sum'] > 0) {
                echo 2;
                exit;
            }
            $result = $db->query_insert("tblusers", $datamod);
            if ($result) {
                //**************** generate log entry *******************
                $logString = "Add New User,User id- " . $result . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************
                echo 1;
                exit;
            } else {
                echo 0;
                exit;
            }
        } else {
            $eid = $_POST['eid'];
            $result = $db->query_update("tblusers", $datamod, "id=" . $eid);
            if ($result) {
                //**************** generate log entry *******************
                $logString = "Update User,User id- " . $eid . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************
                echo 1;
                exit;
            } else {
                echo 0;
                exit;
            }
        }
        echo 0;
        exit;
    } else if ($_POST['actionrequest'] == 'addpermislevel') {

        $datamod = array();
        $datamod['page_name'] = $_POST['name'];
        $datamod['description'] = $_POST['desc'];
        $datamod['catagory'] = $_POST['cat'];
        $datamod['status'] = $_POST['status'];
        $type = $_POST['type'];
        $id = $_POST['eid'];

        if ($type == 0) {
            $dataexits = $db->query_first("SELECT COUNT(id) as sum FROM tblpermission_level WHERE page_name='" . $datamod['page_name'] . "'");
            if ($dataexits['sum'] > 0) {
                echo 2;
                exit;
            } else {

                $result = $db->query_insert("tblpermission_level", $datamod);
                if ($result) {
                    //**************** generate log entry *******************
                    $logString = "Add New Permission Level,Level id- " . $result . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                    $log = Message::log_details($_SESSION['admin']['username'], $logString);
                    // **************************************************
                    echo 1;
                    exit;
                } else {
                    echo 0;
                    exit;
                }
            }
        } else {
            $dataexits = $db->query_first("SELECT page_name FROM tblpermission_level WHERE id='" . $id . "'");

            if ($datamod['page_name'] == $dataexits['page_name']) {
                $result = $db->query_update("tblpermission_level", $datamod, "id=" . $id);
                if ($result) {
                    //**************** generate log entry *******************
                    $logString = "Updated Permission Level,Level id- " . $result . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                    $log = Message::log_details($_SESSION['admin']['username'], $logString);
                    // **************************************************
                    echo 1;
                    exit;
                } else {
                    echo 0;
                    exit;
                }
            } else {
                $dataexits = $db->query_first("SELECT COUNT(id) as sum FROM tblpermission_level WHERE page_name='" . $datamod['page_name'] . "'");
                if ($dataexits['sum'] > 0) {
                    echo 2;
                    exit();
                } else {
                    $result = $db->query_update("tblpermission_level", $datamod, "id=" . $id);
                    if ($result) {
                        //**************** generate log entry *******************
                        $logString = "Updated Permission Level,Level id- " . $result . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                        $log = Message::log_details($_SESSION['admin']['username'], $logString);
                        // **************************************************
                        echo 1;
                        exit;
                    } else {
                        echo 0;
                        exit;
                    }
                }
            }
        }
    }  else if ($_POST['actionrequest'] == 'addSliderImage') {

        if(isset($_FILES["slider_image"]) && $_FILES["slider_image"]["name"]!=""){

            /*$allowedTypes = array(IMAGETYPE_JPEG);
            $detectedType = exif_imagetype($_FILES['slider_image']['tmp_name']);
            $error = !in_array($detectedType, $allowedTypes);

            if($error){
                echo 0;
                exit;
            } else{*/
                $upload_image=upload::upload_images(DOC_ROOT . 'images/content/slider/', $_FILES['slider_image']);

                $data_arr['image_name'] = $upload_image;
                $data_arr['maincontent_id']=$_POST["maincontent_id"];


                $result = $db->query_insert("tblslider_images", $data_arr);
                if ($result) {
                        //**************** generate log entry *******************
                    $logString = "Add New Image to slider,Image id- " . $result . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                    $log = Message::log_details($_SESSION['admin']['username'], $logString);
                        // **************************************************
                    echo 1;
                    exit;
                } else {
                    echo 0;
                    exit;
                }
            //}
            echo 0;
            exit;
        }
        echo 0;
        exit;
    } else if ($_POST['actionrequest'] == 'edit_product_category') {
        $title=$_POST['title'];
        $id=$_POST['id'];

        if ($title=="") {
            echo "0";
            exit();
        } elseif (isset($_POST['id']) && $_POST['id']=="") {
            echo "0";
            exit();
        } else {
            $data=array();
            $data["title"]=$title;

            $result = $db->query_update("tblproduct_category", $data,"id=".$id);
            if ($result) {
                    //**************** generate log entry *******************
                $logString = "Update Product Category, id- " . $id . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                    // **************************************************
                echo 1;
                exit;
            } else {
                echo 0;
                exit;
            }
        }
    }
} else {
    echo 0;
    exit;
}
?>
