<?php
require_once('admin.php');
$per_tag                                = new Permission;
$per_tag->premission_tag                = "sitemain_data";
$permission_block			= $per_tag->check_permission($db);
if(!$permission_block){
	$INCLUDE_FILE = "permission_denied.php";
	require_once('template_main.php');
	exit();
}
//$name
$mainsitedata   =   getData::getMainSitedata();
$companyname    =   $mainsitedata['name'];
$details        =   $mainsitedata['details'];
$details2       =   $mainsitedata['details2'];
$details3       =   $mainsitedata['details3'];
$address1       =   $mainsitedata['address1'];
$address2       =   $mainsitedata['address2'];
$address3       =   $mainsitedata['address3'];
$address4       =   $mainsitedata['address4'];
$telephone1     =   $mainsitedata['tel1'];
$telephone2     =   $mainsitedata['tel2'];
$telephone3     =   $mainsitedata['tel3'];
$fax1           =   $mainsitedata['fax1'];
$fax2           =   $mainsitedata['fax2'];
$email1         =   $mainsitedata['email1'];
$email2         =   $mainsitedata['email2'];
$fb_link        =   $mainsitedata['fb_link'];
$g_link         =   $mainsitedata['g+_link'];
$twiter_link    =   $mainsitedata['twiter_link'];
$linkedin_link  =   $mainsitedata['linkedin_link'];
$pinterest_link =   $mainsitedata['pinterest_link'];
$flickr_link    =   $mainsitedata['flickr'];
$seo_code       =   $mainsitedata['SEO_code'];
$online         =   $mainsitedata['online'];


if(isset($_POST['btnsave'])){
    $companyname    =   $_POST['companyname'];
    $details        =   $_POST['details'];
    $details2       =   $_POST['details2'];
    $details3       =   $_POST['details3'];
    $address1       =   $_POST['address1'];
    $address2       =   $_POST['address2'];
    $address3       =   $_POST['address3'];
    $address4       =   $_POST['address4'];
    $telephone1     =   $_POST['telephone1'];
    $telephone2     =   $_POST['telephone2'];
    $telephone3     =   $_POST['telephone3'];
    $fax1           =   $_POST['fax1'];
    $fax2           =   $_POST['fax2'];
    $email1         =   $_POST['email1'];
    $email2         =   $_POST['email2'];
    $fb_link        =   $_POST['fb_link'];
    $g_link         =   $_POST['g_link'];
    $twiter_link    =   $_POST['twiter_link'];
    $linkedin_link  =   $_POST['linkedin_link'];
    $pinterest_link =   $_POST['pinterest_link'];
    $flickr_link    =   $_POST['flickr_link'];
    $seo_code       =   $_POST['seo_code'];
    $online         =   $_POST['online'];

    $data_main                  =   array();
    $data_main['name']          =   $companyname;
    $data_main['details']       =   $details;
    $data_main['details2']      =   $details2;
    $data_main['details3']      =   $details3;
    $data_main['address1']      =   $address1;
    $data_main['address2']      =   $address2;
    $data_main['address3']      =   $address3;
    $data_main['address4']      =   $address4;
    $data_main['tel1']          =   $telephone1;
    $data_main['tel2']          =   $telephone2;
    $data_main['tel3']          =   $telephone3;
    $data_main['fax1']          =   $fax1;
    $data_main['fax2']          =   $fax2;
    $data_main['email1']        =   $email1;
    $data_main['email2']        =   $email2;
    $data_main['fb_link']       =   $fb_link;
    $data_main['g+_link']       =   $g_link;
    $data_main['twiter_link']   =   $twiter_link;
    $data_main['linkedin_link'] =   $linkedin_link;
    $data_main['pinterest_link']=   $pinterest_link;
    $data_main['flickr']        =   $flickr_link;
    $data_main['SEO_code']      =   $seo_code;
    $data_main['online']        =   $online;
    $result = $db->query_update("tblmaindetails", $data_main, "id=1");
    if($result){
        //**************** generate log entry *******************
        $logString = "Updated Main Data  / USER - ".$_SESSION['admin']['username']." ID - ".$_SESSION['admin']['id'];
        $log = Message::log_details($_SESSION['admin']['username'],$logString);
        // **************************************************
        header('location:'.$_SERVER['PHP_SELF'].'?msg='.base64_encode(6).'');
	exit;
    }   else {
        header('location:'.$_SERVER['PHP_SELF'].'?err='.base64_encode(5).'');
	exit;
    }
}


$temp_heading = "Main Site Data";
$INCLUDE_FILE = "includes/maindata.tpl.php";

$breaddrum = '<li class="active">'.$temp_heading.'</li>';

$page_main_heading = '<i class="fa fa-list-alt"></i>&nbsp;&nbsp;'.'Manage Site Content';

require_once('template_main.php');
?>