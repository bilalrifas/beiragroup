<?php require_once('user.php');

$val = filter_var($_GET['val'], FILTER_SANITIZE_STRING);			// this has to filter and sanitize
$type = $_GET['type'];

if($val=="" || $type ==""){
	header("location: 404.html");
	exit();
}


// $val = str_replace("-", " ", $val);
// $val = str_replace(" and ", " & ", $val);
$val = clean_text($val);
$val = trim($val);

if($type=="main" || $type=="brief"){
	$main = content::get_main_content_by_heading_replace($val);
	if ($main) {
		$_GET['main']=$main['maincontent_id'];
		$_GET['brief']=($type=="brief") ? true : false ;
	} else {
		header("location: 404.html");
		exit();
	}

} elseif ($type="sub") {
	$sub = content::get_sub_content_by_heading_replace($val);
	if ($sub) {
		$_GET['sub']=$sub['subcontent_id'];
	} else {
		header("location: 404.html");
		exit();
	}
} else {
	header("location: 404.html");
	exit();
}



$requirefile="";

if(isset($_GET['main']) &&  is_numeric($_GET['main'])) {

	$page_content=$main;
	if($_GET['main']==5) {
	 $requirefile="includes/aboutus.php";
	} elseif ($_GET['main']==6) {
		$requirefile="includes/contactus.php";
	} elseif ($_GET['main']==7) {

		// here check for the post
		if($_SERVER['REQUEST_METHOD'] == 'POST')
		{
				// CSRF check
			    if($_POST['csrf_token'] != $_SESSION['csrftoken'])
			    {
			        // CSRF attack detected
			        //$result = $e->getMessage() . ' Form ignored.';
			        session_regenerate_id();
			        header("HTTP/1.0 404 Not Found");
			    }

			    header("Location: beira-search.html?q=".urlencode($_POST['q']));
		}
		$requirefile="includes/search.php";
	} elseif ($_GET['main']==9) {
		$requirefile="includes/sitemap.php";
	}

} else if(isset($_GET['sub'])&& is_numeric($_GET['sub'])) {
	$page_content=$sub;

} else{
	header("location: 404.html");
	exit();
}

require_once("templates/template.php");
?>

