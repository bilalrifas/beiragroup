<?php
require_once('user.php');
$home_details=content::get_maincontent_by_id(1);
?>
<?php include_once("detect.php"); 
//ini_set('display_errors','Off');
?>
<!DOCTYPE html>
<html class="tk-myriad-pro">
<head>
<?php include_once("meta_tags.php"); ?>

<meta name="google-site-verification" content="W1ny69Dv81wB-Aj1yTNcJ_ZdsD-VSy5XwX1rQJMzgTI" />
<?php include("includes/tag_manager_head.php"); ?>

<!--
	<meta charset="utf-8">
	<title><?php echo $home_details["meta_title"]; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="generator" content="Webflow"> -->

	<link rel="shortcut icon" type="image/x-icon" href="images/favic.jpg">
	<link rel="apple-touch-icon" href="images/webclip.jpg">

	<link rel="stylesheet" type="text/css" href="css/normalize.min.css">
	<link rel="stylesheet" type="text/css" href="css/webflow.css">
	<link rel="stylesheet" type="text/css" href="css/beiragroup.webflow.css">
	<link rel="stylesheet" type="text/css" href="css/styles.css">
	<link rel="stylesheet" type="text/css" href="webfont/stylesheet.min.css">
	<link rel="stylesheet" type="text/css" href="css/magnific-popup.css">
	<script src="//use.typekit.net/mrg7dwk.js"></script>
  	<script>try{Typekit.load();}catch(e){}</script>
  	<?php include_once("ie-fix.php"); ?>
	
	<script src="js/webfont.js"></script>
	
<style>

#container {
  max-width: 1000px;
  margin: 0 auto;
  background: #EEE;
}

h1,
p { padding: 1em 1em; }

#fvpp-blackout {
  display: none;
  z-index: 499;
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  background: #000;
  opacity: 0.5;
}

#my-welcome-message {
  display: none;
  z-index: 500;
  position: fixed;
  width: 53%;
  left: 23%;
  top: 16%;
  padding: 20px 2%;
  font-family: Calibri, Arial, sans-serif;
  background: #FFF;
}

#fvpp-close {
  position: absolute;
  top: 10px;
  right: 20px;
  cursor: pointer;
}

#fvpp-dialog h2 {
  font-size: 2em;
  margin: 0;
}

#fvpp-dialog p { margin: 0; }
</style>
</head>
<body>



	<div class="header">
	
		<?php
		require_once('header.php');
	$main_contents=content::get_active_main_content();
	?>
	

	</div>
	<div class="body">
		<div class="body-container">
			<?php
			foreach ($main_contents as $key => $row) {
				?>
				<div class="fixer w-inline-block">
					<a class="w-inline-block catagory" id="main_content_box_<?php echo $key+1;?>" href="brief-<?php echo str_replace(' ', '-', str_replace('&', 'and', $row['heading']));?>.html">
						<img class="cat-image" src="images/content/banner/<?php echo $row['image3'];?>" alt="<?php echo $row['heading'];?>">
						<div class="cat-title" id="a<?php echo $key+1;?>"><?php echo $row['heading'];?></div>
					</a>
					<img class="shadow" src="images/shadow.png" alt="shadow">
				</div>
				<?php
			}
			?>

		</div>
	</div>

	<!-- footer start -->
	<div class="footerI">
	<div class="copyright">

	<a  href="http://<?php echo HTTP_PATH; ?>">home | </a>
	<a  href="beira-<?php echo str_replace(' ', '-', str_replace('&', 'and', $about_us_content['heading']));?>.html">careers | </a>
	<a  href="beira-<?php echo str_replace(' ', '-', str_replace('&', 'and', $contact_us_content['heading']));?>.html">contact us | </a>
	<!-- <a class="footer" href="#">privacy policy |  </a> -->
	<a  href="beira-Site-Map.html">site map | </a>

	<script type="text/javascript">
	    var date = new Date();
	    var year = date.getYear();
	    year = year < 1000 ? year + 1900 : year;
	    document.write('&copy;' + '&nbsp;' + year);
	</script>
	Beira Group |
	<a  href="http://www.3cs.lk">Web Design by 3CS</a>
	</div>
	</div>
	<!-- footer ends -->


	<!--[if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"><![endif]-->


	<script type="text/javascript" src="js/jquery1.11.1.js"></script>
	<script src="js/popup.js"></script>


	<!-- <script>
	    $(document).ready(function() {

	      // Initialize the plugin
	      $('#my_popup').popup({
	      	<?php if(!isset($_SESSION['per_user']) && $_SESSION['per_user']!="TRUE"){ ?>
	        autoopen: true
	        <?php } ?>
	      });

	    });
	  </script>-->


	<script>
		$('#form').mouseover(function(){
			$('#input').animate({opacity:'1',width:'101px'},'linear');
		});
		$('#btn').click(function(){
			$('#input').animate({width:'0px',opacity:'0'},'linear');
		});
	</script>

	<?php //echo $seo_code; ?>
	<script>
		WebFont.load({
			google: {
				families: ["Open Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic","Ubuntu:300,300italic,400,400italic,500,500italic,700,700italic"]
			}
		});
	</script>
	
	<script type="text/javascript" src="js/modernizr.js"></script>
	<script type="text/javascript" src="js/webflow.js"></script>
	<!--<div id="my_popup">

    		<img class="award" src="images/award.jpg" alt="award">
    		<button class="my_popup_close">X</button>

  	</div>-->

<!-- POP UP Window  -->
<!--<div class="promo-banner"><img src="images/main-popup.jpg" alt="Notice"></div>-->



<?php
	$_SESSION['per_user']="TRUE";
?>



<!-- POP UP JS -->
<script src="js/jquery.magnific-popup.min.js"></script>

<?php if(!isset($_COOKIE['LAST_VISITED'])): ?>
<!--
<script>
(function($) {
    $(window).load(function () {
        // retrieved this line of code from http://dimsemenov.com/plugins/magnific-popup/documentation.html#api
        $.magnificPopup.open({
            items: {
                src: 'images/main-popup.jpg'
            },
            type: 'image'

        }, 0);
    });
})(jQuery);
</script>
-->
<?php endif; ?>
<!--End  POP UP JS -->

</body>
</html>