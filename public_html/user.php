<?php require_once('top.php');
require_once(DOC_ROOT.'classes/content_class.php');
require_once(DOC_ROOT.'classes/user_class.php');
require_once(DOC_ROOT.'classes/mailing.class.php');
require_once(DOC_ROOT.'classes/Pager.class.php');
require_once(DOC_ROOT . 'classes/gump.class.php');
require_once(DOC_ROOT . 'classes/htmlpurifier/library/HTMLPurifier.auto.php');
require_once(DOC_ROOT . 'classes/securimage/securimage.php');

$config = HTMLPurifier_Config::createDefault();
$purifier = new HTMLPurifier($config);
$securimage = new Securimage();

$user = new user();
?>
