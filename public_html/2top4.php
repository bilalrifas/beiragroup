<?php 
error_reporting(E_ALL);
//error_reporting(E_ALL && ~E_DEPRECATED && ~E_NOTICE);
date_default_timezone_set("Asia/Colombo");
//ini_set('session.cookie_httponly','1');
//ini_set('session.cookie_secure','1');
//session_save_path('/home/m5elo21a/tmp');
session_start();


require_once('/home/m5elo21a/application/config/config.php');
require_once(DOC_ROOT.'classes/database_class.php');
require_once(DOC_ROOT.'config/functions.php');
include_once(DOC_ROOT.'classes/nocsrf.php');


$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$db->connect();

$mainsitedata   =   $db->query_first("SELECT * FROM  tblmaindetails WHERE id=1");
$companyname    =   $mainsitedata['name'];
$factoryname1  	=   $mainsitedata['details'];
$factoryname2  	=  	$mainsitedata['details2'];
$factoryname3   =   $mainsitedata['details3'];
$address1       =   $mainsitedata['address1'];
$address2       =   $mainsitedata['address2'];
$address3       =   $mainsitedata['address3'];
$address4       =   $mainsitedata['address4'];
$telephone1     =   $mainsitedata['tel1'];
$telephone2     =   $mainsitedata['tel2'];
$telephone3     =   $mainsitedata['tel3'];
$fax1           =   $mainsitedata['fax1'];
$fax2           =   $mainsitedata['fax2'];
$email1         =   $mainsitedata['email1'];
$email2         =   $mainsitedata['email2'];
$fb_link        =   $mainsitedata['fb_link'];
$g_link         =   $mainsitedata['g+_link'];
$twiter_link    =   $mainsitedata['twiter_link'];
$linkedin_link  =   $mainsitedata['linkedin_link'];
$pinterest_link =   $mainsitedata['pinterest_link'];
$flickr_link    =   $mainsitedata['flickr'];
$seo_code       =   $mainsitedata['SEO_code'];
$online         =   $mainsitedata['online'];


//image resizing
if(isset($_GET['action']) && $_GET['action']!=""){
      switch ($_GET['action']) {
          case 'signout':
              unset($_SESSION['member']);
         break;
     }
}

// popup check
if(!isset($_COOKIE['LAST_VISITED']))
{
	setcookie('LAST_VISITED',md5(time()));
	setcookie('IPADDR', $_SERVER['REMOTE_ADDR']);
}