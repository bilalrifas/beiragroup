<?php
/*
 * @author 3CS
 * @email - rushan.niroshana@gmail.com
 */
 
class Message{


	public static function showMsg($id){
		
		$message="";
		
		switch(base64_decode($id)){
			case '1':
				$message="<div id='success_message_body' >Welcome ".$_SESSION['admin']['username']." </div>";
			break;
			case '2':
				$message="Invalid username or password";
			break;
			case '3':
				$message="<div id='error_message_body' >Please log in</div>";
			break;
			case '4':
				$message="<div id='error_message_body' >Permission denied</div>";
			break;
			case '5':
				$message="<div id='error_message_body' >Action failed</div>";
			break;
			case '6':
				$message="<div id='success_message_body' >Successfully updated</div>";
			break;
			case '7':
				$message="<div id='success_message_body' >Successfully inserted</div>";
			break;
			case '8':
				$message="<div id='success_message_body' >Successfully deleted</div>";
			break;
			case '9':
				$message="<div id='success_message_body' >Successfully sent</div>";
			break;
			case '10':
				$message="Your account inactived";
			break;

		}

		return $message;
	}
        static function log_details($user,$text){
		global $db;
                $data_arr['log_date'] 	= date('Y-m-d');
                $data_arr['log_time'] 	= date('H:i:s');
                $data_arr['user_name'] 	= $user;
                $data_arr['description']= $text;
                $data_arr['ip_add']     =$_SERVER['REMOTE_ADDR'];
                $result=$db->query_insert("tbllog", $data_arr);
		return $result;
	}
	
}


?>
