<?php
require 'vendor/autoload.php';

use GuzzleHttp\Client;

class SlackNotifier
{

	// for guzzle instance
    private $gclient_config = [
        'verify'          => false
    ];

    // for slack client
	private $slack_settings = [
		'channel' => '@amila3cs',
		'username' => 'Beira Group'
	];

	private $guzzle;

	private $markdowntpl = '../templates/markdown/notification.md';

	const SLACK_ENDPOINT = 'https://hooks.slack.com/services/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';
	const DOMAIN = 'beiragroup.com';

	public function __construct()
	{
		// new guzzle instance
		$this->guzzle = new Client($this->gclient_config);
	}

	public function notify($data)
	{
		// init slack client
		$slackclient = new Maknz\Slack\Client(self::SLACK_ENDPOINT, $this->slack_settings, $this->guzzle);

		// prepare message
		try{
			if(!$msgbody = file_get_contents($this->markdowntpl))
			{
				throw new Exception('Couldn\'t Open Template');
			}
		}catch(Exception $e)
		{
			die($e->getMessage());
		}
      	

     	$msgbody = str_replace('{SITENAME}', self::DOMAIN, $msgbody);
      	$msgbody = str_replace('{NAME}', $data['guest_name'], $msgbody);
      	$msgbody = str_replace('{EMAIL}', $data['guest_email'], $msgbody);
      	$msgbody = str_replace('{MESSAGE}', $data['guest_msg'], $msgbody);
      	$msgbody = str_replace('{DATE}', date('Y-m-d'), $msgbody);

      	$slackclient->send($msgbody);

	}
}