<?php

class Pager
{
   
    function Pager($options = array())
    {
          if (get_class($this) == 'pager') { 
            eval('$this = Pager::factory($options);');
        } else { 
            $msg = 'Pager constructor is deprecated.'
                  .' You must use the "Pager::factory($params)" method'
                  .' instead of "new Pager($params)"';
            trigger_error($msg, E_USER_ERROR);
        }
    }

   

    /**
     * Return a pager based on $mode and $options
     *
     * @param  array $options Optional parameters for the storage class
     * @return object Object   Storage object
     * @static
     * @access public
     */
    static function factory($options = array())
    {
        $mode = (isset($options['mode']) ? ucfirst($options['mode']) : 'Jumping');
        $classname = 'Pager_' . $mode;
        $classfile = 'Pager' . DIRECTORY_SEPARATOR . $mode . '.php';
        // Attempt to include a custom version of the named class, but don't treat
        // a failure as fatal.  The caller may have already included their own
        // version of the named class.
        if (!class_exists($classname)) {
            include_once 'page_ination/Sliding.php';
        }

        // If the class exists, return a new instance of it.
        if (class_exists($classname)) {
            $pager =new $classname($options);
            return $pager;
        }

        $null = null;
        return $null;
    }

  
}
?>