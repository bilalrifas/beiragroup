<?php

class content {

	static function get_all_main_content(){
		global $db;
		$sql = "SELECT *  FROM tblmain_content ORDER BY maincontent_id ASC";
		$result = $db->fetch_all_array($sql);
		return $result;
	}

	static function get_active_main_content(){
		global $db;
		$sql = "SELECT *  FROM tblmain_content WHERE status=1 ORDER BY maincontent_id ASC";
		$result = $db->fetch_all_array($sql);
		return $result;
	}

	static function get_main_content_by_heading($heading){
		global $db;
		$sql = "SELECT * FROM tblmain_content WHERE heading = '".$heading."'";
		$result = $db->query_first($sql);
		return $result;
	}


	static function get_sub_content_by_heading($heading){
		global $db;
		$sql = "SELECT * FROM tblsub_content WHERE heading = '".$heading."'";
		$result = $db->query_first($sql);
		return $result;
	}

	static function get_subcontents_by_main_id($maincontent_id){
		global $db;
		$sql = "SELECT *  FROM tblsub_content WHERE maincontent_id=$maincontent_id  ORDER BY display_order  ASC";
		$result = $db->fetch_all_array($sql);
		return $result;
	}

	static function get_subothercontents_by_sub_id($subcontent_id){
		global $db;
		$sql = "SELECT * FROM tblsubother_content  WHERE subcontent_id=$subcontent_id ORDER BY display_order ASC";
		$result = $db->fetch_all_array($sql);
		return $result;
	}

	static function get_subother_otherContent_by_subotherID($subotherid) {
		global $db;
		$sql = "SELECT * FROM tblsubother_other_content WHERE subothercontent_id=".$subotherid;
		$result = $db->fetch_all_array($sql);
		return $result;
	}

	static function get_all_menu_main_headings(){
		$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
		$db->connect();
		$sql = "SELECT maincontent_id,heading
		FROM tblmain_content
		ORDER BY heading  ASC";
		$result = $db->fetch_all_array($sql);
		return $result;
	}

	static function get_maincontent_by_id($maincontent_id){
		global $db;
		$sql = "SELECT *
		FROM tblmain_content
		WHERE maincontent_id ='".$maincontent_id."'";
		$record = $db->query_first($sql);
		return $record;
	}

	static function update_main_content($data,$maincontent_id){
		global $db;
		$res = $db->query_update("tblmain_content", $data, "maincontent_id=".$maincontent_id);
		return $res;
	}

	static function insert_sub_content($data){
		global $db;
		$id=$db->query_insert("tblsub_content", $data);
		return $id;
	}

	static function get_subcontent_by_id($subcontent_id){
		global $db;
		$sql = "SELECT *
		FROM tblsub_content
		WHERE subcontent_id ='".$subcontent_id."'";
		$record = $db->query_first($sql);
		return $record;
	}

	static function update_sub_content($data,$subcontent_id){
		global $db;
		$res = $db->query_update("tblsub_content", $data, "subcontent_id=".$subcontent_id);
		return $res;
	}



	static function change_sub_content_status($status,$subcontent_id){
		global $db;
		$data = array();
		$data['status'] 	= $status;
		$res = $db->query_update("tblsub_content", $data, "subcontent_id=".$subcontent_id);
		return $res;
	}

	static function delete_subcontent($subcontent_id){
		global $db;
		$sql = "DELETE FROM tblsub_content WHERE subcontent_id =".$subcontent_id;
		$res = $db->query($sql);
		return $res;
	}

	static function delete_content_image($maincontent_id){
		global $db;
		$data = array();
		$data['image'] 	= "";
		$res = $db->query_update("tblmain_content", $data, "maincontent_id=".$maincontent_id);
		return $res;
	}

	static function delete_subcontent_image($subcontent_id){
		global $db;
		$data = array();
		$data['image'] 	= "";
		$res = $db->query_update("tblsub_content", $data, "subcontent_id=".$subcontent_id);
		return $res;
	}

	static function get_subothercontent_by_id($subothercontent_id){
		global $db;
		$sql = "SELECT *
		FROM tblsubother_content
		WHERE subothercontent_id ='".$subothercontent_id."'";
		$record = $db->query_first($sql);
		return $record;
	}

	static function update_subother_content($data,$subothercontent_id){
		global $db;
		$res = $db->query_update("tblsubother_content", $data, "subothercontent_id=".$subothercontent_id);
		return $res;
	}

	static function insert_subother_content($data){
		global $db;
		$id=$db->query_insert("tblsubother_content", $data);
		return $id;
	}



	static function delete_subothercontent($subothercontent_id){
		global $db;
		$sql = "DELETE FROM tblsubother_content WHERE subothercontent_id =".$subothercontent_id;
		$res = $db->query($sql);
		return $res;
	}

	static function change_subothercontent_status($status,$subothercontent_id){
		global $db;
		$data = array();
		$data['status'] 	= $status;
		$res = $db->query_update("tblsubother_content", $data, "subothercontent_id=".$subothercontent_id);
		return $res;
	}

	static function delete_subothercontent_image($subothercontent_id){
		global $db;
		$data = array();
		$data['image'] 	= "";
		$res = $db->query_update("tblsubother_content", $data, "subothercontent_id=".$subothercontent_id);
		return $res;
	}

	static function set_sub_display_order($data,$subcontent_id){
		global $db;
		$res = $db->query_update("tblsub_content", $data, "subcontent_id=".$subcontent_id);
		return $res;
	}
	//------front------------

	static function get_active_main_menu(){
		global $db;
		$sql = "SELECT maincontent_id,heading
		FROM tblmain_content  WHERE status=1 AND maincontent_id!=1
		ORDER BY display_order ASC";
		$result = $db->fetch_all_array($sql);
		return $result;
	}

	static function get_all_main_menu(){
		global $db;
		$sql = "SELECT * FROM tblmain_content WHERE maincontent_id!=1 ORDER BY display_order ASC";
		$result = $db->fetch_all_array($sql);
		return $result;
	}

	static function get_active_submenus_by_main_id($maincontent_id){
		global $db;
		$sql = "SELECT subcontent_id,heading,image2
		FROM tblsub_content
		WHERE maincontent_id=$maincontent_id
		AND status=1
		ORDER BY display_order ASC";
		$result = $db->fetch_all_array($sql);
		return $result;
	}

	static function get_subcontent_data_by_id($subcontent_id){
		global $db;
		$sql = "SELECT sc.*,sc.image1,sc.image2,mc.maincontent_id,mc.heading as main_heading
		FROM tblsub_content sc
		LEFT JOIN tblmain_content mc ON mc.maincontent_id=sc.maincontent_id
		WHERE sc.subcontent_id ='".$subcontent_id."'";
		$record = $db->query_first($sql);
		return $record;
	}

	static function get_active_subcontents_by_main_id($maincontent_id){
		global $db;
		$sql = "SELECT subcontent_id,heading,image1,image2
		FROM tblsub_content
		WHERE maincontent_id=$maincontent_id
		AND status=1
		ORDER BY display_order ASC";
		$result = $db->fetch_all_array($sql);
		return $result;
	}

	static function get_all_subcontents_by_main_id($maincontent_id){
		global $db;
		$sql = "SELECT * FROM tblsub_content
		WHERE maincontent_id=$maincontent_id
		ORDER BY display_order ASC";
		$result = $db->fetch_all_array($sql);
		return $result;
	}

	static function get_active_subothermenus_by_sub_id($subcontent_id){
		global $db;
		$sql = "SELECT subothercontent_id,heading,image1,image2
		FROM tblsubother_content
		WHERE subcontent_id=$subcontent_id
		AND status=1
		ORDER BY display_order ASC";
		$result = $db->fetch_all_array($sql);
		return $result;
	}

	static function get_subothercontent_data_by_id($subothercontent_id){
		global $db;
		$sql = "SELECT soc.*,mc.heading as main_heading,sc.heading as sub_heading,soc.image1,soc.image2
		FROM tblsubother_content soc
		LEFT JOIN tblmain_content mc ON mc.maincontent_id=soc.maincontent_id
		LEFT JOIN tblsub_content sc ON sc.subcontent_id=soc.subcontent_id
		WHERE soc.subothercontent_id ='".$subothercontent_id."' ";
		$record = $db->query_first($sql);
		return $record;
	}

	static function get_active_subothercontents_headings($subcontent_id){
		global $db;
		$sql = "SELECT subothercontent_id,heading
		FROM tblsubother_content
		WHERE subcontent_id=$subcontent_id
		AND status=1
		ORDER BY display_order ASC";
		$result = $db->fetch_all_array($sql);
		return $result;
	}

	static function get_translationcontent($type,$content_id,$language_id){
		global $db;
		$sql = "SELECT tc.*,tc.heading as main_heading
		FROM tbltranslationcontent tc
		WHERE tc.type =".$type." AND tc.content_id=".$content_id." AND tc.language_id=".$language_id."";

		mysql_query("SET character_set_results=utf8");

		$record = $db->query_first($sql);
		return $record;
	}

	static function get_hotel_info(){
		global $db;

		$sql = "SELECT telephone,email FROM tblhotel WHERE hotel_id=1";

		$record = $db->query_first($sql);
		return $record;
	}

	static function get_active_content_images($content_id,$content_type){
		global $db;
		$sql = "SELECT ci.* FROM tblcontent_images ci
		WHERE ci.content_type =".$content_type." AND ci.content_id=".$content_id." AND ci.status=1
		ORDER BY ci.id ASC  ";
		$result = $db->fetch_all_array($sql);
		return $result;
	}

	static function	get_content_search_values($keyword){
		global $db;

		$search_arr=array();

		$sql="SELECT * FROM tblmain_content
		WHERE  ( heading LIKE '%".$keyword."%' || details LIKE '%".$keyword."%' ) AND status=1";
		$result1 = $db->fetch_all_array($sql);

		if(sizeof($result1)>0)
			$search_arr[]=$result1;


		$sql="SELECT * FROM tblsub_content
		WHERE  ( heading LIKE '%".$keyword."%' || details LIKE '%".$keyword."%' ) AND status=1 AND maincontent_id IN (SELECT maincontent_id FROM tblmain_content)";
		$result2 = $db->fetch_all_array($sql);

		if(sizeof($result2)>0)
			$search_arr[]=$result2;

		$sql="SELECT * FROM tblsubother_content
		WHERE  ( heading LIKE '%".$keyword."%' || details LIKE '%".$keyword."%' ) AND status=1 AND  subcontent_id IN (SELECT subcontent_id FROM tblsub_content)";
		$result3 = $db->fetch_all_array($sql);

		if(sizeof($result3)>0)
			$search_arr[]=$result3;

		$sql="SELECT * FROM tbljob_categories
		WHERE  ( category LIKE '%".$keyword."%' || description LIKE '%".$keyword."%' ) AND status=1";
		$result4 = $db->fetch_all_array($sql);

		if(sizeof($result4)>0)
			$search_arr[]=$result4;

		return $search_arr;
	}
	static function	get_port_content_search_values($keyword){
		global $db;

		$search_arr=array();

		$sql="SELECT * FROM tblportfolio
		WHERE  ( title LIKE '%".$keyword."%')";
		$result1 = $db->fetch_all_array($sql);

		if(sizeof($result1)>0)
			$search_arr[]=$result1;

		return $search_arr;
	}

	static function	get_news_content_search_values($keyword){
		global $db;

		$search_arr=array();

		$sql="SELECT * FROM tblnews
		WHERE  ( news_title LIKE '%".$keyword."%' OR news LIKE '%".$keyword."%')";
		$result1 = $db->fetch_all_array($sql);

		if(sizeof($result1)>0)
			$search_arr[]=$result1;

		return $search_arr;
	}

	static function	get_all_content_search_values(){
		global $db;

		$search_arr=array();

		$sql="SELECT * FROM tblmain_content";
		$result1 = $db->fetch_all_array($sql);

		if(sizeof($result1)>0)
			$search_arr[]=$result1;


		$sql="SELECT * FROM tblsub_content WHERE status=1 AND maincontent_id IN (SELECT maincontent_id FROM tblmain_content)";
		$result2 = $db->fetch_all_array($sql);

		if(sizeof($result2)>0)
			$search_arr[]=$result2;

		$sql="SELECT * FROM tblsubother_content WHERE status=1 AND subcontent_id IN (SELECT subcontent_id FROM tblsub_content)";
		$result3 = $db->fetch_all_array($sql);

		if(sizeof($result3)>0)
			$search_arr[]=$result3;

		return $search_arr;
	}
	static function get_home_content(){
		global $db;
		$sql = "SELECT *
		FROM tblmain_content
		WHERE maincontent_id=1";
		$record = $db->query_first($sql);
		return $record;
	}
	static function get_subother_other_content_data_by_id($subothercontent_id){
		global $db;
		$sql = "SELECT soc.*,mc.heading as main_heading,sc.heading as sub_heading
		FROM tblsubother_content soc
		LEFT JOIN tblmain_content mc ON mc.maincontent_id=soc.maincontent_id
		LEFT JOIN tblsub_content sc ON sc.subcontent_id=soc.subcontent_id
		LEFT JOIN tblsubother_other_content oc ON oc.subcontent_id=soc.subcontent_id
		WHERE soc.subothercontent_id ='".$subothercontent_id."' ";
		$record = $db->query_first($sql);
		return $record;
	}
	static function get_active_other_subcontents_by_main_id($maincontent_id){
		global $db;
		$sql = "SELECT *
		FROM tblsubother_other_content
		WHERE subothercontent_id=$maincontent_id
		AND status=1
		ORDER BY display_order ASC";
		$result = $db->fetch_all_array($sql);
		return $result;
	}
	static function get_subother_content_inner_data_by_id($subothercontent_id){
		global $db;
		$sql = "SELECT soc.heading as subotherheading,soc.subothercontent_id,soc.maincontent_id,soc.subcontent_id,mc.heading as main_heading,sc.heading as sub_heading,sc.image1 as backimg1,oc.*
		FROM tblsubother_content soc
		LEFT JOIN tblmain_content mc ON mc.maincontent_id=soc.maincontent_id
		LEFT JOIN tblsub_content sc ON sc.subcontent_id=soc.subcontent_id
		LEFT JOIN tblsubother_other_content oc ON oc.subcontent_id=soc.subcontent_id
		WHERE oc.subother_other_content_id ='".$subothercontent_id."' ";
		$record = $db->query_first($sql);
		return $record;
	}
	static function get_contactus_details($maincontent_id){
		global $db;
		$sql = "SELECT *
		FROM tblmain_content
		WHERE maincontent_id=$maincontent_id
		AND status=1";
		$result = $db->fetch_all_array($sql);
		return $result;
	}
	static function get_active_middle_subcontents_by_main_id($sub_id){
		global $db;
		$sql = "SELECT *
		FROM tblsubother_content
		WHERE subcontent_id=$sub_id
		AND status=1
		ORDER BY display_order ASC";
		$result = $db->fetch_all_array($sql);
		return $result;
	}
	static function get_active_sub_middle_menus_by_sub_id($subcontent_id){
		global $db;
		$sql = "SELECT *
		FROM tblsub_content
		WHERE maincontent_id=$subcontent_id
		AND status=1
		ORDER BY display_order ASC";
		$result = $db->fetch_all_array($sql);
		return $result;
	}
	static function get_portfolio_content(){
		global $db;
		$sql = "SELECT pf.*,pc.category_name
		FROM tblportfolio pf
		LEFT JOIN tblport_category pc ON pc.id =pf.category_id
		WHERE pf.home=1 AND pf.status=1
		ORDER BY pf.display_order ASC LIMIT 0,8";
		$result = $db->fetch_all_array($sql);
		return $result;
	}
	static function get_testimonials_content(){
		global $db;
		$sql = "SELECT *
		FROM tbltestimonials WHERE status=1 ORDER BY id DESC";
		$record = $db->query_first($sql);
		return $record;
	}
	static function get_awards_content(){
		global $db;
		$sql = "SELECT *
		FROM tblawards WHERE status=1 AND home=1 ORDER BY display_order DESC LIMIT 0,2";
		$result = $db->fetch_all_array($sql);
		return $result;
	}
	static function get_awards_latest_content(){
		global $db;
		$sql = "SELECT *
		FROM tblawards WHERE status=1 ORDER BY id DESC";
		$record = $db->query_first($sql);
		return $result;
	}
	static function get_awards_all_content(){
		global $db;
		$sql = "SELECT *
		FROM tblawards WHERE status=1 ORDER BY id DESC";
		$result = $db->fetch_all_array($sql);
		return $result;
	}
	static function get_active_main_slides(){
		global $db;
		$sql = "SELECT *
		FROM tblmain_slides WHERE status=1 ORDER BY display_order ASC,id DESC";
		$result = $db->fetch_all_array($sql);
		return $result;
	}
	static function get_active_fotter_data(){
		global $db;
		$sql = "SELECT maincontent_id,heading
		FROM tblmain_content
		WHERE maincontent_id=2 OR maincontent_id=3 OR maincontent_id=4 OR maincontent_id=5
		ORDER BY display_order ASC";
		$result = $db->fetch_all_array($sql);
		return $result;
	}
	static function get_quick_links(){
		global $db;
		$sql = "SELECT subcontent_id,heading
		FROM tblsub_content WHERE status=1 AND subcontent_id=20 OR subcontent_id=21 OR subcontent_id=22 OR subcontent_id=23 ORDER BY RAND() LIMIT 0,16";
		$result = $db->fetch_all_array($sql);
		return $result;
	}
	static function get_active_subprod_by_main_id($maincontent_id){
		global $db;
		$sql = "SELECT subcontent_id,heading,image2
		FROM tblsub_content
		WHERE maincontent_id=$maincontent_id
		AND status=1
		ORDER BY display_order ASC LIMIT 0,4";
		$result = $db->fetch_all_array($sql);
		return $result;
	}
	static function get_active_main_menu_site_map(){
		global $db;
		$sql = "SELECT maincontent_id,heading
		FROM tblmain_content  WHERE status=1 AND maincontent_id!=1
		ORDER BY display_order ASC LIMIT 0,7";
		$result = $db->fetch_all_array($sql);
		return $result;
	}
	static function get_active_main_images(){
		global $db;
		$sql = "SELECT *
		FROM tblmiddlebanner  WHERE status=1
		ORDER BY id ASC";
		$result = $db->fetch_all_array($sql);
		return $result;
	}

	static function get_active_slider_images($maincontent_id){
		global $db;
		$sql = "SELECT *
		FROM tblslider_images  WHERE status=1 AND maincontent_id=$maincontent_id
		ORDER BY display_order ASC";
		$result = $db->fetch_all_array($sql);
		return $result;
	}

	static function get_active_product_categories(){
		global $db;
		$sql = "SELECT *
		FROM tblproduct_category  WHERE status=1
		ORDER BY display_order ASC";
		$result = $db->fetch_all_array($sql);
		return $result;
	}

	static function get_product_category_id_by_heading($heading){
		global $db;

		$sql = "SELECT id FROM tblproduct_category WHERE  '$heading' = REPLACE(title, ' ', '')";
		$result = $db->query_first($sql);
		return $result;
	}

	static function get_first_subcat_by_mainid($id){
		global $db;
		$sql = "SELECT heading FROM tblproduct_subcategory WHERE category_id =$id AND status=1 ORDER BY display_order ASC";
		$result = $db->query_first($sql);
		return $result;
	}

	static function get_product_subcategory_by_title($title,$id){
		global $db;
		$sql = "SELECT * FROM tblproduct_subcategory WHERE REPLACE(heading, ' ', '') = '$title' AND category_id=$id ";
		$result = $db->query_first($sql);
		return $result;
	}

	static function get_all_active_subcategories_by_categoryid($id){
		global $db;
		$sql = "SELECT * FROM tblproduct_subcategory WHERE category_id = $id AND status=1 ORDER BY display_order ASC";
		$result = $db->fetch_all_array($sql);
		return $result;
	}

	static function get_active_products_by_subcategory($catid,$subcatid){
		global $db;
		$sql = "SELECT * FROM tblproducts WHERE category_id = $catid AND subcategory_id=$subcatid  AND status=1 ORDER BY display_order ASC";
		$result = $db->fetch_all_array($sql);
		return $result;
	}

	static function get_product_content_from_title($catid,$subcatid,$title){
		global $db;
		$sql = "SELECT * FROM tblproducts WHERE category_id = $catid AND subcategory_id=$subcatid AND REPLACE(product_id, ' ', '') = '$title'";
		$result = $db->query_first($sql);
		return $result;
	}

	static function get_active_admin_emails(){
		global $db;
		$sql = "SELECT * FROM tbladminemails WHERE activated=1";
		$result = $db->fetch_all_array($sql);
		return $result;
	}

	static function get_main_content_by_heading_replace($heading){
		global $db;
		$sql = "SELECT * FROM tblmain_content WHERE REPLACE(REPLACE(heading, '&', 'and'), ' ', '-') = '$heading' ";
		$result = $db->query_first($sql);
		return $result;
	}

	static function get_sub_content_by_heading_replace($heading){
		global $db;
		$sql = "SELECT * FROM tblsub_content WHERE REPLACE(REPLACE(heading, '&', 'and'), ' ', '-') = '$heading' ";
		$result = $db->query_first($sql);
		return $result;
	}

}

?>
