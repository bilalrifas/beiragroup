<?php

function text_resize($eng_story, $val) {
    $eng_limit = $val;
    $eng_story1 = strip_tags($eng_story);
    $eng_text = "";
    $eng_white = strpos($eng_story1, " ", $eng_limit);
    if ($eng_white !== false) {
        $eng_text = substr($eng_story1, 0, $eng_white);
        $eng_text .= " ... ";
    } else {
        $eng_text = $eng_story1;
    }
    return $eng_text;
}

function validDate($str) {
    $text = strtotime($str);
    $year = date('Y', $text);

    return ($year < date('Y')) ? false : true;
}

function validtelephone($telephone) {
    $flag = 0;
    $telephone1 = str_split(trim($telephone));
    $valid_characters = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "+", "-", "-", " ", "", ")", "(");
    foreach ($telephone1 as $val) {
        if (!in_array($val, $valid_characters)) {
            $flag = 1;
        }
    }

    if ($flag == 1) {
        return false;
    } else {
        return true;
    }
}

function checkMenu($permission_tag) {
    global $db;

    $per_tag = new Permission;
    $per_tag->premission_tag = $permission_tag;
    $permission_block = $per_tag->check_permission($db);

    return $permission_block;
}

function checkBlockPermissions($args = array()) {
    foreach ($args as $arg) {
        if (checkMenu($arg)) {
            return true;
        }
    }

    return false;
}

function send_mail($user, $text) {

    $data_arr['log_date'] = date('Y-m-d');
    $data_arr['log_time'] = date('H:i:s');
    $data_arr['user_name'] = $user;
    $data_arr['description'] = $text;
    $data_arr['ip_add'] = $_SERVER['REMOTE_ADDR'];

    $insert_id = $db->query_insert("tbllog", $data_arr);
}

function imageResize($width, $height, $target) {
    if ($width > $height) {
        $percentage = ($target / $width);
    } else {
        $percentage = ($target / $height);
    }
    $width = round($width * $percentage);
    $height = round($height * $percentage);
    return "width=\"$width\" height=\"$height\"";
}

function clean_text($string) {
    $filter = xss_clean($string);
    $string = mysql_real_escape_string(trim($filter));
    $search = array('@<script[^>]*?>.*?</script>@si', // Strip out javascript
        '@<[\/\!]*?[^<>]*?>@si', // Strip out HTML tags
        '@<style[^>]*?>.*?</style>@siU', // Strip style tags properly
        '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA
    );
    $text = preg_replace($search, '', $string);
    return $text;
}

function xss_clean($data) {
// Fix &entity\n;
    $data = str_replace(array('&amp;', '&lt;', '&gt;'), array('&amp;amp;', '&amp;lt;', '&amp;gt;'), $data);
    $data = preg_replace('/(&#*\w+)[\x00-\x20]+;/u', '$1;', $data);
    $data = preg_replace('/(&#x*[0-9A-F]+);*/iu', '$1;', $data);
    $data = html_entity_decode($data, ENT_COMPAT, 'UTF-8');

// Remove any attribute starting with "on" or xmlns
    $data = preg_replace('#(<[^>]+?[\x00-\x20"\'])(?:on|xmlns)[^>]*+>#iu', '$1>', $data);

// Remove javascript: and vbscript: protocols
    $data = preg_replace('#([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`\'"]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2nojavascript...', $data);
    $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2novbscript...', $data);
    $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*-moz-binding[\x00-\x20]*:#u', '$1=$2nomozbinding...', $data);

// Only works in IE: <span style="width: expression(alert('Ping!'));"></span>
    $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
    $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
    $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*+>#iu', '$1>', $data);

// Remove namespaced elements (we do not need them)
    $data = preg_replace('#</*\w+:\w[^>]*+>#i', '', $data);

    do {
        // Remove really unwanted tags
        $old_data = $data;
        $data = preg_replace('#</*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|i(?:frame|layer)|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|title|xml)[^>]*+>#i', '', $data);
    } while ($old_data !== $data);

// we are done...
    return $data;
}

?>
